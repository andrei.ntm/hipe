DOCKER_COMPOSE = docker-compose -p hipe
TOOLS = $(DOCKER_COMPOSE) run --rm api
CONSOLE = $(TOOLS) php bin/console

up:
	@$(DOCKER_COMPOSE) up -d --no-recreate
down:
	@$(DOCKER_COMPOSE) down --remove-orphans
build:
	@$(DOCKER_COMPOSE) build --force-rm
sh:
	@$(DOCKER_COMPOSE) exec api sh
logs:
	@$(DOCKER_COMPOSE) logs -f
clean-branches:
	git branch | grep -v "master" | xargs git branch -D
migrate:
	@$(CONSOLE) php bin/console doctrine:migrations:migrate -n
flush-db:
	@$(CONSOLE) bin/console doctrine:database:drop --force
	@$(CONSOLE) bin/console doctrine:database:create
fixtures:
	@$(CONSOLE) doctrine:fixtures:load -n
reset-db:
	@$(TOOLS) sh -c "php bin/console doctrine:database:drop --force && php bin/console doctrine:database:create && php bin/console doctrine:migrations:migrate -n && php bin/console doctrine:fixtures:load -n"
lint:
	@$(TOOLS) sh -c "find -L ./src -name '*.php' -print0 | xargs -0 -n 1 -P 4 php -l > /dev/null"
dump-check:
	@$(TOOLS) ./vendor/bin/var-dump-check --symfony --exclude vendor --exclude public/imports .
phpcs:
	@$(TOOLS) ./vendor/bin/php-cs-fixer fix --rules=@PSR2 ./src
test:
	@$(DOCKER_COMPOSE) build api
	@$(TOOLS) sh -c "DATABASE_URL=postgres://app:app@database/app_test php bin/phpunit -d memory_limit=512M -c phpunit.xml tests"
test-all:
	@$(DOCKER_COMPOSE) build api
	@$(TOOLS) sh -c "find -L ./src -name '*.php' -print0 | xargs -0 -n 1 -P 4 php -l > /dev/null && ./vendor/bin/var-dump-check --symfony --exclude vendor --exclude public/imports . && ./vendor/bin/php-cs-fixer fix --rules=@PSR2 ./src && ./vendor/bin/php-cs-fixer fix --rules=@PSR2 ./tests && DATABASE_URL=postgres://app:app@database/app_test php bin/phpunit -d memory_limit=512M tests"
