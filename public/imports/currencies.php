<?php

//All rates is related to USD

return [
    [
        'code' => 'AUD',
        'currency' => 'Australian Dollar',
        'rate' => 1.0,
        'symbol' => '$',
    ],
    [
        'code' => 'BRL',
        'currency' => 'Brazilian Real',
        'rate' => 1.0,
        'symbol' => 'R$',
    ],
    [
        'code' => 'CAD',
        'currency' => 'Canadian dollar',
        'rate' => 1.0,
        'symbol' => '$',
    ],
    [
        'code' => 'CZK',
        'currency' => 'Czech koruna',
        'rate' => 1.0,
        'symbol' => 'Kč',
    ],
    [
        'code' => 'DKK',
        'currency' => 'Danish krone',
        'rate' => 1.0,
        'symbol' => 'kr',
    ],
    [
        'code' => 'EUR',
        'currency' => 'Euro',
        'rate' => 1.0,
        'symbol' => '€',
    ],
    [
        'code' => 'HKD',
        'currency' => 'Hong Kong dollar',
        'rate' => 1.0,
        'symbol' => '$',
    ],
    [
        'code' => 'HUF',
        'currency' => 'Hungarian forint',
        'rate' => 1.0,
        'symbol' => 'Ft',
    ],
    [
        'code' => 'INR',
        'currency' => 'Indian rupee',
        'rate' => 1.0,
        'symbol' => null,
    ],
    [
        'code' => 'ILS',
        'currency' => 'Israeli new shekel',
        'rate' => 1.0,
        'symbol' => '₪',
    ],
    [
        'code' => 'JPY',
        'currency' => 'Japanese yen',
        'rate' => 1.0,
        'symbol' => '¥',
    ],
    [
        'code' => 'MYR',
        'currency' => 'Malaysian ringgit',
        'rate' => 1.0,
        'symbol' => 'RM',
    ],
    [
        'code' => 'MXN',
        'currency' => 'Mexican peso',
        'rate' => 1.0,
        'symbol' => '$',
    ],
    [
        'code' => 'NZD',
        'currency' => 'New Zealand dollar',
        'rate' => 1.0,
        'symbol' => null,
    ],
    [
        'code' => 'NOK',
        'currency' => 'Norwegian krone',
        'rate' => 1.0,
        'symbol' => null,
    ],
    [
        'code' => 'PHP',
        'currency' => 'Philippine peso',
        'rate' => 1.0,
        'symbol' => null,
    ],
    [
        'code' => 'PLN',
        'currency' => 'Polish złoty',
        'rate' => 1.0,
        'symbol' => null,
    ],
    [
        'code' => 'GBP',
        'currency' => 'Pound sterling',
        'rate' => 1.0,
        'symbol' => null,
    ],
    [
        'code' => 'RUB',
        'currency' => 'Russian ruble',
        'rate' => 1.0,
        'symbol' => null,
    ],
    [
        'code' => 'SGD',
        'currency' => 'Singapore dollar',
        'rate' => 1.0,
        'symbol' => null,
    ],
    [
        'code' => 'SEK',
        'currency' => 'Swedish krona',
        'rate' => 1.0,
        'symbol' => null,
    ],
    [
        'code' => 'CHF',
        'currency' => 'Swiss franc',
        'rate' => 1.0,
        'symbol' => null,
    ],
    [
        'code' => 'THB',
        'currency' => 'Thai baht',
        'rate' => 1.0,
        'symbol' => null,
    ],
    [
        'code' => 'USD',
        'currency' => 'United States dollar',
        'rate' => 1.0,
        'symbol' => '$',
    ],
];
