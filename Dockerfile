FROM php:7.3-fpm-alpine3.11

RUN apk add --no-cache --update \
        acl \
        file \
        gettext \
        git \
        postgresql-client \
        imagemagick \
        imagemagick-dev \
        busybox-suid \
    ;

RUN set -eux; \
    apk add --no-cache --virtual .build-deps \
        $PHPIZE_DEPS \
        icu-dev \
        libzip-dev \
        postgresql-dev \
        zlib-dev \
        libxml2-dev \
        oniguruma-dev \
    ; \
    \
    docker-php-ext-configure zip --with-libzip=/usr/include && docker-php-ext-install zip; \
    docker-php-ext-install -j$(nproc) \
        intl \
        pdo_pgsql \
        zip \
        soap \
        bcmath \
        pcntl \
        mbstring \
    ; \
    pecl install \
        imagick-3.4.3 \
        xdebug-2.8.1 \
    ; \
    pecl clear-cache; \
    docker-php-ext-enable \
        opcache \
        xdebug \
        soap \
        bcmath \
        pcntl \
        imagick \
    ; \
    \
    runDeps="$( \
    		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
    			| tr ',' '\n' \
    			| sort -u \
    			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
    	)"; \
    apk add --no-cache --virtual .app-phpexts-rundeps $runDeps; \
    \
    apk del .build-deps

WORKDIR /srv/api

RUN addgroup -g 1000 -S docker && \
    adduser -u 1000 -S docker -G docker

RUN chown -R docker /srv/api

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_MEMORY_LIMIT=-1

RUN set -eux; \
    composer global require "hirak/prestissimo:^0.3" --prefer-dist --no-progress --no-suggest --classmap-authoritative; \
    composer clear-cache

RUN echo "xdebug.idekey = PHPSTORM" >> $PHP_INI_DIR/conf.d/docker-php-ext-xdebug.ini; \
    echo "xdebug.default_enable = 0" >> $PHP_INI_DIR/conf.d/docker-php-ext-xdebug.ini; \
    echo "xdebug.remote_enable = 1" >> $PHP_INI_DIR/conf.d/docker-php-ext-xdebug.ini; \
    echo "xdebug.remote_autostart = 1" >> $PHP_INI_DIR/conf.d/docker-php-ext-xdebug.ini; \
    echo "xdebug.remote_connect_back = 0" >> $PHP_INI_DIR/conf.d/docker-php-ext-xdebug.ini; \
    echo "xdebug.profiler_enable = 0" >> $PHP_INI_DIR/conf.d/docker-php-ext-xdebug.ini; \
    echo "xdebug.remote_host = 10.254.254.254" >> $PHP_INI_DIR/conf.d/docker-php-ext-xdebug.ini;

COPY docker/docker-entrypoint.sh /usr/local/bin/docker-entrypoint
RUN chmod +x /usr/local/bin/docker-entrypoint

USER docker

EXPOSE 9000

ENTRYPOINT ["docker-entrypoint"]
CMD ["php-fpm"]