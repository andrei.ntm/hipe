#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php "$@"
fi

if [ "$1" = 'php-fpm' ] || [ "$1" = 'php' ]; then
	mkdir -p var/cache var/log var/sessions

	if [ "$APP_ENV" != 'prod' ] && [ "$APP_ENV" != 'test' ]; then
		composer install --prefer-dist --no-progress --no-suggest --no-interaction
	fi

	>&2 echo "Waiting for Postgres to be ready..."
	until pg_isready --timeout=0 --dbname="${DATABASE_URL}"; do
		sleep 1
	done

    if [ "$APP_ENV" != 'prod' ] && [ "$APP_ENV" != 'staging' ]; then
    		bin/console doctrine:migrations:migrate -n
    fi
fi

exec docker-php-entrypoint "$@"