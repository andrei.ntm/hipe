<?php

namespace App\Mailer;

use Symfony\Component\Asset\UrlPackage;
use Symfony\Component\Asset\VersionStrategy\StaticVersionStrategy;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AbstractMailer
{
    /**
     * @var \Symfony\Component\Mailer\MailerInterface
     */
    protected $mailer;

    /**
     * @var \Twig\Environment
     */
    protected $twig;

    /**
     * @var \Symfony\Component\Routing\Generator\UrlGeneratorInterface
     */
    protected $router;

    /**
     * AuthenticationMail constructor.
     * @param \Symfony\Component\Mailer\MailerInterface $mailer
     * @param \Twig\Environment $twig
     * @param \Symfony\Component\Routing\Generator\UrlGeneratorInterface $router
     */
    public function __construct(MailerInterface $mailer, \Twig\Environment $twig, UrlGeneratorInterface $router)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->router = $router;
    }

    public function logoUrl()
    {
        $urlPackage = new UrlPackage(
            sprintf('%s/%s', getenv('APP_URL'), 'images'),
            new StaticVersionStrategy('v1')
        );

        return $urlPackage->getUrl('/hipelogo.png');
    }
}
