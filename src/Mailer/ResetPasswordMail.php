<?php

namespace App\Mailer;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ResetPasswordMail
 * @package App\Mailer
 */
class ResetPasswordMail extends AbstractMailer
{
    /**
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sentResetPasswordMail(UserInterface $user)
    {
        $url = $this->router->generate('reset-password', [
            'token' => $user->getForgotPasswordToken()
        ]);

        $body = $this->twig->render('email/reset-password.html.twig', [
            'url' => sprintf("%s%s", getenv('APP_URL'), $url),
            'fullname' => $user->getFullname(),
            'logo_url' => $this->logoUrl()
        ]);

        $mail = (new TemplatedEmail)
            ->from(new Address(getenv('MAIL_ACCOUNT'), 'Hipe'))
            ->to(new Address($user->getEmail(), $user->getFullname()))
            ->subject('Hipe | Reset password')
            ->html($body);

        $this->mailer->send($mail);
    }
}
