<?php

namespace App\Mailer;

use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;

class AuthenticationMail extends AbstractMailer
{
    /**
     * @param \App\Entity\User $user
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function validateEmail(User $user)
    {
        $url = $this->router->generate('account-validation', [
            'token' => $user->getEmailVerificationToken(),
        ]);

        $body = $this->twig->render('email/email-validation.html.twig', [
            'url' => sprintf('%s%s', getenv('APP_URL'), $url),
            'fullname' => $user->getFullname(),
            'logo_url' => $this->logoUrl(),
        ]);

        $mail = (new TemplatedEmail())
            ->from(new Address(getenv('MAIL_ACCOUNT'), 'Hipe'))
            ->to(new Address($user->getEmail(), $user->getFullname()))
            ->subject('Hipe | Account Validation')
            ->html($body);

        $this->mailer->send($mail);
    }
}
