<?php

namespace App\Traits;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * Trait BlameableTrait
 * @package App\Traits
 * @JMS\ExclusionPolicy("none")
 */
trait BlameableTrait
{
    /**
     * @var \App\Entity\User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * @Gedmo\Blameable(on="create")
     * @JMS\Groups({"feed-item", "short-comment"})
     * @JMS\Type("App\Entity\User")
     */
    private $createdBy;

    /**
     * @var \App\Entity\User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * @Gedmo\Blameable(on="update")
     * @JMS\Groups({"feed-item", "short-comment"})
     * @JMS\Type("App\Entity\User")
     */
    private $updatedBy;

    /**
     * @param null $createdBy
     *
     * @return $this
     */
    public function setCreatedBy($createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return \App\Entity\User
     */
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @param null $updatedBy
     *
     * @return $this
     */
    public function setUpdatedBy($updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return \App\Entity\User
     */
    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }
}
