<?php

namespace App\Repository;

use App\Entity\BagItem;
use App\Entity\Color;
use App\Entity\FeedItem;
use App\Entity\Size;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class FeedItemRepository
 * @package App\Repository
 */
class FeedItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FeedItem::class);
    }

    /**
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @return int|mixed|string
     */
    public function getFeedListQuery(UserInterface $user)
    {
        return $this->createQueryBuilder('f')
            ->innerJoin('f.createdBy', 'u')
            ->innerJoin('u.followers', 'fol')
            ->where('fol.id = :user')
            ->orderBy('f.createdAt', 'DESC')
            ->setParameters([
                'user' => $user->getId()
            ]);
    }

    /**
     * @param \App\Entity\User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getFeedListByUserQuery(User $user)
    {
        return $this->createQueryBuilder('f')
            ->where('f.createdBy = :user')
            ->orderBy('f.createdAt', 'DESC')
            ->setParameters([
                'user' => $user->getId()
            ]);
    }

    /**
     * @param \App\Entity\User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getProductsListQuery(User $user)
    {
        $qb = $this->createQueryBuilder('f');

        return $qb
            ->innerJoin('f.createdBy', 'u')
            ->leftJoin('u.followers', 'fol')
            ->where(
                $qb->expr()->orX(
                    $qb->expr()->isNull('fol.id'),
                    $qb->expr()->neq('fol.id', $user->getId())
                )
            )
            ->andWhere('u.id <> :user')
            ->setParameters([
                'user' => $user->getId()
            ]);
    }

    /**
     * @param $search
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function searchProductsQuery($search)
    {
        $qb = $this->createQueryBuilder('f');

        return $qb->where(
            $qb->expr()->orX(
                $qb->expr()->like('LOWER(f.title)', $qb->expr()->literal('%' . strtolower($search) . '%')),
                $qb->expr()->like('LOWER(f.description)', $qb->expr()->literal('%' . strtolower($search) . '%')),
            )
        );
    }

    /**
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getFavoriteFeedListQuery(UserInterface $user)
    {
        return $this->createQueryBuilder('f')
            ->innerJoin('f.favoriteBy', 'u')
            ->where('u.id = :user')
            ->setParameters([
                'user' => $user->getId()
            ]);
    }

    /**
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @return int|mixed|string
     */
    public function getFeedListMayLike(UserInterface $user)
    {
        return $this->createQueryBuilder('f')
            ->innerJoin('f.createdBy', 'u')
            ->leftJoin('u.followers', 'fol')
            ->where('fol.id <> :user')
            ->andWhere('u.id <> :user')
            ->orderBy('f.createdAt', 'DESC')
            ->setParameters([
                'user' => $user->getId()
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \App\Entity\BagItem $bagItem
     * @return int|mixed|string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function retrieveFeedItem(BagItem $bagItem)
    {
        $parameters = [
          'feed' => $bagItem->getFeedItem()->getId()
        ];

        $qb = $this->createQueryBuilder('f')
            ->leftJoin('f.quantities', 'q')
            ->where('f.id = :feed')
            ->andWhere('q.amount > 0');

        if ($bagItem->getColor() instanceof Color) {
            $qb->andWhere('q.color = :color');
            $parameters['color'] = $bagItem->getColor();
        } else {
            $qb->andWhere('q.color IS NULL');
        }

        if ($bagItem->getSize() instanceof Size) {
            $qb->andWhere('q.size = :size');
            $parameters['size'] = $bagItem->getSize();
        }

        return $qb->setParameters($parameters)->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }
}
