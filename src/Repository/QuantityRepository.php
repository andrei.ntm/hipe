<?php

namespace App\Repository;

use App\Entity\BagItem;
use App\Entity\Color;
use App\Entity\Quantity;
use App\Entity\Size;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Quantity|null find($id, $lockMode = null, $lockVersion = null)
 * @method Quantity|null findOneBy(array $criteria, array $orderBy = null)
 * @method Quantity[]    findAll()
 * @method Quantity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuantityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Quantity::class);
    }

    /**
     * @param \App\Entity\BagItem $bagItem
     * @return int|mixed|string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getByColorOrSize(BagItem $bagItem)
    {
        $parameters = [];

        $qb = $this->createQueryBuilder('q');

        if ($bagItem->getColor() instanceof Color) {
            $qb->andWhere('q.color = :color');
            $parameters['color'] = $bagItem->getColor();
        } else {
            $qb->andWhere('q.color IS NULL');
        }

        if ($bagItem->getSize() instanceof Size) {
            $qb->andWhere('q.size = :size');
            $parameters['size'] = $bagItem->getSize();
        } else {
            $qb->andWhere('q.size IS NULL');
        }

        return $qb->setParameters($parameters)->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }
}
