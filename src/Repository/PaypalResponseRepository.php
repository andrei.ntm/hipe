<?php

namespace App\Repository;

use App\Entity\PaypalResponse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PaypalResponse|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaypalResponse|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaypalResponse[]    findAll()
 * @method PaypalResponse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaypalResponseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaypalResponse::class);
    }

    // /**
    //  * @return PaypalResponse[] Returns an array of PaypalResponse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PaypalResponse
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
