<?php

namespace App\Repository;

use App\Entity\Comment;
use App\Entity\FeedItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    /**
     * CommentRepository constructor.
     * @param \Doctrine\Persistence\ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQuery(FeedItem $feedItem)
    {
        return $this->createQueryBuilder('c')
            ->where('c.feedItem = :item')
            ->setParameters([
                'item' => $feedItem->getId()
            ]);
    }
}
