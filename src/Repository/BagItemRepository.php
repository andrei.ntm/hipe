<?php

namespace App\Repository;

use App\Entity\BagItem;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BagItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method BagItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method BagItem[]    findAll()
 * @method BagItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BagItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BagItem::class);
    }

    /**
     * @param \App\Entity\User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQuery(User $user)
    {
        return $this->createQueryBuilder('b')
            ->where('b.createdBy = :user')
            ->setParameters([
                'user' => $user
            ]);
    }

    /**
     * @param \App\Entity\User $user
     * @param \App\Entity\User $seller
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListBySeller(User $user, User $seller)
    {
        return $this->createQueryBuilder('b')
            ->innerJoin('b.feedItem', 'f')
            ->where('b.createdBy = :user')
            ->andWhere('f.createdBy = :seller')
            ->setParameters([
                'user' => $user,
                'seller' => $seller
            ]);
    }
}
