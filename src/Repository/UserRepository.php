<?php

namespace App\Repository;

use App\Entity\FeedItem;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param $username
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findUserByUsernameOrEmail($username)
    {
        $userQb = $this->createQueryBuilder('u');

        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $userQb->where('u.email = :username');
        } else {
            $userQb->where('u.username = :username');
        }

        return $userQb->setParameters([
                'username' => $username,
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param \App\Entity\User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getFollowersQuery(User $user)
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.following', 'f')
            ->where('f.id = :user')
            ->setParameters([
                'user' => $user->getId()
            ]);
    }

    /**
     * @param \App\Entity\User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getFollowingQuery(User $user)
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.followers', 'f')
            ->where('f.id = :user')
            ->setParameters([
                'user' => $user->getId()
            ]);
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getLikesQuery(FeedItem $feedItem)
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.likes', 'f')
            ->where('f.id = :feed')
            ->setParameters([
                'feed' => $feedItem->getId()
            ]);
    }

    /**
     * @param \App\Entity\User $user
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserListQuery(User $user, ParamFetcher $paramFetcher)
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.id <> :user')
            ->setParameters([
               'user' => $user->getId()
            ]);

        if ($paramFetcher->get('search')) {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('LOWER(u.username)', $qb->expr()->literal('%' . strtolower($paramFetcher->get('search')) . '%')),
                    $qb->expr()->like('LOWER(u.fullname)', $qb->expr()->literal('%' . strtolower($paramFetcher->get('search')) . '%')),
                )
            );
        }

        return $qb;
    }

    /**
     * @param $search
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function searchUserQuery($search)
    {
        $qb = $this->createQueryBuilder('u');

        return $qb->where(
            $qb->expr()->orX(
                $qb->expr()->like('LOWER(u.username)', $qb->expr()->literal('%' . strtolower($search) . '%')),
                $qb->expr()->like('LOWER(u.fullname)', $qb->expr()->literal('%' . strtolower($search) . '%')),
            )
        );
    }

    /**
     * @param \App\Entity\User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUnfollowRandomQuery(User $user)
    {
        $qb = $this->createQueryBuilder('u');

        $qb->leftJoin('u.followers', 'fs')
            ->leftJoin('u.following', 'fg')
            ->where(
                $qb->expr()->orX(
                    $qb->expr()->isNull('fs.id'),
                    $qb->expr()->isNull('fg.id'),
                )
            )
            ->andWhere('u.id <> :user')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->isNull('fs.id'),
                    $qb->expr()->isNull('fg.id'),
                    $qb->expr()->neq('fs.id', $user->getId())
                )
            )
            ->andWhere('random() < 1') //DB is getting randomly from 100%, decrease number for increasing performance(0.02)
            ->setParameters([
                'user' => $user->getId()
            ]);

        return $qb;
    }
}
