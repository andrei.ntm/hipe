<?php

namespace App\Entity;

use App\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaypalResponseRepository")
 */
class PaypalResponse
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Type("integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="paypalResponses", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false, name="user_id")
     * @JMS\Exclude
     */
    private $user;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @JMS\Type("array")
     */
    private $content = [];

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \App\Entity\User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param \Symfony\Component\Security\Core\User\UserInterface|\App\Entity\User|null $user
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getContent(): ?array
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return $this
     *
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
