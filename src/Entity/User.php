<?php

namespace App\Entity;

use App\Traits\TimestampableTrait;
use App\Utils\User as UserUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator as AppAssert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="users")
 * @UniqueEntity("email", groups={"Default", "registration", "fb-register", "fb-register-mail", "vk-register-mail", "vk-register", "update-profile"})
 * @UniqueEntity("username", groups={"Default", "registration", "fb-register", "fb-register-mail", "vk-register-mail", "vk-register", "update-profile"})
 * @UniqueEntity("forgotPasswordToken", groups={"Default", "registration", "fb-register", "fb-register-mail", "vk-register-mail", "vk-register"})
 */
class User implements UserInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"profile", "guest-profile", "visit-profile", "short-info"})
     * @JMS\Type("integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, unique=true)
     * @Assert\NotBlank(groups={"registration"})
     * @Assert\Regex(pattern="/^[a-zA-Z0-9._-]*$/", groups={"registration", "update-profile"})
     * @Assert\Length(min=3, max=100, groups={"registration", "update-profile"})
     * @JMS\Groups({"profile", "guest-profile", "visit-profile", "short-info"})
     * @JMS\Type("string")
     */
    private $username;

    /**
     * @ORM\Column(type="string", name="facebook_id", length=100, nullable=true)
     * @Assert\NotBlank(groups={"fb-register", "fb-register-mail"})
     * @JMS\Exclude
     */
    private $facebookId;

    /**
     * @ORM\Column(type="string", name="facebook_access_token", length=255, nullable=true)
     * @Assert\NotBlank(groups={"fb-register", "fb-register-mail"})
     * @JMS\Exclude
     */
    private $facebookAccessToken;

    /**
     * @ORM\Column(type="string", name="apple_id", length=100, nullable=true)
     * @Assert\NotBlank(groups={"apple-register"})
     * @JMS\Exclude
     */
    private $appleId;

    /**
     * @ORM\Column(type="string", name="apple_access_token", length=255, nullable=true)
     * @Assert\NotBlank(groups={"apple-register"})
     * @JMS\Exclude
     */
    private $appleAccessToken;

    /**
     * @ORM\Column(type="string", name="vk_id", length=100, nullable=true)
     * @Assert\NotBlank(groups={"vk-register", "vk-register-mail"})
     * @JMS\Exclude
     */
    private $vkId;

    /**
     * @ORM\Column(type="string", name="vk_access_token", length=255, nullable=true)
     * @Assert\NotBlank(groups={"vk-register", "vk-register-mail"})
     * @JMS\Exclude
     */
    private $vkAccessToken;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @AppAssert\ImageExists(groups={"update-profile"})
     * @JMS\Groups({"profile", "guest-profile", "visit-profile", "short-info"})
     * @JMS\Type("ImageAbsolutePath")
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(min=3, max=100, groups={"registration", "fb-register", "fb-register-mail", "vk-register", "vk-register-mail", "update-profile"})
     * @Assert\NotBlank(groups={"registration"})
     * @JMS\Groups({"profile", "guest-profile", "visit-profile", "short-info"})
     * @JMS\Type("string")
     */
    private $fullname;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, unique=true)
     * @Assert\NotBlank(groups={"registration", "fb-register-mail", "vk-register-mail", "reset-password", "update-profile", "apple-register"})
     * @Assert\Email(groups={"registration", "fb-register-mail", "vk-register-mail", "reset-password", "update-profile", "apple-register"})
     * @JMS\Groups({"profile"})
     * @JMS\Type("string")
     */
    private $email;

    /**
     * @ORM\Column(type="boolean", name="email_verified", length=100)
     * @JMS\Groups({"profile"})
     * @JMS\Type("boolean")
     */
    private $emailVerified = false;

    /**
     * @ORM\Column(type="string", name="email_verification_token", length=100, nullable=true, unique=true)
     * @JMS\Exclude
     */
    private $emailVerificationToken;

    /**
     * @ORM\Column(type="boolean", name="account_completed", length=100)
     * @JMS\Groups({"profile"})
     * @JMS\Type("boolean")
     */
    private $accountCompleted = false;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * @Assert\Length(max=255)
     * @JMS\Groups({"profile", "visit-profile"})
     * @JMS\Type("string")
     */
    private $description;

    /**
     * @ORM\Column(name="instagram_account", type="string", nullable=true, length=50)
     * @Assert\Length(max=50, groups={"update-profile"})
     * @JMS\Groups({"profile", "visit-profile"})
     * @JMS\Type("string")
     */
    private $instagramAccount;

    /**
     * @ORM\Column(name="youtube_account", type="string", nullable=true, length=50)
     * @Assert\Length(max=50, groups={"update-profile"})
     * @JMS\Groups({"profile", "visit-profile"})
     * @JMS\Type("string")
     */
    private $youtubeAccount;

    /**
     * @ORM\Column(name="tiktok_account", type="string", nullable=true, length=50)
     * @Assert\Length(max=50, groups={"update-profile"})
     * @JMS\Groups({"profile", "visit-profile"})
     * @JMS\Type("string")
     */
    private $tiktokAccount;

    /**
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     * @Assert\NotBlank(groups={"registration", "fb-register", "fb-register-mail", "vk-register", "vk-register-mail", "apple-register"})
     * @JMS\Groups({"profile"})
     * @JMS\Type("App\Entity\Currency")
     */
    private $currency;

    /**
     * @ORM\Column(type="boolean")
     * @JMS\Groups({"profile"})
     * @JMS\Type("boolean")
     */
    private $suspended = false;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotBlank(groups={"registration"})
     * @Assert\Length(min=6, max=100, groups={"registration", "update-profile"})
     * @JMS\Exclude
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Type(type="string")
     * @Assert\Length(max=255)
     * @JMS\Type("string")
     * @JMS\Exclude
     */
    private $salt;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     * @JMS\Groups({"profile", "visit-profile", "short-info"})
     * @JMS\Type("array")
     */
    private $roles = [];

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\FeedItem", inversedBy="favoriteBy")
     * @ORM\JoinTable(name="favorites",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="feed_item_id", referencedColumnName="id")}
     *      )
     * @JMS\Exclude
     */
    private $favorites;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\FeedItem", inversedBy="likedBy")
     * @ORM\JoinTable(name="likes",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="feed_item_id", referencedColumnName="id")}
     *      )
     * @JMS\Exclude
     */
    private $likes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="following")
     * @ORM\JoinTable(name="followers",
     *      joinColumns={@ORM\JoinColumn(name="follow_user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     * @JMS\Exclude
     */
    private $followers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="followers")
     * @JMS\Exclude
     */
    private $following;

    /**
     * @ORM\Column(type="string", name="forgot_password_token", length=100, nullable=true, unique=true)
     * @JMS\Exclude
     */
    private $forgotPasswordToken;

    /**
     * @ORM\Column(type="string", name="merchant_id", length=100, nullable=true, unique=true)
     * @JMS\Type("string")
     */
    private $merchantId;

    /**
     * @ORM\Column(type="string", name="onboarding_status", length=100, nullable=true)
     * @JMS\Type("string")
     */
    private $onboardingStatus;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FeedItem", mappedBy="createdBy")
     * @JMS\Exclude
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PaypalResponse", mappedBy="user", orphanRemoval=true)
     */
    private $paypalResponses;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->followers = new ArrayCollection();
        $this->following = new ArrayCollection();
        $this->favorites = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->paypalResponses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param $role
     *
     * @return $this
     */
    public function addRole($role): self
    {
        if (!in_array($role, $this->roles)) {
            array_push($this->roles, $role);
        }

        return $this;
    }

    /**
     * @param $role
     *
     * @return $this
     */
    public function removeRole($role): self
    {
        if (in_array($role, $this->roles)) {
            unset($this->roles[array_search($role, $this->roles)]);
        }

        return $this;
    }

    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getRoles(): ?array
    {
        return $this->roles;
    }

    /**
     * @return string|null
     */
    public function getSalt(): ?string
    {
        return $this->salt;
    }

    /**
     * @param string|null $salt
     * @return $this
     */
    public function setSalt(?string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return string
     */
    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    /**
     * @param string $fullname
     * @return $this
     */
    public function setFullname(?string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * @return string
     */
    public function getForgotPasswordToken(): ?string
    {
        return $this->forgotPasswordToken;
    }

    /**
     * @param string $forgotPasswordToken
     *
     * @return $this
     */
    public function setForgotPasswordToken(?string $forgotPasswordToken): self
    {
        $this->forgotPasswordToken = $forgotPasswordToken;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getInstagramAccount(): ?string
    {
        return $this->instagramAccount;
    }

    /**
     * @param string|null $instagramAccount
     * @return $this
     */
    public function setInstagramAccount(?string $instagramAccount): self
    {
        $this->instagramAccount = $instagramAccount;

        return $this;
    }

    public function getYoutubeAccount(): ?string
    {
        return $this->youtubeAccount;
    }

    /**
     * @param string|null $youtubeAccount
     * @return $this
     */
    public function setYoutubeAccount(?string $youtubeAccount): self
    {
        $this->youtubeAccount = $youtubeAccount;

        return $this;
    }

    public function getTiktokAccount(): ?string
    {
        return $this->tiktokAccount;
    }

    /**
     * @param string|null $tiktokAccount
     * @return $this
     */
    public function setTiktokAccount(?string $tiktokAccount): self
    {
        $this->tiktokAccount = $tiktokAccount;

        return $this;
    }

    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    /**
     * @param string|null $facebookId
     * @return $this
     */
    public function setFacebookId(?string $facebookId): self
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    public function getFacebookAccessToken(): ?string
    {
        return $this->facebookAccessToken;
    }

    /**
     * @param string|null $facebookAccessToken
     * @return $this
     */
    public function setFacebookAccessToken(?string $facebookAccessToken): self
    {
        $this->facebookAccessToken = $facebookAccessToken;

        return $this;
    }

    public function getVkId(): ?string
    {
        return $this->vkId;
    }

    /**
     * @param string|null $vkId
     * @return $this
     */
    public function setVkId(?string $vkId): self
    {
        $this->vkId = $vkId;

        return $this;
    }

    public function getVkAccessToken(): ?string
    {
        return $this->vkAccessToken;
    }

    /**
     * @param string|null $vkAccessToken
     * @return $this
     */
    public function setVkAccessToken(?string $vkAccessToken): self
    {
        $this->vkAccessToken = $vkAccessToken;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEmailVerified(): ?bool
    {
        return $this->emailVerified;
    }

    /**
     * @param bool|null $emailVerified
     * @return $this
     */
    public function setEmailVerified(?bool $emailVerified): self
    {
        $this->emailVerified = $emailVerified;

        return $this;
    }

    public function isSuspended(): ?bool
    {
        return $this->suspended;
    }

    /**
     * @param bool|null $suspended
     * @return $this
     */
    public function setSuspended(?bool $suspended): self
    {
        $this->suspended = $suspended;

        return $this;
    }

    public function isAccountCompleted(): bool
    {
        return $this->accountCompleted;
    }

    /**
     * @param bool $accountCompleted
     * @return $this
     */
    public function setAccountCompleted(?bool $accountCompleted): self
    {
        $this->accountCompleted = $accountCompleted;

        return $this;
    }

    public function getEmailVerificationToken(): ?string
    {
        return $this->emailVerificationToken;
    }

    /**
     * @param string|null $emailVerificationToken
     * @return $this
     */
    public function setEmailVerificationToken(?string $emailVerificationToken): self
    {
        $this->emailVerificationToken = $emailVerificationToken;

        return $this;
    }

    /**
     * @return \App\Entity\Currency|null
     */
    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    /**
     * @param $currency
     *
     * @return $this
     */
    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    /**
     * @param string|null $photo
     * @return $this
     */
    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAppleId(): ?string
    {
        return $this->appleId;
    }

    /**
     * @param $appleId
     * @return $this
     */
    public function setAppleId(?string $appleId): self
    {
        $this->appleId = $appleId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAppleAccessToken(): ?string
    {
        return $this->appleAccessToken;
    }

    /**
     * @param string|null $appleAccessToken
     * @return $this
     */
    public function setAppleAccessToken(?string $appleAccessToken): self
    {
        $this->appleAccessToken = $appleAccessToken;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getFollowing(): Collection
    {
        return $this->following;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getFollowers(): Collection
    {
        return $this->followers;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavorites(): Collection
    {
        return $this->favorites;
    }

    /**
     * @return mixed
     */
    public function getMerchantId(): string
    {
        return $this->merchantId;
    }

    /**
     * @param string|null $merchantId
     * @return $this
     */
    public function setMerchantId(?string $merchantId): self
    {
        $this->merchantId = $merchantId;

        return $this;
    }

    /**
     * @return string
     */
    public function getOnboardingStatus(): string
    {
        return $this->onboardingStatus;
    }

    /**
     * @param string|null $onboardingStatus
     * @return $this
     */
    public function setOnboardingStatus(?string $onboardingStatus): self
    {
        $this->onboardingStatus = $onboardingStatus;

        return $this;
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @return $this
     */
    public function addFavorite(FeedItem $feedItem): self
    {
        if (!$this->favorites->contains($feedItem)) {
            $this->favorites->add($feedItem);
        }

        return $this;
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @return $this
     */
    public function removeFavorite(FeedItem $feedItem): self
    {
        if ($this->favorites->contains($feedItem)) {
            $this->favorites->removeElement($feedItem);
        }

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @return $this
     */
    public function addLike(FeedItem $feedItem): self
    {
        if (!$this->likes->contains($feedItem)) {
            $this->likes->add($feedItem);
        }

        return $this;
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @return $this
     */
    public function removeLike(FeedItem $feedItem): self
    {
        if ($this->likes->contains($feedItem)) {
            $this->likes->removeElement($feedItem);
        }

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @param \App\Entity\FeedItem $product
     * @return $this
     */
    public function addProduct(FeedItem $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
        }

        return $this;
    }

    /**
     * @param \App\Entity\FeedItem $product
     * @return $this
     */
    public function removeProduct(FeedItem $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }

        return $this;
    }

    /**
     * @return Collection|PaypalResponse[]
     */
    public function getPaypalResponses(): Collection
    {
        return $this->paypalResponses;
    }

    public function addPaypalResponse(PaypalResponse $paypalResponse): self
    {
        if (!$this->paypalResponses->contains($paypalResponse)) {
            $this->paypalResponses[] = $paypalResponse;
            $paypalResponse->setUser($this);
        }

        return $this;
    }

    public function removePaypalResponse(PaypalResponse $paypalResponse): self
    {
        if ($this->paypalResponses->contains($paypalResponse)) {
            $this->paypalResponses->removeElement($paypalResponse);
            // set the owning side to null (unless already changed)
            if ($paypalResponse->getUser() === $this) {
                $paypalResponse->setUser(null);
            }
        }

        return $this;
    }
}
