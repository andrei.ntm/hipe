<?php

namespace App\Entity;

use App\Traits\BlameableTrait;
use App\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyRepository")
 * @ORM\Table(name="currencies")
 * @UniqueEntity("code")
 */
class Currency
{
    use TimestampableTrait;
    use BlameableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"feed-item", "profile"})
     * @JMS\Type("integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20, unique=true)
     * @Assert\NotBlank
     * @Assert\Type(type="string")
     * @Assert\Length(max=3)
     * @JMS\Groups({"feed-item", "profile"})
     * @JMS\Type("string")
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank
     * @Assert\Type(type="string")
     * @Assert\Length(max=100)
     * @JMS\Groups({"feed-item", "profile"})
     * @JMS\Type("string")
     */
    private $title;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=10)
     * @Assert\Regex("/^(\d{1,5})$|^(\d{1,5}\.\d{0,10})$/")
     * @Assert\GreaterThan(0)
     * @Assert\NotBlank
     * @Assert\Type(type="numeric")
     * @JMS\Type("float")
     */
    private $rate;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Assert\Type(type="string")
     * @Assert\Length(max=4)
     * @JMS\Groups({"feed-item", "profile"})
     * @JMS\Type("string")
     */
    private $symbol;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getRate(): ?float
    {
        return $this->rate;
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(?string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }
}
