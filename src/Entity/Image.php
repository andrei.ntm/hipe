<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImagesRepository")
 * @ORM\Table(name="images")
 */
class Image
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"feed-item", "minimal-feed-item", "feed-short-info"})
     * @JMS\Type("integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="path")
     * @JMS\Groups({"feed-item", "minimal-feed-item", "feed-short-info"})
     * @JMS\Type("ImageAbsolutePath")
     */
    private $path;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FeedItem", inversedBy="images")
     * @ORM\JoinColumn(name="feed_item", nullable=false)
     * @JMS\Type("App\Entity\FeedItem")
     */
    private $feedItem;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \App\Entity\FeedItem|null
     */
    public function getFeedItem(): ?FeedItem
    {
        return $this->feedItem;
    }

    /**
     * @param \App\Entity\FeedItem|null $feedItem
     * @return $this
     */
    public function setFeedItem(?FeedItem $feedItem): self
    {
        $this->feedItem = $feedItem;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param $path
     *
     * @return $this
     */
    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }
}
