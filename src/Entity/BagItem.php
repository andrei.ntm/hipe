<?php

namespace App\Entity;

use App\Traits\BlameableTrait;
use App\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BagItemRepository")
 * @ORM\Table(name="bag_items", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="content_item_unique",
 *            columns={"feed_item_id", "color_id", "size_id"}
 *          ),
 * })
 *
 * @UniqueEntity(fields={"feedItem", "color", "size"})
 */
class BagItem
{
    use TimestampableTrait, BlameableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"bag-item"})
     * @JMS\Type("integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @ORM\ManyToOne(targetEntity="App\Entity\FeedItem")
     * @ORM\JoinColumn(name="feed_item_id", nullable=false)
     * @JMS\Groups({"bag-item"})
     * @JMS\Type("App\Entity\FeedItem")
     */
    private $feedItem;

    /**
     * @Assert\NotBlank
     * @Assert\Type("integer")
     * @Assert\Range(min=1)
     * @ORM\Column(type="integer")
     * @JMS\Groups({"bag-item"})
     * @JMS\Type("integer")
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Color")
     * @ORM\JoinColumn(name="color_id", nullable=true)
     * @JMS\Groups({"bag-item"})
     * @JMS\Type("App\Entity\Color")
     */
    private $color;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Size")
     * @ORM\JoinColumn(name="size_id", nullable=true)
     * @JMS\Groups({"bag-item"})
     * @JMS\Type("App\Entity\Size")
     */
    private $size;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFeedItem(): ?FeedItem
    {
        return $this->feedItem;
    }

    public function setFeedItem(?FeedItem $feedItem): self
    {
        $this->feedItem = $feedItem;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getColor(): ?Color
    {
        return $this->color;
    }

    public function setColor(?Color $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getSize(): ?Size
    {
        return $this->size;
    }

    public function setSize(?Size $size): self
    {
        $this->size = $size;

        return $this;
    }
}
