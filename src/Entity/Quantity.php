<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuantityRepository")
 * @ORM\Table(name="quantities")
 */
class Quantity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", name="amount")
     * @Assert\NotBlank
     * @Assert\Range(min=0, max=9999)
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("integer")
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FeedItem", inversedBy="quantities")
     * @ORM\JoinColumn(name="feed_item", referencedColumnName="id")
     * @Assert\NotBlank
     * @JMS\Type("App\Entity\FeedItem")
     */
    private $feedItem;

    /**
     * @ORM\ManyToOne(targetEntity="Color", cascade={"persist"})
     * @ORM\JoinColumn(name="color_id", referencedColumnName="id", nullable=true)
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("App\Entity\Color")
     */
    private $color;

    /**
     * @ORM\ManyToOne(targetEntity="Size", cascade={"persist"})
     * @ORM\JoinColumn(name="size_id", referencedColumnName="id", nullable=true)
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("App\Entity\Size")
     */
    private $size;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param $amount
     *
     * @return $this
     */
    public function setAmount($amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return \App\Entity\FeedItem|null
     */
    public function getFeedItem(): ?FeedItem
    {
        return $this->feedItem;
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     *
     * @return $this
     */
    public function setFeedItem(FeedItem $feedItem): self
    {
        $this->feedItem = $feedItem;

        return $this;
    }

    /**
     * @return \App\Entity\Color|null
     */
    public function getColor(): ?Color
    {
        return $this->color;
    }

    /**
     * @param \App\Entity\Color $color
     *
     * @return $this
     */
    public function setColor(?Color $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSize(): ?Size
    {
        return $this->size;
    }

    /**
     * @param \App\Entity\Size $size
     *
     * @return $this
     */
    public function setSize(?Size $size): self
    {
        $this->size = $size;

        return $this;
    }
}
