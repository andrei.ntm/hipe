<?php

namespace App\Entity;

use App\Traits\BlameableTrait;
use App\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FeedItemRepository")
 * @ORM\Table(name="feed_items")
 */
class FeedItem
{
    use TimestampableTrait;
    use BlameableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"feed-item", "minimal-feed-item", "feed-short-info"})
     * @JMS\Type("integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", name="title", nullable=true)
     * @JMS\Groups({"feed-item", "minimal-feed-item", "feed-short-info"})
     * @JMS\Type("string")
     */
    private $title;

    /**
     * @ORM\Column(type="text", name="description", nullable=true)
     * @JMS\Groups({"feed-item", "feed-short-info"})
     * @JMS\Type("string")
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", name="worldwide_shipping", options={"default":0})
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("boolean")
     */
    private $worldwideShipping = false;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * @JMS\Groups({"feed-item", "feed-short-info"})
     * @JMS\Type("App\Entity\Category")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("App\Entity\Country")
     */
    private $country;

    /**
     * @ORM\OneToOne(targetEntity="Price", cascade={"persist"})
     * @ORM\JoinColumn(name="price_id", referencedColumnName="id")
     * @JMS\Groups({"feed-item", "feed-short-info"})
     * @JMS\Type("App\Entity\Price")
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="feedItem", cascade={"persist"}, orphanRemoval=true)
     * @JMS\Groups({"feed-item", "minimal-feed-item", "feed-short-info"})
     * @JMS\Type("ArrayCollection<App\Entity\Image>")
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Quantity", mappedBy="feedItem", cascade={"persist"}, orphanRemoval=true)
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("ArrayCollection<App\Entity\Quantity>")
     */
    private $quantities;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="favorites")
     * @JMS\Exclude
     */
    private $favoriteBy;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="likes")
     * @JMS\Exclude
     */
    private $likedBy;

    /**
     * @var \App\Entity\User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="products")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * @Gedmo\Blameable(on="create")
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("App\Entity\User")
     */
    private $createdBy;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="feedItem", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Type("App\Entity\Comment")
     */
    private $comments;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->quantities = new ArrayCollection();
        $this->favoriteBy = new ArrayCollection();
        $this->likedBy = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return \App\Entity\Country|null
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * @param \App\Entity\Country $country
     *
     * @return $this
     */
    public function setCountry(Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return \App\Entity\Price|null
     */
    public function getPrice(): ?Price
    {
        return $this->price;
    }

    /**
     * @param \App\Entity\Price $price
     *
     * @return $this
     */
    public function setPrice(Price $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function isWorldwideShipping(): bool
    {
        return $this->worldwideShipping;
    }

    /**
     * @param bool $worldwideShipping
     * @return $this
     */
    public function setWorldwideShipping(bool $worldwideShipping): self
    {
        $this->worldwideShipping = $worldwideShipping;

        return $this;
    }

    public function getQuantities(): Collection
    {
        return $this->quantities;
    }

    /**
     * @param \App\Entity\Quantity $quantity
     *
     * @return $this
     */
    public function addQuantity(Quantity $quantity): self
    {
        if (!$this->quantities->contains($quantity)) {
            $this->quantities->add($quantity);
            $quantity->setFeedItem($this);
        }

        return $this;
    }

    /**
     * @param \App\Entity\Quantity $quantity
     * @return $this
     */
    public function removeQuantity(Quantity $quantity): self
    {
        if ($this->quantities->contains($quantity)) {
            $this->quantities->removeElement($quantity);

            if ($quantity->getFeedItem() === $this) {
                $quantity->setFeedItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * @param \App\Entity\Image $image
     *
     * @return $this
     */
    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images->add($image);
            $image->setFeedItem($this);
        }

        return $this;
    }

    /**
     * @param \App\Entity\Image $image
     *
     * @return $this
     */
    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);

            if ($image->getFeedItem() === $this) {
                $image->setFeedItem(null);
            }
        }

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return $this
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return \App\Entity\Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param \App\Entity\Category $category
     *
     * @return $this
     */
    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavoriteBy(): Collection
    {
        return $this->favoriteBy;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikedBy(): Collection
    {
        return $this->likedBy;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setFeedItem($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getFeedItem() === $this) {
                $comment->setFeedItem(null);
            }
        }

        return $this;
    }
}
