<?php

namespace App\Entity;

use App\Traits\BlameableTrait;
use App\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @ORM\Table(name="categories")
 * @UniqueEntity("key")
 */
class Category
{
    use TimestampableTrait;
    use BlameableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"feed-item", "feed-short-info"})
     * @JMS\Type("integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     * @JMS\Groups({"feed-item", "feed-short-info"})
     * @JMS\Type("string")
     */
    private $key;

    /**
     * @ORM\Column(type="integer", unique=true)
     * @JMS\Type("integer")
     */
    private $position;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKey(): ?string
    {
        return $this->key;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param $position
     *
     * @return $this
     */
    public function setPosition($position): self
    {
        $this->position = $position;

        return $this;
    }
}
