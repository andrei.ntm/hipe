<?php

namespace App\Entity;

use App\Traits\BlameableTrait;
use App\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 * @ORM\Table(name="comments")
 */
class Comment
{
    use TimestampableTrait, BlameableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"short-comment"})
     * @JMS\Type("integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @JMS\Groups({"short-comment"})
     * @JMS\Type("string")
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FeedItem", inversedBy="comments")
     * @ORM\JoinColumn(name="feed_item", referencedColumnName="id")
     * @JMS\Exclude
     */
    private $feedItem;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @JMS\Groups({"short-comment"})
     * @JMS\Type("array")
     */
    private $tags = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getFeedItem(): ?FeedItem
    {
        return $this->feedItem;
    }

    public function setFeedItem(?FeedItem $feedItem): self
    {
        $this->feedItem = $feedItem;

        return $this;
    }

    public function getTags(): ?array
    {
        return $this->tags;
    }

    public function setTags(?array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }
}
