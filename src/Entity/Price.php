<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PriceRepository")
 * @ORM\Table("prices")
 */
class Price
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"feed-item", "feed-short-info"})
     * @JMS\Type("integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", name="amount")
     * @JMS\Groups({"feed-item", "feed-short-info"})
     * @JMS\Type("float")
     */
    private $amount;

    /**
     * @ORM\Column(type="float", name="domestic_shipping_amount")
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("float")
     */
    private $domesticShippingAmount;

    /**
     * @ORM\Column(type="float", name="worldwide_shipping_amount", nullable=true)
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("float")
     */
    private $worldwideShippingAmount;

    /**
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     * @JMS\Groups({"feed-item", "feed-short-info"})
     * @JMS\Type("App\Entity\Currency")
     */
    private $currency;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return \App\Entity\Currency|null
     */
    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    /**
     * @param \App\Entity\Currency $currency
     *
     * @return $this
     */
    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDomesticShippingAmount()
    {
        return $this->domesticShippingAmount;
    }

    /**
     * @param $domesticShippingAmount
     *
     * @return $this
     */
    public function setDomesticShippingAmount($domesticShippingAmount): self
    {
        $this->domesticShippingAmount = $domesticShippingAmount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWorldwideShippingAmount()
    {
        return $this->worldwideShippingAmount;
    }

    /**
     * @param $worldwideShippingAmount
     *
     * @return \App\Entity\Price
     */
    public function setWorldwideShippingAmount($worldwideShippingAmount): self
    {
        $this->worldwideShippingAmount = $worldwideShippingAmount;

        return $this;
    }
}
