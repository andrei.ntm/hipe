<?php

namespace App\Entity;

use App\Traits\BlameableTrait;
use App\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CountryRepository")
 * @ORM\Table(name="countries")
 * @UniqueEntity("code")
 */
class Country
{
    use TimestampableTrait;
    use BlameableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="code", length=100, unique=true)
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("string")
     */
    private $code;

    /**
     * @ORM\Column(type="string", name="name", length=100)
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DeliveryAddress", mappedBy="country")
     * @JMS\Exclude
     */
    private $deliveryAddresses;

    public function __construct()
    {
        $this->deliveryAddresses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|DeliveryAddress[]
     */
    public function getDeliveryAddresses(): Collection
    {
        return $this->deliveryAddresses;
    }

    /**
     * @param \App\Entity\DeliveryAddress $deliveryAddress
     * @return $this
     */
    public function addAddress(DeliveryAddress $deliveryAddress): self
    {
        if (!$this->deliveryAddresses->contains($deliveryAddress)) {
            $this->deliveryAddresses[] = $deliveryAddress;
            $deliveryAddress->setCountry($this);
        }

        return $this;
    }

    /**
     * @param \App\Entity\DeliveryAddress $deliveryAddress
     * @return $this
     */
    public function removeAddress(DeliveryAddress $deliveryAddress): self
    {
        if ($this->deliveryAddresses->contains($deliveryAddress)) {
            $this->deliveryAddresses->removeElement($deliveryAddress);
            // set the owning side to null (unless already changed)
            if ($deliveryAddress->getCountry() === $this) {
                $deliveryAddress->setCountry(null);
            }
        }

        return $this;
    }
}
