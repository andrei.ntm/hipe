<?php

namespace App\Entity;

use App\Traits\BlameableTrait;
use App\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ColorRepository")
 * @ORM\Table(name="colors")
 */
class Color
{
    use TimestampableTrait;
    use BlameableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="code", length=50)
     * @Assert\NotBlank
     * @Assert\Regex("/^\#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/")
     * @JMS\Groups({"feed-item"})
     * @JMS\Type("string")
     */
    private $code;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }
}
