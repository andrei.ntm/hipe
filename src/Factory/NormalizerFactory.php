<?php

namespace App\Factory;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class NormalizerFactory
 * @package App\Factory
 */
class NormalizerFactory
{
    /**
     * @var iterable
     */
    private $normalizers;

    /**
     * NormalizerFactory constructor.
     * @param iterable $normalizers
     */
    public function __construct(iterable $normalizers)
    {
        $this->normalizers = $normalizers;
    }

    /**
     * @param $data
     * @return mixed|null
     */
    public function getNormalizer($data)
    {
        foreach ($this->normalizers as $normalizer) {
            if ($normalizer instanceof NormalizerInterface && $normalizer->supportsNormalization($data)) {
                return $normalizer;
            }
        }
        return null;
    }
}
