<?php

namespace App\Utils;

class ErrorType
{
    const NOT_COMPLETED_ACCOUNT = 'NOT_COMPLETED_ACCOUNT';
    const EMPTY_EMAIL = 'EMPTY_EMAIL';
    const EMPTY_USERNAME = 'EMPTY_USERNAME';
    const EMPTY_ROLE = 'EMPTY_ROLE';
}
