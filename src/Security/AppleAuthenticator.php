<?php

namespace App\Security;

use App\Entity\User;
use App\Exception\StandardException;
use App\Handlers\ORM\AppleRegisterHandler;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AuthenticatorInterface;
use App\Services\AppleService;

/**
 * Class AppleAuthenticator
 * @package App\Security
 */
class AppleAuthenticator implements AuthenticatorInterface
{
    /**
     * @var \Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface
     */
    protected $JWTTokenManager;

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;

    /**
     * @var \App\Services\AppleService
     */
    protected $appleService;

    /**
     * @var \App\Handlers\ORM\AppleRegisterHandler
     */
    protected $appleRegisterHandler;

    /**
     * AppleAuthenticator constructor.
     * @param \Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface $JWTTokenManager
     * @param \Doctrine\ORM\EntityManagerInterface $em
     * @param \App\Services\AppleService $appleService
     * @param \App\Handlers\ORM\AppleRegisterHandler $appleRegisterHandler
     */
    public function __construct(JWTTokenManagerInterface $JWTTokenManager, EntityManagerInterface $em, AppleService $appleService, AppleRegisterHandler $appleRegisterHandler)
    {
        $this->JWTTokenManager = $JWTTokenManager;
        $this->em = $em;
        $this->appleService = $appleService;
        $this->appleRegisterHandler = $appleRegisterHandler;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return;
    }

    public function supports(Request $request)
    {
        return true;
    }

    public function getCredentials(Request $request)
    {
        if (!$request->request->has('access_token')) {
            throw new AuthenticationException();
        }

        return [
            'access_token' => $request->request->get('access_token')
        ];
    }

    /**
     * @param mixed $credentials
     * @param \Symfony\Component\Security\Core\User\UserProviderInterface $userProvider
     * @return \App\Entity\User|\Symfony\Component\Security\Core\User\UserInterface|null
     * @throws \App\Exception\InvalidFormException
     * @throws \App\Exception\StandardException
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        try {
            $appleResponse = $this->appleService->setAccessToken($credentials['access_token'])->retrieveUser();
        } catch (\Exception $e) {
            throw new StandardException("Apple sign failed", $e->getCode());
        }

        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy([
            'appleId' => $appleResponse['token']
        ]);

        if (!$user instanceof User) {
            $user = $this->appleRegisterHandler->registerUser($appleResponse);
        }

        $user->setAppleAccessToken($credentials['access_token']);

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return $user instanceof UserInterface;
    }

    public function createAuthenticatedToken(UserInterface $user, $providerKey)
    {
        return new JWTUserToken($user->getRoles(), $user, $this->JWTTokenManager->create($user), $providerKey);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new Response('Authentication failure', Response::HTTP_UNAUTHORIZED);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return;
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
