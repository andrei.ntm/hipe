<?php

namespace App\Security;

use App\Entity\User;
use App\Handlers\ORM\FacebookRegisterHandler;
use App\Services\FacebookService;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AuthenticatorInterface;

class FacebookAuthenticator implements AuthenticatorInterface
{
    /**
     * @var \App\Services\FacebookService
     */
    private $facebookService;

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    /**
     * @var \App\Handlers\ORM\FacebookRegisterHandler
     */
    private $facebookRegisterHandler;

    /**
     * @var \Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface
     */
    private $JWTTokenManager;

    /**
     * FacebookAuthenticator constructor.
     * @param \App\Services\FacebookService $facebookService
     * @param \Doctrine\ORM\EntityManagerInterface $em
     * @param \App\Handlers\ORM\FacebookRegisterHandler $facebookRegisterHandler
     * @param \Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface $JWTTokenManager
     */
    public function __construct(
        FacebookService $facebookService,
        EntityManagerInterface $em,
        FacebookRegisterHandler $facebookRegisterHandler,
        JWTTokenManagerInterface $JWTTokenManager
    ) {
        $this->facebookService = $facebookService;
        $this->em = $em;
        $this->facebookRegisterHandler = $facebookRegisterHandler;
        $this->JWTTokenManager = $JWTTokenManager;
    }

    /**
     * @inheritDoc
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return;
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request)
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getCredentials(Request $request)
    {
        if (!$request->request->has('access_token')) {
            throw new AuthenticationException();
        }

        return [
            'access_token' => $request->request->get('access_token')
        ];
    }

    /**
     * @param mixed $credentials
     * @param \Symfony\Component\Security\Core\User\UserProviderInterface $userProvider
     * @return $this|\Symfony\Component\Security\Core\User\UserInterface|null
     * @throws \App\Exception\InvalidFormException
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $facebookResponse = $this->facebookService->setAccessToken($credentials['access_token'])->retrieveUser();
        $facebookUser = $facebookResponse->getDecodedBody();

        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy([
           'facebookId' => $facebookUser['id']
        ]);

        if (!$user instanceof User) {
            $user = $this->facebookRegisterHandler->registerUser(array_merge($facebookUser, [
                'access_token' => $facebookResponse->getAccessToken()
            ]));
        }

        $user->setFacebookAccessToken($facebookResponse->getAccessToken());

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return $user instanceof UserInterface;
    }

    /**
     * @inheritDoc
     */
    public function createAuthenticatedToken(UserInterface $user, $providerKey)
    {
        return new JWTUserToken($user->getRoles(), $user, $this->JWTTokenManager->create($user), $providerKey);
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new Response('Authentication failure', Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return;
    }

    /**
     * @inheritDoc
     */
    public function supportsRememberMe()
    {
        return false;
    }
}
