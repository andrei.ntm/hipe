<?php

namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\PayloadAwareUserProviderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class AuthUserProvider
 * @package App\Security
 */
class AuthUserProvider implements PayloadAwareUserProviderInterface
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    /**
     * AuthUserProvider constructor.
     * @param \Doctrine\ORM\EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @inheritDoc
     */
    public function loadUserByUsername($username)
    {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findUserByUsernameOrEmail($username);

        if (!$user instanceof User) {
            throw new UsernameNotFoundException();
        }

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function loadUserByUsernameAndPayload($username, array $payload)
    {
        $user = $this->em->getRepository(User::class)->find($username);

        if (!$user instanceof User) {
            throw new UsernameNotFoundException();
        }

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function refreshUser(UserInterface $user)
    {
        // TODO: Implement refreshUser() method.
    }

    /**
     * @inheritDoc
     */
    public function supportsClass($class)
    {
        // TODO: Implement supportsClass() method.
    }
}
