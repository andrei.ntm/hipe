<?php

namespace App\Security;

use App\Entity\User;
use App\Handlers\ORM\VkRegisterHandler;
use App\Services\VkService;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AuthenticatorInterface;
use Symfony\Component\Security\Guard\Token\GuardTokenInterface;

class VkAuthenticator implements AuthenticatorInterface
{
    /**
     * @var \App\Services\VkService
     */
    private $vkService;

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    /**
     * @var \App\Handlers\ORM\VkRegisterHandler
     */
    private $vkRegisterHandler;

    /**
     * @var \Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface
     */
    private $JWTTokenManager;

    /**
     * VkAuthenticator constructor.
     * @param \App\Services\VkService $vkService
     * @param \Doctrine\ORM\EntityManagerInterface $em
     * @param \App\Handlers\ORM\VkRegisterHandler $vkRegisterHandler
     * @param \Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface $JWTTokenManager
     */
    public function __construct(
        VkService $vkService,
        EntityManagerInterface $em,
        VkRegisterHandler $vkRegisterHandler,
        JWTTokenManagerInterface $JWTTokenManager
    ) {
        $this->vkService = $vkService;
        $this->em = $em;
        $this->vkRegisterHandler = $vkRegisterHandler;
        $this->JWTTokenManager = $JWTTokenManager;
    }

    /**
     * @inheritDoc
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return;
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request)
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getCredentials(Request $request)
    {
        if (!$request->request->has('access_token')) {
            throw new AuthenticationException();
        }

        return [
            'access_token' => $request->request->get('access_token'),
            'email' => $request->request->has('email') ? $request->request->get('email') : null
        ];
    }

    /**
     * @param mixed $credentials
     * @param \Symfony\Component\Security\Core\User\UserProviderInterface $userProvider
     * @return \App\Entity\User|\Symfony\Component\Security\Core\User\UserInterface|null
     * @throws \App\Exception\InvalidFormException
     * @throws \VK\Exceptions\VKClientException
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $vkResponse = $this->vkService
            ->setAccessToken($credentials['access_token'])
            ->retrieveUser();

        $vkUser = reset($vkResponse);

        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy([
           'vkId' => $vkUser['id']
        ]);


        if (!$user instanceof User) {
            $user = $this->vkRegisterHandler->registerUser(array_merge($vkUser, $credentials));
        }

        $user->setVkAccessToken($credentials['access_token']);

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return $user instanceof UserInterface;
    }

    /**
     * @inheritDoc
     */
    public function createAuthenticatedToken(UserInterface $user, $providerKey)
    {
        return new JWTUserToken($user->getRoles(), $user, $this->JWTTokenManager->create($user), $providerKey);
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new Response('Authentication failure', Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return;
    }

    /**
     * @inheritDoc
     */
    public function supportsRememberMe()
    {
        return false;
    }
}
