<?php

namespace App\Handlers\ORM;

use App\Entity\User;
use FOS\RestBundle\Request\ParamFetcher;

/**
 * Class InfluencerHandler
 * @package App\Handlers\ORM
 */
class InfluencerHandler extends AbstractHandler
{
    protected function getEntityClass()
    {
        return User::class;
    }

    protected function getFormTypeClass($method)
    {
        return;
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return array
     */
    public function getRandomInfluencers(ParamFetcher $paramFetcher)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        return $this->getPaginated(
            $this->repository->getUnfollowRandomQuery($user),
            20
        );
    }
}
