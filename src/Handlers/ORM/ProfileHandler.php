<?php

namespace App\Handlers\ORM;

use App\Entity\User;
use App\Exception\InvalidFormException;
use App\Form\ProfileType;
use App\Mailer\AuthenticationMail;
use Symfony\Component\Form\FormError;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class ProfileHandler
 * @package App\Handlers\ORM
 */
class ProfileHandler extends AbstractHandler
{
    /**
     * @var \Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface
     */
    protected $userPasswordEncoder;

    /**
     * @var \App\Mailer\AuthenticationMail
     */
    protected $authenticationMail;

    /**
     * ProfileHandler constructor.
     * @param \Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface $userPasswordEncoder
     * @param \App\Mailer\AuthenticationMail $authenticationMail
     */
    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder, AuthenticationMail $authenticationMail)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->authenticationMail = $authenticationMail;
    }

    public function getEntityClass()
    {
        return User::class;
    }

    public function getFormTypeClass($method)
    {
        return ProfileType::class;
    }

    /**
     * @return \Symfony\Component\Security\Core\User\UserInterface
     */
    public function getProfile()
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    /**
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendValidateEmail()
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        if (!$user->isEmailVerified()) {
            $user->setEmailVerificationToken(hash('sha256', time().rand().$user->getId()));
            $this->save($user);

            $this->authenticationMail->validateEmail($user);
        }

        return;
    }

    /**
     * @param $parameters
     * @return mixed
     * @throws \App\Exception\InvalidFormException
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function updateProfile($parameters)
    {
        /** @var User $currentUser */
        $currentUser = $this->tokenStorage->getToken()->getUser();
        $oldPassword = $currentUser->getPassword();
        $oldEmail = $currentUser->getEmail();

        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->patch($parameters, false, ['update-profile'], $this->tokenStorage->getToken()->getUser());

        /** @var User $user */
        $user = $form->getData();

        if (array_key_exists('role', $parameters)) {
            if (count($user->getRoles()) > 0) {
                $form->get('role')->addError(new FormError('You already selected a role.'));

                throw new InvalidFormException($form);
            }

            $user->addRole($form->get('role')->getData());
        }

        if (array_key_exists('password', $parameters)) {
            if (!$this->userPasswordEncoder->isPasswordValid($currentUser->setPassword($oldPassword), $form->get('old_password')->getData())) {
                $form->get('old_password')->addError(new FormError('Old password is not correct'));

                throw new InvalidFormException($form);
            }

            $user->setPassword(
                $this->userPasswordEncoder->encodePassword($user, $form->get('password')->getData())
            );
        }

        if (array_key_exists('email', $parameters) && $form->get('email')->getData() !== $oldEmail) {
            $user
                ->setEmailVerified(false)
                ->setEmailVerificationToken(hash('sha256', time().rand().$user->getId()));

            $this->authenticationMail->validateEmail($user);
        }

        if (!$user->isAccountCompleted() && $user->getEmail() && $user->getUsername() && count($user->getRoles()) > 0) {
            $user->setAccountCompleted(true);
        }

        return $this->save($user);
    }
}
