<?php

namespace App\Handlers\ORM;

use App\Entity\Currency;
use App\Entity\User;
use App\Exception\InvalidFormException;
use App\Form\UserType;
use App\Mailer\AuthenticationMail;
use App\Utils\User as UserUtils;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterHandler extends AbstractHandler
{
    /**
     * @var \Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface
     */
    protected $encoder;

    /**
     * @var \Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface
     */
    protected $JWTTokenManager;

    /**
     * @var \App\Mailer\AuthenticationMail
     */
    protected $authenticationMail;

    /**
     * RegisterHandler constructor.
     */
    public function __construct(UserPasswordEncoderInterface $encoder, JWTTokenManagerInterface $JWTTokenManager, AuthenticationMail $authenticationMail)
    {
        $this->encoder = $encoder;
        $this->JWTTokenManager = $JWTTokenManager;
        $this->authenticationMail = $authenticationMail;
    }

    protected function getEntityClass()
    {
        return User::class;
    }

    protected function getFormTypeClass($method)
    {
        return UserType::class;
    }

    /**
     * @param array $parameters
     * @return array
     *
     * @throws \App\Exception\InvalidFormException
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function registerByEmail(array $parameters)
    {
        /** @var Currency $currency */
        $currency = $this->em->getRepository(Currency::class)->findOneBy([
            'code' => 'USD',
        ]);

        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->post(array_merge($parameters, [
            'currency' => $currency->getId(),
        ]), false, ['registration']);

        /** @var User $user */
        $user = $form->getData();

        $user
            ->addRole($form->get('role')->getData())
            ->setPassword(
                $this->encoder->encodePassword($user, $form->get('password')->getData())
            )
            ->setAccountCompleted(true)
            ->setEmailVerificationToken(hash('sha256', time().rand().$user->getId()));

        $this->save($user);

        $this->authenticationMail->validateEmail($user);

        return [
            'user' => $user,
            'token' => $this->JWTTokenManager->create($user),
        ];
    }
}
