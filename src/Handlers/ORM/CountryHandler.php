<?php

namespace App\Handlers\ORM;

use App\Entity\Country;
use Doctrine\Common\Collections\Collection;

/**
 * Class CountryHandler.
 */
class CountryHandler extends AbstractHandler
{
    /**
     * @return string
     */
    protected function getEntityClass()
    {
        return Country::class;
    }

    /**
     * @param $method
     */
    protected function getFormTypeClass($method)
    {
        return;
    }

    /**
     * @return object[]
     */
    public function allCountries()
    {
        return $this->repository->findAll();
    }
}
