<?php

namespace App\Handlers\ORM;

use App\Entity\Comment;
use App\Entity\FeedItem;
use App\Form\CommentType;
use FOS\RestBundle\Request\ParamFetcher;

/**
 * Class CommentHandler
 * @package App\Handlers\ORM
 */
class CommentHandler extends AbstractHandler
{
    /**
     * @return string
     */
    protected function getEntityClass()
    {
        return Comment::class;
    }

    /**
     * @param $method
     * @return string
     */
    protected function getFormTypeClass($method)
    {
        return CommentType::class;
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return array
     */
    public function getCommentsByPost(FeedItem $feedItem, ParamFetcher $paramFetcher)
    {
        return $this->getPaginated(
            $this->repository->getListQuery($feedItem),
            $paramFetcher->get('limit'),
            $paramFetcher->get('offset')
        );
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @param $parameters
     * @return mixed|\Symfony\Component\Form\Form
     * @throws \App\Exception\InvalidFormException
     */
    public function createComment(FeedItem $feedItem, $parameters)
    {
        return $this->post(array_merge($parameters, [
            'feed_item' => $feedItem->getId()
        ]));
    }
}
