<?php

namespace App\Handlers\ORM;

use App\Entity\FeedItem;
use App\Entity\User;
use FOS\RestBundle\Request\ParamFetcher;

/**
 * Class UserHandler
 * @package App\Handlers\ORM
 */
class UserHandler extends AbstractHandler
{
    /**
     * @return string
     */
    protected function getEntityClass()
    {
        return User::class;
    }

    /**
     * @param $method
     */
    protected function getFormTypeClass($method)
    {
        return;
    }

    /**
     * @param \App\Entity\User $user
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return array
     */
    public function getUserFollowers(User $user, ParamFetcher $paramFetcher)
    {
        return $this->getPaginated(
            $this->repository->getFollowersQuery($user),
            $paramFetcher->get('limit'),
            $paramFetcher->get('offset')
        );
    }

    /**
     * @param \App\Entity\User $user
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return array
     */
    public function getUserFollowing(User $user, ParamFetcher $paramFetcher)
    {
        return $this->getPaginated(
            $this->repository->getFollowingQuery($user),
            $paramFetcher->get('limit'),
            $paramFetcher->get('offset')
        );
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return array
     */
    public function getLikesByFeedItem(FeedItem $feedItem, ParamFetcher $paramFetcher)
    {
        return $this->getPaginated(
            $this->repository->getLikesQuery($feedItem),
            $paramFetcher->get('limit'),
            $paramFetcher->get('offset')
        );
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return array
     */
    public function getUserList(ParamFetcher $paramFetcher)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        return $this->getPaginated(
            $this->repository->getUserListQuery($user, $paramFetcher),
            $paramFetcher->get('limit'),
            $paramFetcher->get('offset')
        );
    }
}
