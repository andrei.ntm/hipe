<?php

namespace App\Handlers\ORM;

use App\Entity\BagItem;
use App\Entity\User;
use App\Form\BagItemType;
use App\Helper\QueryBuilder\SortHelper;
use App\Repository\UserRepository;
use App\Services\BagAvailabilityService;
use FOS\RestBundle\Request\ParamFetcher;

/**
 * Class BagHandler
 * @package App\Handlers\ORM
 */
class BagHandler extends AbstractHandler
{
    /**
     * @var BagAvailabilityService $bagAvailabilityService
     */
    private $bagAvailabilityService;

    /**
     * BagHandler constructor.
     * @param \App\Services\BagAvailabilityService $bagAvailabilityService
     */
    public function __construct(BagAvailabilityService $bagAvailabilityService)
    {
        $this->bagAvailabilityService = $bagAvailabilityService;
    }

    /**
     * @return string
     */
    protected function getEntityClass()
    {
        return BagItem::class;
    }

    /**
     * @param $method
     * @return string
     */
    protected function getFormTypeClass($method)
    {
        return BagItemType::class;
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return array
     */
    public function getBagItemsAll(ParamFetcher $paramFetcher)
    {
        /** @var \App\Entity\User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        return $this->getPaginated(
            $this->repository->getListQuery($user),
            $paramFetcher->get('limit'),
            $paramFetcher->get('offset'),
            SortHelper::buildSortCriteriaFromQueryParam($paramFetcher->get('sort'))
        );
    }

    /**
     * @param $parameters
     * @return string
     * @throws \App\Exception\InvalidFormException
     */
    public function addBagItem($parameters)
    {
        /** @var BagItem $bagItemModel */
        $bagItemModel = $this->post($parameters, false)->getData();

        /** @var BagItem $bagItem */
        $bagItem = $this->repository->findOneBy([
            'feedItem' => $bagItemModel->getFeedItem(),
            'color' => $bagItemModel->getColor(),
            'size' => $bagItemModel->getSize()
        ]);

        if ($bagItem instanceof BagItem) {
            $bagItem->setQuantity(
              $bagItem->getQuantity() + $bagItemModel->getQuantity()
            );

            return $this->save($bagItem);
        }

        return $this->save($bagItemModel);
    }

    /**
     * @param \App\Entity\User|null $creator
     * @return \Doctrine\Common\Collections\ArrayCollection
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function availability(User $creator = null)
    {
        /** @var \App\Entity\User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        if ($creator instanceof User) {
            /** @var \Doctrine\ORM\QueryBuilder $bagItemsQuery */
            $bagItemsQuery = $this->repository->getListBySeller($user, $creator);
        } else {
            /** @var \Doctrine\ORM\QueryBuilder $bagItemsQuery */
            $bagItemsQuery = $this->repository->getListQuery($user);
        }

        return $this->bagAvailabilityService->checkBagItemsCollection($bagItemsQuery->getQuery()->getResult());
    }
}
