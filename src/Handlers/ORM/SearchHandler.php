<?php

namespace App\Handlers\ORM;

use App\Helper\QueryBuilder\SortHelper;
use App\Repository\FeedItemRepository;
use App\Repository\UserRepository;
use FOS\RestBundle\Request\ParamFetcher;

/**
 * Class SearchHandler
 * @package App\Handlers\ORM
 */
class SearchHandler extends AbstractHandler
{
    /**
     * @var \App\Repository\UserRepository
     */
    private $userRepository;

    /**
     * @var \App\Repository\FeedItemRepository
     */
    private $feedItemRepository;

    /**
     * SearchHandler constructor.
     * @param \App\Repository\UserRepository $userRepository
     * @param \App\Repository\FeedItemRepository $feedItemRepository
     */
    public function __construct(UserRepository $userRepository, FeedItemRepository $feedItemRepository)
    {
        $this->userRepository = $userRepository;
        $this->feedItemRepository = $feedItemRepository;
    }

    protected function getEntityClass()
    {
        return;
    }

    protected function getFormTypeClass($method)
    {
        return;
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return array
     */
    public function searchResults(ParamFetcher $paramFetcher)
    {
        switch ($paramFetcher->get('type')) {
            case 'products':
                $qb = $this->feedItemRepository->searchProductsQuery($paramFetcher->get('search'));
                break;
            case 'users':
                $qb = $this->userRepository->searchUserQuery($paramFetcher->get('search'));
                break;
        }

        return $this->getPaginated(
            $qb,
            $paramFetcher->get('limit'),
            $paramFetcher->get('offset'),
            SortHelper::buildSortCriteriaFromQueryParam($paramFetcher->get('sort'))
        );
    }
}
