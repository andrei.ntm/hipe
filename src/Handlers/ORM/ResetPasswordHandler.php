<?php

namespace App\Handlers\ORM;

use App\Entity\User;
use App\Exception\InvalidFormException;
use App\Form\Model\ResetPasswordForm;
use App\Form\RequestResetPasswordType;
use App\Form\ResetPasswordFormType;
use App\Mailer\ResetPasswordMail;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class ResetPasswordHandler
 * @package App\Handlers\ORM
 */
class ResetPasswordHandler extends AbstractHandler
{
    /**
     * @var \App\Mailer\ResetPasswordMail
     */
    private $resetPasswordMail;

    /**
     * @var \Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * ResetPasswordHandler constructor.
     * @param \App\Mailer\ResetPasswordMail $resetPasswordMail
     * @param \Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface $encoder
     */
    public function __construct(ResetPasswordMail $resetPasswordMail, UserPasswordEncoderInterface $encoder)
    {
        $this->resetPasswordMail = $resetPasswordMail;
        $this->encoder = $encoder;
    }

    protected function getEntityClass()
    {
        return User::class;
    }

    protected function getFormTypeClass($method)
    {
        return RequestResetPasswordType::class;
    }

    /**
     * @param $parameters
     * @throws \App\Exception\InvalidFormException
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function requestResetPassword($parameters)
    {
        $form = $this->post($parameters, false, ['reset-password']);
        $formData = $form->getData();

        /** @var User $user */
        $user = $this->repository->findOneBy([
            'email' => $formData->getEmail()
        ]);

        if (!$user instanceof User || empty($user->getPassword())) {
            $form->get('email')->addError(new FormError('Email does not exist'));

            throw new InvalidFormException($form);
        }

        $user->setForgotPasswordToken(hash('sha256', time() . rand() . $user->getEmail()));

        $this->em->persist($user);
        $this->em->flush();

        $this->resetPasswordMail->sentResetPasswordMail($user);

        return;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param string $token
     * @return array|\Symfony\Component\Form\FormInterface
     */
    public function getResetPasswordForm(Request $request, string $token)
    {
        /** @var User $user */
        $user = $this->repository->findOneBy([
            'forgotPasswordToken' => $token
        ]);

        if (!$user instanceof User) {
            return [
                'status' => 'user-not-found'
            ];
        }

        $resetPassword = new ResetPasswordForm();

        $form = $this->formFactory->create(ResetPasswordFormType::class, $resetPassword);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user
                ->setPassword(
                    $this->encoder->encodePassword($user, $resetPassword->getPassword())
                )
            ->setForgotPasswordToken(null);

            $this->em->persist($user);
            $this->em->flush();

            return [
                'status' => 'password-changed'
            ];
        }

        return $form;
    }
}
