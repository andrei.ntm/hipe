<?php

namespace App\Handlers\ORM;

use App\Entity\User;
use App\Services\MarketplacePaypalService;

/**
 * Class CheckoutHandler
 * @package App\Handlers\ORM
 */
class CheckoutHandler extends AbstractHandler
{
    /**
     * @var \App\Services\MarketplacePaypalService
     */
    protected $marketplacePaypalService;

    /**
     * CheckoutHandler constructor.
     * @param \App\Services\MarketplacePaypalService $marketplacePaypalService
     */
    public function __construct(MarketplacePaypalService $marketplacePaypalService)
    {
        $this->marketplacePaypalService = $marketplacePaypalService;
    }

    public function getEntityClass()
    {
        return null;
    }

    public function getFormTypeClass($method)
    {
        return null;
    }

    /**
     * @param \App\Entity\User $user
     * @return array
     * @throws \Exception
     */
    public function prepareCheckoutParameters(User $user): array
    {
        if (!in_array('ROLE_INFLUENCER', $user->getRoles())) {
            throw new \Exception('Something went wrong! The current user is not an influencer!');
        }

        return [
            'merchant_id' => $user->getMerchantId(),
            'token' => $this->tokenStorage->getToken()->getCredentials(),
            'client_id' => $this->marketplacePaypalService->getPaypalClientId(),
            'seller_id' => $user->getId()
        ];
    }
}