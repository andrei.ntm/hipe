<?php

namespace App\Handlers\ORM;

use App\Form\Model\UploadMedia\CollectionImages;
use App\Form\CollectionUploadImagesType;
use App\Services\CloudinaryService;
use App\Services\ImageService;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * Class UploadImageHandler.
 */
class UploadImageHandler extends AbstractHandler
{
    /**
     * @var string
     */
    protected $rootPath;

    /**
     * @var CloudinaryService
     */
    protected $cloudinaryService;

    /**
     * UploadImageHandler constructor.
     * @param string $rootPath
     * @param CloudinaryService $cloudinaryService
     */
    public function __construct(string $rootPath, CloudinaryService  $cloudinaryService)
    {
        $this->rootPath = $rootPath;
        $this->cloudinaryService = $cloudinaryService;
    }

    protected function getEntityClass()
    {
        return CollectionImages::class;
    }

    protected function getFormTypeClass($method)
    {
        return CollectionUploadImagesType::class;
    }

    /**
     * @param $parameters
     * @param $host
     *
     * @return array
     *
     * @throws \App\Exception\InvalidFormException
     */
    public function uploadBatchImages($parameters, $host)
    {
        $form = $this->post($parameters, false);

        $images = [];

        /** @var \App\Form\Model\UploadMedia\Image $image */
        foreach ($form->getData()->getImages() as $image) {
            try {
//                $response = $this->cloudinaryService->uploadFile($image->getImage(), null);
//                $newFileName = $response['secure_url'];
                $newFileName = sprintf('%s-%s.%s', uniqid(), date('Y-m-d'), $image->getImage()->guessExtension());

                $image->getImage()->move(
                    sprintf('%s/public/images/upload', $this->rootPath),
                    $newFileName
                );
            } catch (FileException $e) {
                $newFileName = null;
            }

            if (!is_null(($newFileName))) {
//                array_push($images, $newFileName);
                array_push($images, sprintf('%s/images/upload/%s', $host, $newFileName));
            }
        }

        return $images;
    }
}
