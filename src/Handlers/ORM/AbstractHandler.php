<?php

namespace App\Handlers\ORM;

use App\Exception\InvalidFormException;
use App\Helper\QueryBuilder\PaginateHelper;
use App\Helper\QueryBuilder\SortHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\MappingException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class AbstractHandler.
 */
abstract class AbstractHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    protected $repository;

    /**
     * @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    abstract protected function getEntityClass();

    abstract protected function getFormTypeClass($method);

    /**
     * @param \Doctrine\ORM\EntityManagerInterface $em
     * @return $this
     */
    public function setEntityManager(EntityManagerInterface $em)
    {
        $this->em = $em;

        if (class_exists($this->getEntityClass())) {
            try {
                $this->repository = $em->getRepository($this->getEntityClass());
            } catch (\Exception $exception) {
                $this->repository = null;
            }
        }

        return $this;
    }

    /**
     * @param \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage
     * @return $this
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;

        return $this;
    }

    /**
     * @param \Symfony\Component\Form\FormFactoryInterface $formFactory
     * @return $this
     */
    public function setFormFactory(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;

        return $this;
    }

    /**
     * @param \Doctrine\ORM\QueryBuilder|null $qb
     * @param int $limit
     * @param int $offset
     * @param array $sort
     * @return array
     */
    public function getPaginated(QueryBuilder $qb = null, $limit = PaginateHelper::DEFAULT_LIMIT, $offset = 0, $sort = [])
    {
        /** @var QueryBuilder $qb */
        $qb = $qb ?: $this->repository->createQueryBuilder('a');

        $qb = PaginateHelper::apply($qb, $limit, $offset);

        if ($sort) {
            $qb = SortHelper::applyORM($qb, $sort);
        }

        $paginator = new Paginator($qb, true);

        $results = iterator_to_array($paginator);

        return [
            'results' => $results,
            'count' => count($results),
        ];
    }

    /**
     * @param array $parameters
     * @param $method
     * @param null  $entity
     * @param null  $formType
     * @param array $validation_groups
     *
     * @return \Symfony\Component\Form\FormInterface
     *
     * @throws \App\Exception\InvalidFormException
     */
    protected function processForm(array $parameters, $method, $entity = null, $formType = null, $validation_groups = [])
    {
        $entityClass = $this->getEntityClass();

        $entity = $entity ?? new $entityClass();
        $formType = $formType ?? $this->getFormTypeClass($method);

        $form = $this->formFactory->create($formType, $entity, compact('method', 'validation_groups'));
        $form->submit($parameters, 'PATCH' !== $method);

        if ($form->isValid()) {
            return $form;
        }

        throw new InvalidFormException($form);
    }

    /**
     * @param array $parameters
     * @param bool  $save
     * @param null  $entity
     * @param null  $formType
     * @param array $validation_groups
     *
     * @return mixed|\Symfony\Component\Form\Form
     *
     * @throws \App\Exception\InvalidFormException
     */
    public function post(array $parameters, $save = true, $validation_groups = [], $entity = null, $formType = null)
    {
        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->processForm($parameters, 'POST', $entity, $formType, $validation_groups);

        return $save ? $this->save($form->getData()) : $form;
    }

    /**
     * @param array $parameters
     * @param bool  $save
     * @param array $validation_groups
     * @param null  $entity
     * @param null  $formType
     *
     * @return mixed|\Symfony\Component\Form\FormInterface
     *
     * @throws \App\Exception\InvalidFormException
     */
    public function patch(array $parameters, $save = true, $validation_groups = [], $entity = null, $formType = null)
    {
        $form = $this->processForm($parameters, 'PATCH', $entity, $formType, $validation_groups);

        return $save ? $this->save($form->getData()) : $form;
    }

    /**
     * @param array $parameters
     * @param bool $save
     * @param null $entity
     * @param null $formType
     *
     * @return mixed|\Symfony\Component\Form\FormInterface
     *
     * @throws \App\Exception\InvalidFormException
     */
    public function put(array $parameters, $save = true, $entity = null, $formType = null)
    {
        $form = $this->processForm($parameters, 'PUT', $entity, $formType);

        return $save ? $this->save($form->getData()) : $form;
    }

    /**
     * @param $entity
     *
     * @return mixed
     */
    public function save($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }

    /**
     * @param $entity
     */
    public function remove($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }
}
