<?php

namespace App\Handlers\ORM;

use App\Entity\Currency;
use App\Entity\User;
use App\Form\FacebookUserType;
use Symfony\Component\Security\Core\User\UserInterface;

class FacebookRegisterHandler extends AbstractHandler
{
    protected function getEntityClass()
    {
        return User::class;
    }

    protected function getFormTypeClass($method)
    {
        return FacebookUserType::class;
    }

    /**
     * @param array $parameters
     * @return \App\Entity\User
     *
     * @throws \App\Exception\InvalidFormException
     */
    public function registerUser(array $parameters)
    {
        /** @var Currency $currency */
        $currency = $this->em->getRepository(Currency::class)->findOneBy([
            'code' => 'USD',
        ]);

        $data = [
            'fullname' => $parameters['name'],
            'facebook_id' => $parameters['id'],
            'facebook_access_token' => $parameters['access_token'],
            'email' => !empty($parameters['email']) ? $parameters['email'] : null,
            'currency_id' => $currency->getId(),
            'email_verified' => !empty($parameters['email']),
            'email_verification_token' => !empty($parameters['email']) ? null : hash('sha256', time().rand()),
        ];

        return $this->post($data, true, !empty($data['email']) ? ['fb-register-mail'] : ['fb-register']);
    }

    /**
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @return array
     */
    public function auth(UserInterface $user)
    {
        return [
            'user' => $user,
            'token' => $this->tokenStorage->getToken()->getCredentials(),
        ];
    }
}
