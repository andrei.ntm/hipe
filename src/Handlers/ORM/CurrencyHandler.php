<?php

namespace App\Handlers\ORM;

use App\Entity\Currency;
use App\Form\CurrencyType;

/**
 * Class CurrencyHandler.
 */
class CurrencyHandler extends AbstractHandler
{
    /**
     * @return string
     */
    protected function getEntityClass()
    {
        return Currency::class;
    }

    /**
     * @param $method
     * @return string
     */
    protected function getFormTypeClass($method)
    {
        return CurrencyType::class;
    }

    /**
     * @return object[]
     */
    public function getAllCurrencies()
    {
        return $this->repository->findAll();
    }
}
