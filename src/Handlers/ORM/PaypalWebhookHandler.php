<?php

namespace App\Handlers\ORM;

use App\Entity\PaypalResponse;
use App\Entity\User;
use App\Utils\PaypalStatus;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PaypalWebhookHandler
 * @package App\Handlers\ORM
 */
class PaypalWebhookHandler extends AbstractHandler
{
    /**
     * @return string
     */
    public function getEntityClass()
    {
        return User::class;
    }

    /**
     * @param $method
     * @return null
     */
    public function getFormTypeClass($method)
    {
        return null;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function handlePaypalResponse(Request $request)
    {
        $payload = json_decode($request->getContent(), true);

        /** @var User $user */
        $user = $this->repository->find($payload['resource']['tracking_id']);

        switch ($payload['event_type']) {
            case PaypalStatus::MERCHANT_CONSENT_GRANTED:
                $user
                    ->setOnboardingStatus(PaypalStatus::MERCHANT_CONSENT_GRANTED)
                    ->setMerchantId($payload['resource']['merchant_id']);
                break;
            case PaypalStatus::ONBOARDING_COMPLETED:
                $user
                    ->setOnboardingStatus(PaypalStatus::ONBOARDING_COMPLETED)
                    ->setMerchantId($payload['resource']['merchant_id']);
                break;
        }

        $paypalResponse = (new PaypalResponse)
            ->setUser($user)
            ->setContent($request->getContent());


        $this->save($paypalResponse);
    }
}