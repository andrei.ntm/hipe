<?php

namespace App\Handlers\ORM;

use App\Form\Model\PaypalOrderModel;
use App\Form\PaypalOrderType;
use App\Services\CreateOrderService;
use App\Services\MarketplacePaypalService;
use GuzzleHttp\Client as GuzzleClient;

/**
 * Class CreateOrderHandler
 * @package App\Handlers\ORM
 */
class CreateOrderHandler extends AbstractHandler
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleClient;

    /**
     * @var \App\Services\MarketplacePaypalService
     */
    private $marketplacePaypalService;

    /**
     * @var \App\Services\CreateOrderService
     */
    private $createOrderService;

    /**
     * CreateOrderHandler constructor.
     * @param \GuzzleHttp\Client $guzzleClient
     * @param \App\Services\MarketplacePaypalService $marketplacePaypalService
     * @param \App\Services\CreateOrderService $createOrderService
     */
    public function __construct(GuzzleClient $guzzleClient, MarketplacePaypalService $marketplacePaypalService, CreateOrderService $createOrderService)
    {
        $this->guzzleClient = $guzzleClient;
        $this->marketplacePaypalService = $marketplacePaypalService;
        $this->createOrderService = $createOrderService;
    }

    /**
     * @return string
     */
    protected function getEntityClass(): ?string
    {
        return PaypalOrderModel::class;
    }

    /**
     * @param $method
     * @return string
     */
    protected function getFormTypeClass($method): ?string
    {
        return PaypalOrderType::class;
    }

    /**
     * @param $parameters
     * @return mixed
     * @throws \App\Exception\InvalidFormException
     */
    public function initiateOrder($parameters)
    {
        /** @var PaypalOrderModel $paypalItemModel */
        $paypalItemModel = $this->post($parameters, false)->getData();

        $token = $this->marketplacePaypalService->getAccessToken();

        $orderParams = $this->createOrderService->calculateSumFee($paypalItemModel->getSeller());

        $response = $this->guzzleClient->post(sprintf('%s/v2/checkout/orders', $this->marketplacePaypalService->getMarketplacePaypalUrl()), [
            'headers' => [
                'Authorization' => sprintf('Bearer %s', $token),
                'Content-Type' => 'application/json',
                'PayPal-Partner-Attribution-Id' => $paypalItemModel->getSeller()->getMerchantId(),
            ],
            'json' => [
                'intent' => 'CAPTURE',
                'purchase_units' => [
                    [
                        'amount' => [
                            'currency_code' => $paypalItemModel->getSeller()->getCurrency()->getCode(),
                            'value' => $orderParams['sum']
                        ],
                        'payee' => [
                            'merchant_id' => $paypalItemModel->getSeller()->getMerchantId(),
                        ],
                        'payment_instruction' => [
                            'disbursement_mode' => 'INSTANT',
                            'platform_fees' => [
                                [
                                    'amount' => [
                                        'currency_code' => $paypalItemModel->getSeller()->getCurrency()->getCode(),
                                        'value' => $orderParams['fee']
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }
}