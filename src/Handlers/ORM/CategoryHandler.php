<?php

namespace App\Handlers\ORM;

use App\Entity\Category;

class CategoryHandler extends AbstractHandler
{
    /**
     * @return string
     */
    protected function getEntityClass()
    {
        return Category::class;
    }

    /**
     * @param $method
     * @return null
     */
    protected function getFormTypeClass($method)
    {
        return null;
    }

    /**
     * @return object[]
     */
    public function getAllCategories()
    {
        return $this->repository->findBy([], [
            'position' => 'ASC',
        ]);
    }
}
