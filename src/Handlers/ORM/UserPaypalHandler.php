<?php

namespace App\Handlers\ORM;

use App\Entity\User;
use App\Services\MarketplacePaypalService;
use GuzzleHttp\Client as GuzzleClient;

/**
 * Class UserPaypalHandler
 * @package App\Handlers\ORM
 */
class UserPaypalHandler extends AbstractHandler
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $guzzleClient;

    /**
     * @var \App\Services\MarketplacePaypalService
     */
    protected $marketplacePaypalService;

    /**
     * UserPaypalHandler constructor.
     * @param \GuzzleHttp\Client $guzzleClient
     * @param \App\Services\MarketplacePaypalService $marketplacePaypalService
     */
    public function __construct(GuzzleClient $guzzleClient, MarketplacePaypalService $marketplacePaypalService)
    {
        $this->guzzleClient = $guzzleClient;
        $this->marketplacePaypalService = $marketplacePaypalService;
    }

    protected function getEntityClass()
    {
        return User::class;
    }

    protected function getFormTypeClass($method)
    {
        return null;
    }

    /**
     * @return array
     */
    public function getOnboardUrl()
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $token = $this->marketplacePaypalService->getAccessToken();

        $response = $this->guzzleClient->post(sprintf('%s/v2/customer/partner-referrals', $this->marketplacePaypalService->getMarketplacePaypalUrl()), [
            'headers' => [
                'Authorization' => sprintf('Bearer %s', $token),
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'tracking_id' => sprintf("%s", $user->getId()),
                'operations' => [
                    [
                        'operation' => 'API_INTEGRATION',
                        'api_integration_preference' => [
                            'rest_api_integration' => [
                                'integration_method' => 'PAYPAL',
                                'integration_type' => 'THIRD_PARTY',
                                'third_party_details' => [
                                    'features' => ['PAYMENT', 'REFUND', 'PARTNER_FEE']
                                ]
                            ]
                        ]
                    ]
                ],
                'products' => ['EXPRESS_CHECKOUT'],
                'legal_consents' => [
                    [
                        'type' => 'SHARE_DATA_CONSENT',
                        'granted' => true
                    ]
                ]
            ]
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        return [
            'url' => $data['links'][1]['href']
        ];
    }
}