<?php

namespace App\Handlers\ORM;

use App\Entity\User;
use App\Exception\InvalidFormException;
use App\Exception\StandardException;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FollowHandler
 * @package App\Handlers\ORM
 */
class FollowHandler extends AbstractHandler
{
    const FOLLOW = 'follow';
    const UNFOLLOW = 'unfollow';

    protected function getEntityClass()
    {
        return User::class;
    }

    protected function getFormTypeClass($method)
    {
        return;
    }

    /**
     * @param \App\Entity\User $followedUser
     * @return array
     * @throws \App\Exception\StandardException
     */
    public function handleFollow(User $followedUser)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        if ($user === $followedUser) {
            throw new StandardException("You can not follow yourself.", Response::HTTP_BAD_REQUEST);
        }

        $isFollowed = $user->getFollowing()->contains($followedUser);

        if ($isFollowed) {
            $action = self::UNFOLLOW;
            $followedUser->getFollowers()->removeElement($user);
        } else {
            $action = self::FOLLOW;
            $followedUser->getFollowers()->add($user);
        }

        $this->save($followedUser);

        return compact('action');
    }
}
