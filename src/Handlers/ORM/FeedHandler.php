<?php

namespace App\Handlers\ORM;

use App\Entity\FeedItem;
use App\Entity\Image;
use App\Entity\Price;
use App\Entity\User;
use App\Form\FeedItemType;
use App\Form\Model\FeedItem\FeedItem as FeedItemModel;
use App\Helper\QueryBuilder\SortHelper;
use App\Repository\FeedItemRepository;
use App\Services\ItemQuantityService;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class FeedHandler.
 */
class FeedHandler extends AbstractHandler
{
    const LIKED = 'liked';
    const UNLIKED = 'unliked';

    /**
     * @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var \App\Services\ItemQuantityService
     */
    protected $itemQuantityService;

    /**
     * @var \App\Repository\FeedItemRepository
     */
    protected $feedItemRepository;

    /**
     * FeedHandler constructor.
     * @param \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage
     * @param \App\Services\ItemQuantityService $itemQuantityService
     * @param \App\Repository\FeedItemRepository $feedItemRepository
     */
    public function __construct(TokenStorageInterface $tokenStorage, ItemQuantityService $itemQuantityService, FeedItemRepository $feedItemRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->itemQuantityService = $itemQuantityService;
        $this->feedItemRepository = $feedItemRepository;
    }

    protected function getEntityClass()
    {
        return FeedItemModel::class;
    }

    protected function getFormTypeClass($method)
    {
        return FeedItemType::class;
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return array
     */
    public function getFeedList(ParamFetcher $paramFetcher)
    {
        $qb = $this->feedItemRepository->getFeedListQuery(
            $this->tokenStorage->getToken()->getUser()
        );

        $data = $this->getPaginated($qb, $paramFetcher->get('limit'), $paramFetcher->get('offset'));

        if ($paramFetcher->get('limit') > $data['count']) {
            $data['may_like_results'] = $this->feedItemRepository->getFeedListMayLike(
                $this->tokenStorage->getToken()->getUser()
            );
        }

        return $data;
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return array
     */
    public function getProductsList(ParamFetcher $paramFetcher)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        return $this->getPaginated(
            $this->feedItemRepository->getProductsListQuery($user),
            $paramFetcher->get('limit'),
            $paramFetcher->get('offset'),
            SortHelper::buildSortCriteriaFromQueryParam($paramFetcher->get('sort'))
        );
    }

    /**
     * @param \App\Entity\User $user
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return array
     */
    public function getFeedListByUser(User $user, ParamFetcher $paramFetcher)
    {
        return $this->getPaginated(
            $this->feedItemRepository->getFeedListByUserQuery($user),
            $paramFetcher->get('limit'),
            $paramFetcher->get('offset')
        );
    }

    /**
     * @param $parameters
     *
     * @return string
     *
     * @throws \App\Exception\InvalidFormException
     */
    public function addFeedItem($parameters)
    {
        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->post($parameters, false);
        /** @var FeedItemModel $feedItemModel */
        $feedItemModel = $form->getData();

        $this->itemQuantityService->clear()->createQuantityObjects($feedItemModel->getQuantities());

        $price = (new Price())
            ->setAmount($feedItemModel->getPrice()->getAmount())
            ->setCurrency($this->tokenStorage->getToken()->getUser()->getCurrency())
            ->setDomesticShippingAmount($feedItemModel->getPrice()->getDomesticShippingAmount())
            ->setWorldwideShippingAmount($feedItemModel->getPrice()->getWorldwideShippingAmount());

        $feedItem = (new FeedItem())
            ->setTitle($feedItemModel->getTitle())
            ->setDescription($feedItemModel->getDescription())
            ->setCategory($feedItemModel->getCategory())
            ->setCountry($feedItemModel->getLocation())
            ->setWorldwideShipping($feedItemModel->isWorldwideShipping())
            ->setPrice($price);

        foreach ($feedItemModel->getImages() as $rawImage) {
            $feedItem->addImage(
                (new Image())->setPath($rawImage)
            );
        }

        foreach ($this->itemQuantityService->getQuantities() as $quantity) {
            $feedItem->addQuantity($quantity);
        }

        return $this->save($feedItem);
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @return array
     */
    public function handleLike(FeedItem $feedItem)
    {
        /** @var \App\Entity\User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $isLiked = $user->getLikes()->contains($feedItem);

        if ($isLiked) {
            $action = self::UNLIKED;
            $user->removeLike($feedItem);
        } else {
            $action = self::LIKED;
            $user->addLike($feedItem);
        }

        $this->save($user);

        return compact('action');
    }
}
