<?php

namespace App\Handlers\ORM;

use App\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;

class AccountValidationHandler extends AbstractHandler
{
    protected function getEntityClass()
    {
        return User::class;
    }

    protected function getFormTypeClass($method)
    {
        return;
    }

    /**
     * @param $token
     * @return bool
     */
    public function checkUserByToken($token)
    {
        /** @var User $user */
        $user = $this->repository->findOneBy([
            'emailVerificationToken' => $token
        ]);

        if ($user instanceof UserInterface) {
            $user
                ->setEmailVerificationToken(null)
                ->setEmailVerified(true);

            $this->em->persist($user);
            $this->em->flush();
        }

        return $user instanceof User;
    }
}
