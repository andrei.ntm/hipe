<?php

namespace App\Handlers\ORM;

use App\Entity\DeliveryAddress;
use App\Form\DeliveryAddressType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DeliveryAddressHandler
 * @package App\Handlers\ORM
 */
class DeliveryAddressHandler extends AbstractHandler
{
    /**
     * @return string
     */
    protected function getEntityClass()
    {
        return DeliveryAddress::class;
    }

    /**
     * @param $method
     * @return string
     */
    protected function getFormTypeClass($method)
    {
        return DeliveryAddressType::class;
    }

    /**
     * @return object|null
     */
    public function getDeliveryAddress(): ?object
    {
        return $this->repository->findOneBy([
            'createdBy' => $this->tokenStorage->getToken()->getUser()->getId()
        ]);
    }

    /**
     * @param $parameters
     * @return array
     * @throws \App\Exception\InvalidFormException
     */
    public function createOrUpdateDeliveryAddress($parameters): array
    {
        /** @var DeliveryAddress $address */
        $address = $this->getDeliveryAddress();

        if ($address instanceof DeliveryAddress) {
            $address = $this->patch($parameters, true, [], $address);
            $responseCode = Response::HTTP_OK;
        } else {
            $address = $this->post($parameters);
            $responseCode = Response::HTTP_CREATED;
        }

        return [
            'address' => $address,
            'code' => $responseCode,
        ];
    }
}