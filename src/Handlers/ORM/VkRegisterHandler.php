<?php

namespace App\Handlers\ORM;

use App\Entity\Currency;
use App\Entity\User;
use App\Form\VkUserType;
use Symfony\Component\Security\Core\User\UserInterface;

class VkRegisterHandler extends AbstractHandler
{
    protected function getEntityClass()
    {
        return User::class;
    }

    protected function getFormTypeClass($method)
    {
        return VkUserType::class;
    }

    /**
     * @param array $parameters
     * @return \App\Entity\User
     *
     * @throws \App\Exception\InvalidFormException
     */
    public function registerUser(array $parameters)
    {
        /** @var Currency $currency */
        $currency = $this->em->getRepository(Currency::class)->findOneBy([
            'code' => 'USD',
        ]);

        $data = [
            'fullname' => sprintf('%s %s', $parameters['first_name'], $parameters['last_name']),
            'vk_id' => $parameters['id'],
            'vk_access_token' => $parameters['access_token'],
            'email' => $parameters['email'],
            'currency_id' => $currency->getId(),
            'email_verified' => !empty($parameters['email']),
            'email_verification_token' => !empty($parameters['email']) ? null : hash('sha256', time().rand()),
        ];

        return $this->post($data, true, !empty($data['email']) ? ['vk-register-mail'] : ['vk-register']);
    }

    /**
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @return array
     */
    public function auth(UserInterface $user)
    {
        return [
            'user' => $user,
            'token' => $this->tokenStorage->getToken()->getCredentials(),
        ];
    }
}
