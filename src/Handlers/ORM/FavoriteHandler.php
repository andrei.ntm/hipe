<?php

namespace App\Handlers\ORM;

use App\Entity\FeedItem;
use App\Repository\FeedItemRepository;
use FOS\RestBundle\Request\ParamFetcher;
use PhpParser\Node\Param;

/**
 * Class FavoriteHandler
 * @package App\Handlers\ORM
 */
class FavoriteHandler extends AbstractHandler
{
    const FAVORITE = 'favorite';
    const UNFAVORITE = 'unfavorite';

    /**
     * @var \App\Repository\FeedItemRepository
     */
    protected $feedItemRepository;

    /**
     * FavoriteHandler constructor.
     * @param \App\Repository\FeedItemRepository $feedItemRepository
     */
    public function __construct(FeedItemRepository $feedItemRepository)
    {
        $this->feedItemRepository = $feedItemRepository;
    }

    protected function getEntityClass()
    {
        return;
    }

    /**
     * @param $method
     */
    protected function getFormTypeClass($method)
    {
        return;
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @return array
     */
    public function handleFavorite(FeedItem $feedItem)
    {
        /** @var \App\Entity\User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $isFavorite = $user->getFavorites()->contains($feedItem);

        if ($isFavorite) {
            $action = self::UNFAVORITE;
            $user->removeFavorite($feedItem);
        } else {
            $action = self::FAVORITE;
            $user->addFavorite($feedItem);
        }

        $this->save($user);

        return compact('action');
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return array
     */
    public function favoriteList(ParamFetcher $paramFetcher)
    {
        $qb = $this->feedItemRepository->getFavoriteFeedListQuery(
            $this->tokenStorage->getToken()->getUser()
        );

        return $this->getPaginated($qb, $paramFetcher->get('limit'), $paramFetcher->get('offset'));
    }
}
