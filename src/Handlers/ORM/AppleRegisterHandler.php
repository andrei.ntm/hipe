<?php

namespace App\Handlers\ORM;

use App\Entity\Currency;
use App\Entity\User;
use App\Form\AppleUserType;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class AppleRegisterHandler
 * @package App\Handlers\ORM
 */
class AppleRegisterHandler extends AbstractHandler
{
    protected function getEntityClass()
    {
        return User::class;
    }

    protected function getFormTypeClass($method)
    {
        return AppleUserType::class;
    }

    /**
     * @param array $parameters
     * @return \App\Entity\User
     * @throws \App\Exception\InvalidFormException
     */
    public function registerUser(array $parameters)
    {
        /** @var Currency $currency */
        $currency = $this->em->getRepository(Currency::class)->findOneBy([
            'code' => 'USD',
        ]);

        $data = array(
            'email' => $parameters['email'],
            'apple_id' => $parameters['token'],
            'apple_access_token' => $parameters['access_token'],
            'email_verified' => $parameters['email_verified'] && !$parameters['is_private_email'],
            'email_verification_token' => $parameters['email_verified'] && !$parameters['is_private_email'] ? null : hash('sha256', time().rand()),
            'currency_id' => $currency->getId(),
        );

        return $this->post($data, true, ['apple-register']);
    }

    /**
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @return array
     */
    public function auth(UserInterface $user)
    {
        return [
            'user' => $user,
            'token' => $this->tokenStorage->getToken()->getCredentials(),
        ];
    }
}
