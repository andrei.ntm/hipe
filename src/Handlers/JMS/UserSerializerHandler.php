<?php

namespace App\Handlers\JMS;

use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\Metadata\StaticPropertyMetadata;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class UserSerializerHandler
 * @package App\Handlers\JMS
 */
class UserSerializerHandler
{
    /**
     * @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * UserSerializerHandler constructor.
     * @param \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param \JMS\Serializer\EventDispatcher\ObjectEvent $event
     */
    public function onPostSerialize(ObjectEvent $event)
    {
        /** @var \App\Entity\User $loggedUser */
        $loggedUser = $this->tokenStorage->getToken()->getUser();

        /** @var \App\Entity\User $user */
        $user = $event->getObject();

        /** @var \JMS\Serializer\JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();

        if (!$event->getContext()->hasAttribute('groups')) {
            return;
        }

        $groups = $event->getContext()->getAttribute('groups');

        if ((bool)array_intersect(['visit-profile', 'short-info'], $groups)) {
            $visitor->visitProperty(
                new StaticPropertyMetadata('', 'is_followed', null),
                $user->getFollowers()->contains($loggedUser)
            );
        }

        if ((bool)array_intersect(['visit-profile', 'profile'], $groups)) {
            $visitor->visitProperty(
                new StaticPropertyMetadata('', 'followers_count', null),
                $user->getFollowers()->count()
            );

            $visitor->visitProperty(
                new StaticPropertyMetadata('', 'following_count', null),
                $user->getFollowing()->count()
            );

            $visitor->visitProperty(
                new StaticPropertyMetadata('', 'products_count', null),
                $user->getProducts()->count()
            );
        }
    }
}
