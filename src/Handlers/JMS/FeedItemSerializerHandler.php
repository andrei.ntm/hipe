<?php

namespace App\Handlers\JMS;

use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\Metadata\StaticPropertyMetadata;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class FeedItemSerializerHandler
 * @package App\Handlers\JMS
 */
class FeedItemSerializerHandler
{
    /**
     * @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * FeedItemSerializerHandler constructor.
     * @param \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param \JMS\Serializer\EventDispatcher\ObjectEvent $event
     */
    public function onPostSerialize(ObjectEvent $event)
    {
        /** @var \App\Entity\User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        /** @var \App\Entity\FeedItem $feedItem */
        $feedItem = $event->getObject();

        /** @var \JMS\Serializer\JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();

        if (!$event->getContext()->hasAttribute('groups')) {
            return;
        }

        $groups = $event->getContext()->getAttribute('groups');

        if ((bool)array_intersect(['feed-item'], $groups)) {
            $visitor->visitProperty(
                new StaticPropertyMetadata('', 'likes', null),
                $feedItem->getLikedBy()->count()
            );
        }

        if ((bool)array_intersect(['feed-item'], $groups)) {
            $visitor->visitProperty(
                new StaticPropertyMetadata('', 'comments', null),
                $feedItem->getComments()->count()
            );
        }

        if ((bool)array_intersect(['feed-item'], $groups)) {
            $visitor->visitProperty(
                new StaticPropertyMetadata('', 'is_liked', null),
                $user->getLikes()->contains($feedItem)
            );
        }

        if ((bool)array_intersect(['feed-item'], $groups)) {
            $visitor->visitProperty(
                new StaticPropertyMetadata('', 'is_favorite', null),
                $user->getFavorites()->contains($feedItem)
            );
        }
    }
}
