<?php

namespace App\Handlers\JMS;

use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use Symfony\Component\HttpFoundation\RequestStack;

class ImagePathHandler implements SubscribingHandlerInterface
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request|null
     */
    protected $request;

    /**
     * ImagePathHandler constructor.
     * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @return array
     */
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => 'ImageAbsolutePath',
                'method' => 'serializeImageAbsolutePath',
            ],
        ];
    }

    public function serializeImageAbsolutePath(JsonSerializationVisitor $visitor, $url, array $type, Context $context)
    {
        return $url; //sprintf('%s/%s', $this->request->getSchemeAndHttpHost(), $url);
    }
}
