<?php

namespace App\Command\Populate;

use App\Entity\Country;
use App\Helper\TruncateTableHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class PopulateCountriesCommand.
 */
class PopulateCountriesCommand extends Command
{
    protected static $defaultName = 'app:populate:countries';

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;

    /**
     * PopulateCountriesCommand constructor.
     * @param \Doctrine\ORM\EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setDescription('Import database with countries')
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        TruncateTableHandler::handle($this->em, $output, Country::class);

        $rootPath = dirname(__FILE__, 4);

        $countries = include $rootPath.'/public/imports/countries.php';

        foreach ($countries as $code => $name) {
            $io->text(sprintf('New country imported: %s', $name));

            $country = (new Country())
                ->setCode($code)
                ->setName($name);

            $this->em->persist($country);
        }

        $this->em->flush();

        $io->success('Import countries command finished.');

        return 0;
    }
}
