<?php

namespace App\Command\Populate;

use App\Entity\Currency;
use App\Helper\TruncateTableHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class PopulateCurrenciesCommand.
 */
class PopulateCurrenciesCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:populate:currencies';

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;

    /**
     * PopulateCurrenciesCommand constructor.
     * @param \Doctrine\ORM\EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setDescription('Import database with currencies')
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        TruncateTableHandler::handle($this->em, $output, Currency::class);

        $rootPath = dirname(__FILE__, 4);

        $countries = include $rootPath.'/public/imports/currencies.php';

        foreach ($countries as $countryArray) {
            $currency = (new Currency())
                ->setCode($countryArray['code'])
                ->setRate($countryArray['rate'])
                ->setSymbol($countryArray['symbol'])
                ->setTitle($countryArray['currency'])
            ;

            $this->em->persist($currency);
        }

        $this->em->flush();

        $io->success('Import currencies command finished.');

        return 0;
    }
}
