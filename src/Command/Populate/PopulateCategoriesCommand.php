<?php

namespace App\Command\Populate;

use App\Entity\Category;
use App\Helper\TruncateTableHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PopulateCategoriesCommand extends Command
{
    protected static $defaultName = 'app:populate:categories';

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;

    /**
     * PopulateCategoriesCommand constructor.
     * @param \Doctrine\ORM\EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setDescription('Import database with categories')
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        TruncateTableHandler::handle($this->em, $output, Category::class);

        $categoriesKeys = [
            'top-wear', 'bottom-wear', 'shoes', 'accessories', 'beauty', 'technology', 'food',
        ];

        foreach ($categoriesKeys as $index => $categoryKey) {
            $io->text(sprintf('Category added: %s', $categoryKey));

            $category = (new Category())
                ->setPosition($index + 1)
                ->setKey($categoryKey);

            $this->em->persist($category);
        }
        $this->em->flush();

        $io->success('Import categories command finished.');

        return 0;
    }
}
