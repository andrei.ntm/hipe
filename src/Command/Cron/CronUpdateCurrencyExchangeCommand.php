<?php

namespace App\Command\Cron;

use App\Entity\Currency;
use App\Handlers\ORM\CurrencyHandler;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class CronUpdateCurrencyExchangeCommand.
 */
class CronUpdateCurrencyExchangeCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:cron:update-currency-exchange';

    /**
     * @var \GuzzleHttp\ClientInterface
     */
    protected $guzzleClient;

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \App\Handlers\ORM\CurrencyHandler
     */
    protected $currencyHandler;

    /**
     * CronUpdateCurrencyExchangeCommand constructor.
     * @param \GuzzleHttp\ClientInterface $guzzleClient
     * @param \Doctrine\ORM\EntityManagerInterface $em
     * @param \Psr\Log\LoggerInterface $logger
     * @param \App\Handlers\ORM\CurrencyHandler $currencyHandler
     */
    public function __construct(ClientInterface $guzzleClient, EntityManagerInterface $em, LoggerInterface $logger, CurrencyHandler $currencyHandler)
    {
        $this->guzzleClient = $guzzleClient;
        $this->em = $em;
        $this->logger = $logger;
        $this->currencyHandler = $currencyHandler;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $output->writeln([
            '<info>Starting update exchange rates for currencies</info>',
            '<info>=======================================</info>',
        ]);

        /** @var \GuzzleHttp\Psr7\Response $requestApi */
        $requestApi = $this->guzzleClient->get(getenv('EXCHANGE_RATES_API_URL').'?base=USD');
        $requestResult = json_decode($requestApi->getBody()->getContents(), true);

        if (array_key_exists('rates', $requestResult)) {
            $currencies = $this->em->getRepository(Currency::class)->findAll();
            $rates = $requestResult['rates'];

            $this->em->beginTransaction();

            try {
                /** @var Currency $currency */
                foreach ($currencies as $currency) {
                    $currencyUpdated = $this->currencyHandler->patch([
                        'rate' => $rates[$currency->getCode()],
                    ], true, [], $currency);

                    $this->em->persist($currencyUpdated);
                }

                $this->em->flush();
                $this->em->commit();
            } catch (\Exception $exception) {
                $io->error(sprintf('Error: %s', $exception->getMessage()));
                $this->logger->critical(sprintf("<error>Currency '%s' has not been updated. And transaction has been rolled back. </error>", $currency->getCode()));
                $this->em->rollBack();
            }
        }

        $io->success('Command has been finished.');

        return 0;
    }
}
