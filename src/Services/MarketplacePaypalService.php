<?php

namespace App\Services;

use GuzzleHttp\Client as GuzzleClient;

class MarketplacePaypalService
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $guzzleClient;

    /**
     * @var string
     */
    protected $paypalUrl;

    /**
     * @var string
     */
    protected $marketplacePaypalUrl;

    /**
     * @var string
     */
    protected $paypalClientId;

    /**
     * @var string
     */
    protected $paypalSecret;

    /**
     * MarketplacePaypalService constructor.
     * @param \GuzzleHttp\Client $guzzleClient
     */
    public function __construct(GuzzleClient $guzzleClient)
    {
        $this->guzzleClient = $guzzleClient;
    }

    /**
     * @param $paypalUrl
     * @return $this
     */
    public function setPaypalUrl($paypalUrl): self
    {
        $this->paypalUrl = $paypalUrl;

        return $this;
    }

    /**
     * @param $marketplacePaypalUrl
     * @return $this
     */
    public function setMarketplacePaypalUrl($marketplacePaypalUrl): self
    {
        $this->marketplacePaypalUrl = $marketplacePaypalUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getMarketplacePaypalUrl(): string
    {
        return $this->marketplacePaypalUrl;
    }

    /**
     * @param $paypalClientId
     * @return $this
     */
    public function setPaypalClientId($paypalClientId): self
    {
        $this->paypalClientId = $paypalClientId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaypalClientId(): string
    {
        return $this->paypalClientId;
    }

    /**
     * @param $paypalSecret
     * @return $this
     */
    public function setPaypalSecret($paypalSecret): self
    {
        $this->paypalSecret = $paypalSecret;

        return $this;
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getAccessToken(): string
    {
        $response = $this->guzzleClient->post(sprintf('%s/v1/oauth2/token', $this->paypalUrl), [
            'form_params' => [
                'grant_type' => 'client_credentials'
            ],
            'auth' => [
                $this->paypalClientId, $this->paypalSecret
            ]
        ]);

        $data = json_decode($response->getBody()->getContents());

        return $data->access_token;
    }
}