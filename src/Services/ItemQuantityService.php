<?php

namespace App\Services;

use App\Entity\Color;
use App\Entity\Quantity;
use App\Entity\Size;
use App\Form\Model\FeedItem\Quantity as QuantityModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class ItemQuantityService
{
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $sizes;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $colors;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $quantities;

    /**
     * ItemQuantityService constructor.
     */
    public function __construct()
    {
        $this->sizes = new ArrayCollection();
        $this->quantities = new ArrayCollection();
        $this->colors = new ArrayCollection();
    }

    public function getQuantities(): Collection
    {
        return $this->quantities;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $quantitiesCollection
     * @return $this
     */
    public function createQuantityObjects(Collection $quantitiesCollection)
    {
        /** @var QuantityModel $quantityModel */
        foreach ($quantitiesCollection as $quantityModel) {
            $quantity = (new Quantity())
                ->setAmount($quantityModel->getAmount())
                ->setSize($this->checkSize($quantityModel))
                ->setColor($this->checkColors($quantityModel))
            ;

            $this->quantities->add($quantity);
        }

        return $this;
    }

    /**
     * @param \App\Form\Model\FeedItem\Quantity $quantityModel
     * @return \App\Entity\Color|mixed|null
     */
    protected function checkColors(QuantityModel $quantityModel)
    {
        if (!$quantityModel->getColor()) {
            return null;
        }

        $color = (new Color())->setCode($quantityModel->getColor());

        if (in_array($color, $this->colors->toArray())) {
            $color = $this->colors->get(
                array_search($color, $this->colors->toArray())
            );
        } else {
            $this->colors->add($color);
        }

        return $color;
    }

    /**
     * @param \App\Form\Model\FeedItem\Quantity $quantityModel
     * @return \App\Entity\Size|mixed|void|null
     */
    protected function checkSize(QuantityModel $quantityModel)
    {
        if (!$quantityModel->getSize()) {
            return null;
        }

        $size = (new Size())->setNumber($quantityModel->getSize());

        if (in_array($size, $this->sizes->toArray())) {
            $size = $this->sizes->get(
                array_search($size, $this->sizes->toArray())
            );
        } else {
            $this->sizes->add($size);
        }

        return $size;
    }

    /**
     * @return $this
     */
    public function clear(): self
    {
        $this->sizes = new ArrayCollection();
        $this->quantities = new ArrayCollection();
        $this->colors = new ArrayCollection();

        return $this;
    }
}
