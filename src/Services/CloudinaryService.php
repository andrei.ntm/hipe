<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class CloudinaryService
{
    /**
     * CloudinaryService constructor.
     * @param $cloudinaryKey
     */
    public function __construct($cloudinaryKey)
    {
        \Cloudinary::config_from_url($cloudinaryKey);
    }

    /**
     * @param UploadedFile $file
     * @param null $folder
     * @return mixed
     */
    public function uploadFile(UploadedFile $file, $folder = null)
    {
        $mimeParts = explode("/", $file->getMimeType());

        $options = [
            "resource_type" => "auto",
            "folder" => $folder ? $folder : 'defaults',
        ];

        if (!in_array($mimeParts[0], ['image', 'video'])) {
            $options = [
                "resource_type" => "raw",
                "folder" => $folder ? $folder : 'defaults',
                "public_id" => sprintf("%s.%s", bin2hex(openssl_random_pseudo_bytes(8)), $file->getClientOriginalExtension())
            ];
        }

        return \Cloudinary\Uploader::upload_large($file->getPathname(), $options);
    }
}