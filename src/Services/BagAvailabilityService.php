<?php

namespace App\Services;

use App\Entity\BagItem;
use App\Entity\Quantity;
use App\Repository\QuantityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class BagAvailabilityService
 * @package App\Services
 */
class BagAvailabilityService
{
    /**
     * @var \App\Repository\QuantityRepository
     */
    private $quantityRepository;

    /**
     * BagAvailabilityService constructor.
     * @param \App\Repository\QuantityRepository $quantityRepository
     */
    public function __construct(QuantityRepository $quantityRepository)
    {
        $this->quantityRepository = $quantityRepository;
    }

    /**
     * @param $bagItems
     * @return \Doctrine\Common\Collections\ArrayCollection
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function checkBagItemsCollection($bagItems)
    {
        $response = new ArrayCollection();

        /** @var \App\Entity\BagItem $bagItem */
        foreach ($bagItems as $bagItem) {
            $response->add([
                'id' => $bagItem->getId(),
                'status' => $this->checkBagItem($bagItem)
            ]);
        }

        return $response;
    }

    /**
     * @param \App\Entity\BagItem $bagItem
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function checkBagItem(BagItem $bagItem)
    {
        /** @var \App\Entity\Quantity $quantity */
        $quantity = $this->quantityRepository->getByColorOrSize($bagItem);

        if (!$quantity instanceof Quantity) {
            return 'KO';
        }

        if ($bagItem->getQuantity() > $quantity->getAmount()) {
            return 'KO';
        }

        return 'OK';
    }
}