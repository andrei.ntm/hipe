<?php


namespace App\Services;

use Aws\S3\S3Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageService
{
    const PREFIX_LARGE_THUMBNAIL = 'thumbnail600x600';
    const PREFIX_SMALL_THUMBNAIL = 'thumbnail288x150';
    const PREFIX_ORIGINAL = 'original';

    /**
     * @var S3Client
     */
    private $s3Client;

    /**
     * @var string
     */
    private $bucket;

    /**
     * ImageService constructor.
     * @param S3Client $s3Client
     * @param string $bucket
     */
    public function __construct(S3Client $s3Client, $bucket)
    {
        $this->s3Client = $s3Client;
        $this->bucket = $bucket;
    }

    /**
     * @param UploadedFile $file
     * @return array
     */
    public function save(UploadedFile $file)
    {
        $fileName = uniqid();

        return [
            'image_url' => $this->sendToS3(
                file_get_contents($file),
                self::makePath($file, $fileName, self::PREFIX_ORIGINAL),
                $file->getClientMimeType()
            )
//            'large_thumbnail_url' => $this->sendToS3(
//                $this->generateThumbnail($file, 288, 150),
//                self::makePath($file, $fileName, self::PREFIX_LARGE_THUMBNAIL),
//                $file->getClientMimeType()
//            ),
//            'small_thumbnail_url' => $this->sendToS3(
//                self::generateThumbnail($file, 600, 600),
//                self::makePath($file, $fileName, self::PREFIX_SMALL_THUMBNAIL),
//                $file->getClientMimeType()
//            ),
        ];
    }

    /**
     * @param $file
     * @param $path
     * @param $mimeType
     * @return mixed
     */
    private function sendToS3($file, $path, $mimeType)
    {
        return $this->s3Client->putObject([
            'Bucket' => $this->bucket,
            'Key' => $path,
            'Body' => $file,
            'ACL' => 'public-read',
            'ContentType' => $mimeType,
        ])['ObjectURL'];
    }

    /**
     * @param UploadedFile $file
     * @param $width
     * @param $height
     * @return string
     * @throws \ImagickException
     */
    private static function generateThumbnail(UploadedFile $file, $width, $height)
    {
        $picture = new \Imagick();
        $picture->readImageBlob(file_get_contents($file));
        $picture->cropThumbnailImage($width, $height);
        return $picture->getImageBlob();
    }

    /**
     * @param UploadedFile $file
     * @param $name
     * @param string $prefix
     * @return string
     */
    private static function makePath(UploadedFile $file, $name, $prefix = self::PREFIX_ORIGINAL)
    {
        return sprintf('%s_%s.%s', $prefix, $name, $file->getClientOriginalExtension());
    }
}