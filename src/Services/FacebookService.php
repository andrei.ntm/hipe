<?php

namespace App\Services;

use Facebook\Facebook;

class FacebookService
{
    /**
     * @var \Facebook\Facebook
     */
    private $fb;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * FacebookService constructor.
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    public function __construct()
    {
        $this->fb = new Facebook([
            'app_id' => getenv('FB_APP_ID'),
            'app_secret' => getenv('FB_APP_SECRET'),
        ]);
    }

    /**
     * @param string $accessToken
     * @return $this
     */
    public function setAccessToken(string $accessToken): self
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * @return \Facebook\FacebookResponse
     */
    public function retrieveUser()
    {
        try {
            $response = $this->fb->get('/me?fields=id,name,email,picture.type(large)', $this->accessToken);
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        return $response;
    }
}
