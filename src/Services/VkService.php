<?php

namespace App\Services;

use VK\Client\VKApiClient;
use VK\Exceptions\VKApiException;

class VkService
{
    /**
     * @var \VK\Client\VKApiClient
     */
    private $vkApiClient;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * VkService constructor.
     * @param \VK\Client\VKApiClient $VKApiClient
     */
    public function __construct(VKApiClient $VKApiClient)
    {
        $this->vkApiClient = $VKApiClient;
    }

    /**
     * @param string $accessToken
     * @return $this
     */
    public function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * @return mixed
     * @throws \VK\Exceptions\VKClientException
     */
    public function retrieveUser()
    {
        try {
            $response = $this->vkApiClient->users()->get($this->accessToken, [
                'fields' => ['email']
            ]);
        } catch (VKApiException $exception) {
            echo 'Graph returned an error: ' . $exception->getMessage();
            exit;
        }

        return $response;
    }
}
