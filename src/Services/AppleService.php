<?php

namespace App\Services;

use App\Helper\AppleSecretKey;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AppleService
 * @package App\Services
 */
class AppleService
{
    /**
     * @var \Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface
     */
    protected $JWTTokenManager;

    /**
     * @var \GuzzleHttp\ClientInterface
     */
    protected $guzzleClient;

    /**
     * AppleService constructor.
     * @param \Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface $JWTTokenManager
     * @param \GuzzleHttp\ClientInterface $guzzleClient
     */
    public function __construct(JWTTokenManagerInterface $JWTTokenManager, GuzzleClientInterface $guzzleClient)
    {
        $this->JWTTokenManager = $JWTTokenManager;
        $this->guzzleClient = $guzzleClient;
    }

    /**
     * @var string
     */
    protected $accessToken;

    /**
     * @param $accessToken
     * @return $this
     */
    public function setAccessToken($accessToken): self
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * @return array
     */
    public function retrieveUser()
    {
        $userData = $this->tokenVerification();

        return [
            'token' => $userData['body']['sub'],
            'access_token' => $this->accessToken,
            'email' => $userData['body']['email'],
            'email_verified' => filter_var($userData['body']['email_verified'], FILTER_VALIDATE_BOOLEAN),
            'is_private_email' => array_key_exists('is_private_email', $userData['body']) ? filter_var($userData['body']['is_private_email'], FILTER_VALIDATE_BOOLEAN) : false,
        ];
    }

    protected function tokenVerification()
    {
        $result = $this->verificationRequest();

        return $this->decodeJWT($result['id_token']);
    }

    protected function verificationRequest()
    {
        $response = $this->guzzleClient->post('https://appleid.apple.com/auth/token', [
            'form_params' => [
                'client_id' => getenv('APP_CLIENT_ID'),
                'client_secret' => AppleSecretKey::generate(),
                'grant_type' => 'authorization_code',
                'code' => $this->accessToken
            ]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param $token
     * @return array
     */
    protected function decodeJWT($token)
    {
        list($tokenHeader, $tokenBody, $tokenSignature) = explode(".", $token);

        $tokenHeader = base64_decode($tokenHeader);
        $tokenBody = base64_decode($tokenBody);

        return [
            'header' => json_decode($tokenHeader, true),
            'body' => json_decode($tokenBody, true),
            'signature' => $tokenSignature,
        ];
    }
}
