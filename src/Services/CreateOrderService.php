<?php

namespace App\Services;

use App\Entity\BagItem;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CreateOrderService
{
    /**
     * @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $entityManager;

    /**
     * CreateOrderService constructor.
     * @param \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     */
    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $entityManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
    }

    /**
     * @param \App\Entity\User $seller
     * @return int[]
     */
    public function calculateSumFee(User $seller): array
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $bagItems = $this->entityManager->getRepository(BagItem::class)->getListBySeller($user, $seller)->getQuery()->getResult();

        $sum = 0;

        /** @var BagItem $bagItem */
        foreach($bagItems as $bagItem) {
            $sum += $bagItem->getFeedItem()->getPrice()->getAmount();
        }

        return [
            'sum' => $sum,
            'fee' => number_format((10*100)/$sum, 2),
        ];
    }
}