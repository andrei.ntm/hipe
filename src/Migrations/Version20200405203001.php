<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200405203001 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE quantities_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE sizes_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE prices_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE colors_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE feed_items_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE quantities (id INT NOT NULL, feed_item INT DEFAULT NULL, color_id INT DEFAULT NULL, size_id INT DEFAULT NULL, number INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F1B1B53C9F8CCE49 ON quantities (feed_item)');
        $this->addSql('CREATE INDEX IDX_F1B1B53C7ADA1FB5 ON quantities (color_id)');
        $this->addSql('CREATE INDEX IDX_F1B1B53C498DA827 ON quantities (size_id)');
        $this->addSql('CREATE TABLE sizes (id INT NOT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, number VARCHAR(50) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B69E8769DE12AB56 ON sizes (created_by)');
        $this->addSql('CREATE INDEX IDX_B69E876916FE72E1 ON sizes (updated_by)');
        $this->addSql('CREATE TABLE prices (id INT NOT NULL, currency_id INT DEFAULT NULL, amount INT NOT NULL, domestic_shipping_amount INT NOT NULL, worldwide_shipping_amount INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E4CB6D5938248176 ON prices (currency_id)');
        $this->addSql('CREATE TABLE colors (id INT NOT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, code VARCHAR(50) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C2BEC39FDE12AB56 ON colors (created_by)');
        $this->addSql('CREATE INDEX IDX_C2BEC39F16FE72E1 ON colors (updated_by)');
        $this->addSql('CREATE TABLE feed_items (id INT NOT NULL, country_id INT DEFAULT NULL, price_id INT DEFAULT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, description TEXT DEFAULT NULL, worldwide_shipping BOOLEAN DEFAULT \'false\' NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1491BAF1F92F3E70 ON feed_items (country_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1491BAF1D614C7E7 ON feed_items (price_id)');
        $this->addSql('CREATE INDEX IDX_1491BAF1DE12AB56 ON feed_items (created_by)');
        $this->addSql('CREATE INDEX IDX_1491BAF116FE72E1 ON feed_items (updated_by)');
        $this->addSql('ALTER TABLE quantities ADD CONSTRAINT FK_F1B1B53C9F8CCE49 FOREIGN KEY (feed_item) REFERENCES feed_items (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE quantities ADD CONSTRAINT FK_F1B1B53C7ADA1FB5 FOREIGN KEY (color_id) REFERENCES colors (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE quantities ADD CONSTRAINT FK_F1B1B53C498DA827 FOREIGN KEY (size_id) REFERENCES sizes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sizes ADD CONSTRAINT FK_B69E8769DE12AB56 FOREIGN KEY (created_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sizes ADD CONSTRAINT FK_B69E876916FE72E1 FOREIGN KEY (updated_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE prices ADD CONSTRAINT FK_E4CB6D5938248176 FOREIGN KEY (currency_id) REFERENCES currencies (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE colors ADD CONSTRAINT FK_C2BEC39FDE12AB56 FOREIGN KEY (created_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE colors ADD CONSTRAINT FK_C2BEC39F16FE72E1 FOREIGN KEY (updated_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE feed_items ADD CONSTRAINT FK_1491BAF1F92F3E70 FOREIGN KEY (country_id) REFERENCES countries (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE feed_items ADD CONSTRAINT FK_1491BAF1D614C7E7 FOREIGN KEY (price_id) REFERENCES prices (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE feed_items ADD CONSTRAINT FK_1491BAF1DE12AB56 FOREIGN KEY (created_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE feed_items ADD CONSTRAINT FK_1491BAF116FE72E1 FOREIGN KEY (updated_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE quantities DROP CONSTRAINT FK_F1B1B53C498DA827');
        $this->addSql('ALTER TABLE feed_items DROP CONSTRAINT FK_1491BAF1D614C7E7');
        $this->addSql('ALTER TABLE quantities DROP CONSTRAINT FK_F1B1B53C7ADA1FB5');
        $this->addSql('ALTER TABLE quantities DROP CONSTRAINT FK_F1B1B53C9F8CCE49');
        $this->addSql('DROP SEQUENCE quantities_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE sizes_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE prices_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE colors_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE feed_items_id_seq CASCADE');
        $this->addSql('DROP TABLE quantities');
        $this->addSql('DROP TABLE sizes');
        $this->addSql('DROP TABLE prices');
        $this->addSql('DROP TABLE colors');
        $this->addSql('DROP TABLE feed_items');
    }
}
