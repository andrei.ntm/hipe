<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200530191816 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE bag_items_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE bag_items (id INT NOT NULL, feed_item_id INT NOT NULL, color_id INT NOT NULL, size_id INT NOT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, quantity INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1C28CBB0A87D462B ON bag_items (feed_item_id)');
        $this->addSql('CREATE INDEX IDX_1C28CBB07ADA1FB5 ON bag_items (color_id)');
        $this->addSql('CREATE INDEX IDX_1C28CBB0498DA827 ON bag_items (size_id)');
        $this->addSql('CREATE INDEX IDX_1C28CBB0DE12AB56 ON bag_items (created_by)');
        $this->addSql('CREATE INDEX IDX_1C28CBB016FE72E1 ON bag_items (updated_by)');
        $this->addSql('ALTER TABLE bag_items ADD CONSTRAINT FK_1C28CBB0A87D462B FOREIGN KEY (feed_item_id) REFERENCES feed_items (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bag_items ADD CONSTRAINT FK_1C28CBB07ADA1FB5 FOREIGN KEY (color_id) REFERENCES colors (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bag_items ADD CONSTRAINT FK_1C28CBB0498DA827 FOREIGN KEY (size_id) REFERENCES sizes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bag_items ADD CONSTRAINT FK_1C28CBB0DE12AB56 FOREIGN KEY (created_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bag_items ADD CONSTRAINT FK_1C28CBB016FE72E1 FOREIGN KEY (updated_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE bag_items_id_seq CASCADE');
        $this->addSql('DROP TABLE bag_items');
    }
}
