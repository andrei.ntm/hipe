<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200501180220 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE followers_id_seq CASCADE');
        $this->addSql('DROP INDEX users_follow');
        $this->addSql('ALTER TABLE followers ALTER user_id SET NOT NULL');
        $this->addSql('ALTER TABLE followers ALTER follow_user_id SET NOT NULL');
        $this->addSql('ALTER TABLE followers ADD PRIMARY KEY (follow_user_id, user_id)');
        $this->addSql('ALTER TABLE prices ALTER amount TYPE DOUBLE PRECISION');
        $this->addSql('ALTER TABLE prices ALTER amount DROP DEFAULT');
        $this->addSql('ALTER TABLE prices ALTER domestic_shipping_amount TYPE DOUBLE PRECISION');
        $this->addSql('ALTER TABLE prices ALTER domestic_shipping_amount DROP DEFAULT');
        $this->addSql('ALTER TABLE prices ALTER worldwide_shipping_amount TYPE DOUBLE PRECISION');
        $this->addSql('ALTER TABLE prices ALTER worldwide_shipping_amount DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE followers_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('ALTER TABLE prices ALTER amount TYPE INT');
        $this->addSql('ALTER TABLE prices ALTER amount DROP DEFAULT');
        $this->addSql('ALTER TABLE prices ALTER domestic_shipping_amount TYPE INT');
        $this->addSql('ALTER TABLE prices ALTER domestic_shipping_amount DROP DEFAULT');
        $this->addSql('ALTER TABLE prices ALTER worldwide_shipping_amount TYPE INT');
        $this->addSql('ALTER TABLE prices ALTER worldwide_shipping_amount DROP DEFAULT');
        $this->addSql('DROP INDEX "primary"');
        $this->addSql('ALTER TABLE followers ALTER follow_user_id DROP NOT NULL');
        $this->addSql('ALTER TABLE followers ALTER user_id DROP NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX users_follow ON followers (user_id, follow_user_id)');
    }
}
