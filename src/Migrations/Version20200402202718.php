<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200402202718 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE categories_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE countries_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE users_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE currencies_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE cron_job_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE cron_report_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE categories (id INT NOT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, key VARCHAR(50) NOT NULL, position INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3AF346688A90ABA9 ON categories (key)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3AF34668462CE4F5 ON categories (position)');
        $this->addSql('CREATE INDEX IDX_3AF34668DE12AB56 ON categories (created_by)');
        $this->addSql('CREATE INDEX IDX_3AF3466816FE72E1 ON categories (updated_by)');
        $this->addSql('CREATE TABLE countries (id INT NOT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, code VARCHAR(100) NOT NULL, name VARCHAR(100) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5D66EBAD77153098 ON countries (code)');
        $this->addSql('CREATE INDEX IDX_5D66EBADDE12AB56 ON countries (created_by)');
        $this->addSql('CREATE INDEX IDX_5D66EBAD16FE72E1 ON countries (updated_by)');
        $this->addSql('CREATE TABLE users (id INT NOT NULL, username VARCHAR(100) DEFAULT NULL, facebook_id VARCHAR(100) DEFAULT NULL, facebook_access_token VARCHAR(255) DEFAULT NULL, vk_id VARCHAR(100) DEFAULT NULL, vk_access_token VARCHAR(255) DEFAULT NULL, fullname VARCHAR(100) DEFAULT NULL, type VARCHAR(100) DEFAULT NULL, email VARCHAR(100) DEFAULT NULL, email_verified BOOLEAN NOT NULL, email_verification_token VARCHAR(100) DEFAULT NULL, account_completed BOOLEAN NOT NULL, description VARCHAR(255) DEFAULT NULL, instagram_account VARCHAR(50) DEFAULT NULL, youtube_account VARCHAR(50) DEFAULT NULL, tiktok_account VARCHAR(50) DEFAULT NULL, suspended BOOLEAN NOT NULL, password VARCHAR(100) DEFAULT NULL, salt VARCHAR(255) NOT NULL, roles TEXT NOT NULL, forgot_password_token VARCHAR(100) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9F85E0677 ON users (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9E7927C74 ON users (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9C4995C67 ON users (email_verification_token)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E98625E4A2 ON users (forgot_password_token)');
        $this->addSql('COMMENT ON COLUMN users.roles IS \'(DC2Type:simple_array)\'');
        $this->addSql('CREATE TABLE currencies (id INT NOT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, code VARCHAR(20) NOT NULL, title VARCHAR(100) NOT NULL, rate NUMERIC(15, 10) NOT NULL, symbol VARCHAR(10) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_37C4469377153098 ON currencies (code)');
        $this->addSql('CREATE INDEX IDX_37C44693DE12AB56 ON currencies (created_by)');
        $this->addSql('CREATE INDEX IDX_37C4469316FE72E1 ON currencies (updated_by)');
        $this->addSql('CREATE TABLE cron_job (id INT NOT NULL, name VARCHAR(191) NOT NULL, command VARCHAR(1024) NOT NULL, schedule VARCHAR(191) NOT NULL, description VARCHAR(191) NOT NULL, enabled BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX un_name ON cron_job (name)');
        $this->addSql('CREATE TABLE cron_report (id INT NOT NULL, job_id INT DEFAULT NULL, run_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, run_time DOUBLE PRECISION NOT NULL, exit_code INT NOT NULL, output TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B6C6A7F5BE04EA9 ON cron_report (job_id)');
        $this->addSql('ALTER TABLE categories ADD CONSTRAINT FK_3AF34668DE12AB56 FOREIGN KEY (created_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE categories ADD CONSTRAINT FK_3AF3466816FE72E1 FOREIGN KEY (updated_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE countries ADD CONSTRAINT FK_5D66EBADDE12AB56 FOREIGN KEY (created_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE countries ADD CONSTRAINT FK_5D66EBAD16FE72E1 FOREIGN KEY (updated_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE currencies ADD CONSTRAINT FK_37C44693DE12AB56 FOREIGN KEY (created_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE currencies ADD CONSTRAINT FK_37C4469316FE72E1 FOREIGN KEY (updated_by) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cron_report ADD CONSTRAINT FK_B6C6A7F5BE04EA9 FOREIGN KEY (job_id) REFERENCES cron_job (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE categories DROP CONSTRAINT FK_3AF34668DE12AB56');
        $this->addSql('ALTER TABLE categories DROP CONSTRAINT FK_3AF3466816FE72E1');
        $this->addSql('ALTER TABLE countries DROP CONSTRAINT FK_5D66EBADDE12AB56');
        $this->addSql('ALTER TABLE countries DROP CONSTRAINT FK_5D66EBAD16FE72E1');
        $this->addSql('ALTER TABLE currencies DROP CONSTRAINT FK_37C44693DE12AB56');
        $this->addSql('ALTER TABLE currencies DROP CONSTRAINT FK_37C4469316FE72E1');
        $this->addSql('ALTER TABLE cron_report DROP CONSTRAINT FK_B6C6A7F5BE04EA9');
        $this->addSql('DROP SEQUENCE categories_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE countries_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE users_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE currencies_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE cron_job_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE cron_report_id_seq CASCADE');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE countries');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE currencies');
        $this->addSql('DROP TABLE cron_job');
        $this->addSql('DROP TABLE cron_report');
    }
}
