<?php

namespace App\Controller;

use App\Handlers\ORM\UploadImageHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOS;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class UploadImageController.
 *
 * @SWG\Tag(name="Upload images")
 * @FOS\Route("/api/upload-images")
 */
class UploadImageController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\UploadImageHandler
     */
    protected $uploadImageHandler;

    /**
     * UploadImageController constructor.
     * @param \App\Handlers\ORM\UploadImageHandler $uploadImageHandler
     */
    public function __construct(UploadImageHandler $uploadImageHandler)
    {
        $this->uploadImageHandler = $uploadImageHandler;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \App\Exception\InvalidFormException
     * @FOS\Post("/collection", name="post_collection_images")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="multipart/form-data")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Post(path="/api/upload-images/collection")
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Upload fields",
     *     @Model(type=App\Form\CollectionUploadImagesType::class)
     * )
     * @SWG\Response(
     *     response=201,
     *     description="Upload media file",
     *     @SWG\Schema(type="array",
     *          @SWG\Items(
     *          		type="object",
     *              	@SWG\Property(type="file", property="image")
     *          )
     *     )
     * )
     */
    public function cpostImagesAction(Request $request): Response
    {
        $data = $this->uploadImageHandler->uploadBatchImages($request->files->all(), $request->getSchemeAndHttpHost());

        $view = $this->view($data, Response::HTTP_CREATED);

        return $this->handleView($view);
    }
}
