<?php

namespace App\Controller;

use App\Entity\FeedItem;
use App\Handlers\ORM\FeedHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as FOS;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * Class LikeController
 * @package App\Controller
 * @SWG\Tag(name="Like")
 * @FOS\Route("/api/likes")
 */
class LikeController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\FeedHandler
     */
    private $feedHandler;

    /**
     * LikeController constructor.
     * @param \App\Handlers\ORM\FeedHandler $feedHandler
     */
    public function __construct(FeedHandler $feedHandler)
    {
        $this->feedHandler = $feedHandler;
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @return \Symfony\Component\HttpFoundation\Response
     * @FOS\Get("/{id}", name="get_like")
     *
     * @ParamConverter("feedItem", options={"id" = "id"}, class="App\Entity\FeedItem")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Get(path="/api/likes/{id}")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Feed item id resource"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Favorite feed item status",
     *     @SWG\Schema(type="object",
     *          @SWG\Property(type="string", property="action")
     *     )
     * )
     */
    public function getLikeAction(FeedItem $feedItem)
    {
        $data = $this->feedHandler->handleLike($feedItem);

        $view = $this->view($data, Response::HTTP_OK);
        return $this->handleView($view);
    }
}
