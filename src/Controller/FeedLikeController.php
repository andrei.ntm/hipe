<?php

namespace App\Controller;

use App\Entity\FeedItem;
use App\Handlers\ORM\UserHandler;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as FOS;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * Class FeedLikeController
 * @package App\Controller
 * @SWG\Tag(name="Feed like")
 * @FOS\Route("/api/feed")
 */
class FeedLikeController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\UserHandler
     */
    private $userHandler;

    /**
     * FeedLikeController constructor.
     * @param \App\Handlers\ORM\UserHandler $userHandler
     */
    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("/{id}/likes", name="get_feed_likes")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="multipart/form-data")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @FOS\QueryParam(name="limit", requirements="\d+", default="1000", description="Maximum results")
     * @FOS\QueryParam(name="offset", requirements="\d+", default="0", description="Offset of the overview")
     *
     * @ParamConverter("feedItem", options={"id" = "id"}, class="App\Entity\FeedItem")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Feed item id resource"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns feed item collection.",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="results", type="array",
     *              @SWG\Items(ref=@Model(type=App\Entity\FeedItem::class))
     *          ),
     *          @SWG\Property(property="count", type="integer")
     *     )
     * )
     */
    public function cgetFeedLikeAction(FeedItem $feedItem, ParamFetcher $paramFetcher)
    {
        $data = $this->userHandler->getLikesByFeedItem($feedItem, $paramFetcher);

        $view = $this->view($data, Response::HTTP_OK)->setContext(
            (new Context())->setGroups(["short-info"])
        );
        return $this->handleView($view);
    }
}
