<?php

namespace App\Controller;

use App\Handlers\ORM\CategoryHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOS;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CategoryController.
 *
 * @SWG\Tag(name="Category")
 * @FOS\Route("/api/categories")
 */
class CategoryController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\CategoryHandler
     */
    private $categoryHandler;

    /**
     * CategoryController constructor.
     * @param \App\Handlers\ORM\CategoryHandler $categoryHandler
     */
    public function __construct(CategoryHandler $categoryHandler)
    {
        $this->categoryHandler = $categoryHandler;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("", name="categories_all")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Get(path="/api/categories")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Get cagegories collection",
     *     @Model(type=App\Entity\Category::class)
     * )
     */
    public function cgetCategoriesAction()
    {
        $view = $this->view($this->categoryHandler->getAllCategories(), Response::HTTP_OK);

        return $this->handleView($view);
    }
}
