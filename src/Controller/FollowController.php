<?php

namespace App\Controller;

use App\Entity\User;
use App\Handlers\ORM\FollowHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as FOS;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * Class FollowController
 * @package App\Controller
 * @SWG\Tag(name="Follow")
 * @FOS\Route("/api/follow")
 */
class FollowController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\FollowHandler
     */
    protected $followHandler;

    /**
     * FollowController constructor.
     * @param \App\Handlers\ORM\FollowHandler $followHandler
     */
    public function __construct(FollowHandler $followHandler)
    {
        $this->followHandler = $followHandler;
    }

    /**
     * @param \App\Entity\User $user
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\StandardException
     *
     * @FOS\Get("/{id}", name="get_follow_user")
     *
     * @ParamConverter("user", options={"id" = "id"}, class="App\Entity\User")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Get(path="/api/follow/{id}")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="User id resource"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Follow user status",
     *     @SWG\Schema(type="object",
     *          @SWG\Property(type="string", property="action")
     *     )
     * )
     */
    public function getFollowAction(User $user)
    {
        $data = $this->followHandler->handleFollow($user);

        $view = $this->view($data, Response::HTTP_OK);
        return $this->handleView($view);
    }
}
