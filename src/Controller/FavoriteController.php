<?php

namespace App\Controller;

use App\Entity\FeedItem;
use App\Handlers\ORM\FavoriteHandler;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as FOS;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FavoriteController
 * @package App\Controller
 * @SWG\Tag(name="Favorite")
 * @FOS\Route("/api/favorites")
 */
class FavoriteController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\FavoriteHandler
     */
    private $favoriteHandler;

    /**
     * FavoriteController constructor.
     * @param \App\Handlers\ORM\FavoriteHandler $favoriteHandler
     */
    public function __construct(FavoriteHandler $favoriteHandler)
    {
        $this->favoriteHandler = $favoriteHandler;
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("", name="get_favorites")
     *
     * @FOS\QueryParam(name="limit", requirements="\d+", default="1000", description="Maximum results")
     * @FOS\QueryParam(name="offset", requirements="\d+", default="0", description="Offset of the overview")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Get(path="/api/favorites")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the favorites list",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="results", type="array",
     *              @SWG\Items(ref=@Model(type=App\Entity\FeedItem::class))
     *          ),
     *          @SWG\Property(property="count", type="integer")
     *      )
     * )
     */
    public function getFavoriteListAction(ParamFetcher $paramFetcher)
    {
        $data = $this->favoriteHandler->favoriteList($paramFetcher);

        $view = $this->view($data, Response::HTTP_OK)->setContext(
            (new Context())->addGroups(['feed-item', 'guest-profile'])
        );
        return $this->handleView($view);
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @return \Symfony\Component\HttpFoundation\Response
     * @FOS\Get("/handle/{id}", name="get_handle_favorite")
     *
     * @ParamConverter("feedItem", options={"id" = "id"}, class="App\Entity\FeedItem")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Get(path="/api/favorites/handle/{id}")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Feed item id resource"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Favorite feed item status",
     *     @SWG\Schema(type="object",
     *          @SWG\Property(type="string", property="action")
     *     )
     * )
     */
    public function getFavoriteAction(FeedItem $feedItem)
    {
        $data = $this->favoriteHandler->handleFavorite($feedItem);

        $view = $this->view($data, Response::HTTP_OK);
        return $this->handleView($view);
    }
}
