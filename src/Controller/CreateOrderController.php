<?php

namespace App\Controller;

use App\Handlers\ORM\CreateOrderHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as FOS;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CreateOrderController
 * @package App\Controller
 * @SWG\Tag(name="Create order")
 * @FOS\Route("/api/create-order")
 */
class CreateOrderController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\CreateOrderHandler
     */
    private $createOrderHandler;

    /**
     * CreateOrderController constructor.
     * @param \App\Handlers\ORM\CreateOrderHandler $createOrderHandler
     */
    public function __construct(CreateOrderHandler $createOrderHandler)
    {
        $this->createOrderHandler = $createOrderHandler;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\InvalidFormException
     *
     * @FOS\Post("", name="post_create_order")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Create order resource",
     *     @Model(type=App\Form\CreateOrderType::class)
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns payment id.",
     *    @SWG\Schema(type="object",
     *          @SWG\Property(property="id", type="string")
     *     )
     * )
     */
    public function postCreateOrderController(Request $request): ?Response
    {
        $response = $this->createOrderHandler->initiateOrder($request->request->all());

        $view = $this->view($response, Response::HTTP_CREATED);
        return $this->handleView($view);
    }
}