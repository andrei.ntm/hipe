<?php

namespace App\Controller;

use App\Handlers\ORM\CheckoutHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use App\Entity\User;
use FOS\RestBundle\Controller\Annotations as FOS;

/**
 * Class CheckoutController
 * @package App\Controller
 * @SWG\Tag(name="Checkout")
 * @FOS\Route("/api/checkout")
 */
class CheckoutController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\CheckoutHandler
     */
    protected $checkoutHandler;

    /**
     * CheckoutController constructor.
     * @param \App\Handlers\ORM\CheckoutHandler $checkoutHandler
     */
    public function __construct(CheckoutHandler $checkoutHandler)
    {
        $this->checkoutHandler = $checkoutHandler;
    }

    /**
     * @param \App\Entity\User $user
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     *
     * @FOS\Get("/{seller_id}", name="get_checkout")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @ParamConverter("user", options={"id" = "seller_id"}, class="App\Entity\User")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns checkout link.",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="url", type="string")
     *     )
     * )
     */
    public function getCheckoutAction(User $user): Response
    {
        $parameters = $this->checkoutHandler->prepareCheckoutParameters($user);

        return $this->render('paypal/checkout.html.twig', $parameters);
    }
}