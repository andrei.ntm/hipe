<?php

namespace App\Controller;

use App\Entity\BagItem;
use App\Handlers\ORM\BagHandler;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as FOS;

/**
 * Class BagController
 * @package App\Controller
 * @SWG\Tag(name="Bag")
 * @FOS\Route("/api/bag")
 */
class BagController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\BagHandler
     */
    private $bagHandler;

    /**
     * BagController constructor.
     * @param \App\Handlers\ORM\BagHandler $bagHandler
     */
    public function __construct(BagHandler $bagHandler)
    {
        $this->bagHandler = $bagHandler;
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("", name="get_bag_item_collection")
     *
     * @FOS\QueryParam(name="limit", requirements="\d+", default="1000", description="Maximum results")
     * @FOS\QueryParam(name="offset", requirements="\d+", default="0", description="Offset of the overview")
     * @FOS\QueryParam(name="sort", description="Sort parameter with the format ?sort=id,-status")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns bag item collection.",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="results", type="array",
     *              @SWG\Items(ref=@Model(type=App\Entity\BagItem::class))
     *          ),
     *          @SWG\Property(property="count", type="integer")
     *     )
     * )
     */
    public function cgetBagAction(ParamFetcher $paramFetcher)
    {
        $data = $this->bagHandler->getBagItemsAll($paramFetcher);

        $view = $this->view($data, Response::HTTP_OK)->setContext(
            (new Context())->addGroups(['bag-item', 'feed-item', 'profile'])
        );
        return $this->handleView($view);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\InvalidFormException
     *
     * @FOS\Post("", name="post_bag_item")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Create bag item resource",
     *     @Model(type=App\Form\BagItemType::class)
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns bag item resource.",
     *     @Model(type=App\Entity\BagItem::class)
     * )
     */
    public function postBagAction(Request $request)
    {
        $data = $this->bagHandler->addBagItem($request->request->all());

        $view = $this->view($data, Response::HTTP_CREATED)->setContext(
            (new Context())->addGroups(['bag-item', 'feed-item', 'profile'])
        );
        return $this->handleView($view);
    }

    /**
     * @param \App\Entity\BagItem $bagItem
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\InvalidFormException
     *
     * @FOS\Put("/{id}", name="put_bag_item")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @ParamConverter("bagItem", options={"id" = "id"}, class="App\Entity\BagItem")
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Update bag item  resource",
     *     @Model(type=App\Form\BagItemType::class)
     * )
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="BagItem id resource"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns bag item resource.",
     *     @Model(type=App\Entity\BagItem::class)
     * )
     */
    public function putBagAction(BagItem $bagItem, Request $request)
    {
        $data = $this->bagHandler->patch($request->request->all(), true, [], $bagItem);

        $view = $this->view($data, Response::HTTP_OK)->setContext(
            (new Context())->addGroups(['bag-item', 'feed-item', 'profile'])
        );
        return $this->handleView($view);
    }

    /**
     * @param \App\Entity\BagItem $bagItem
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Delete("/{id}", name="delege_bag_item")
     *
     * @ParamConverter("bagItem", options={"id" = "id"}, class="App\Entity\BagItem")
     *
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="BagItem id resource"
     * )
     *
     * @SWG\Response(
     *     response=204,
     *      description="Delete bag item resource"
     * )
     */
    public function deleteBagAction(BagItem $bagItem)
    {
        $this->bagHandler->remove($bagItem);

        $view = $this->view([], Response::HTTP_NO_CONTENT);
        return $this->handleView($view);
    }
}
