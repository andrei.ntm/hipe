<?php

namespace App\Controller;

use App\Handlers\ORM\CurrencyHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOS;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CurrencyController.
 *
 * @SWG\Tag(name="Currencies")
 * @FOS\Route("/api/currencies")
 */
class CurrencyController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\CurrencyHandler
     */
    protected $currencyHandler;

    /**
     * CurrencyController constructor.
     * @param \App\Handlers\ORM\CurrencyHandler $currencyHandler
     */
    public function __construct(CurrencyHandler $currencyHandler)
    {
        $this->currencyHandler = $currencyHandler;
    }

    /**
     * @FOS\Get("", name="get_currencies_all")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Get(path="/api/currencies")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Get currencies collection",
     *     @Model(type=App\Entity\Currency::class)
     * )
     */
    public function cgetCurrenciesAction(): Response
    {
        $data = $this->currencyHandler->getAllCurrencies();

        $view = $this->view($data, Response::HTTP_OK);

        return $this->handleView($view);
    }
}
