<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Handlers\ORM\ProfileHandler;
use App\Handlers\ORM\UserHandler;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOS;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserFollowersController
 * @package App\Controller\User
 * @SWG\Tag(name="User followers")
 * @FOS\Route("/api/users/{id}")
 */
class UserFollowersController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\UserHandler
     */
    private $userHandler;

    /**
     * UserFollowersController constructor.
     * @param \App\Handlers\ORM\UserHandler $userHandler
     */
    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * @param \App\Entity\User $user
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("/followers")
     *
     * @FOS\QueryParam(name="limit", requirements="\d+", default="1000", description="Maximum results")
     * @FOS\QueryParam(name="offset", requirements="\d+", default="0", description="Offset of the overview")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @ParamConverter("user", options={"id" = "id"}, class="App\Entity\User")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="User id resource"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="User followers list",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="results", type="array",
     *              @SWG\Items(ref=@Model(type=App\Entity\User::class))
     *          ),
     *          @SWG\Property(property="count", type="integer")
     *      )
     * )
     */
    public function cgetUserFollowersAction(User $user, ParamFetcher $paramFetcher)
    {
        $data = $this->userHandler->getUserFollowers($user, $paramFetcher);

        $view = $this->view($data, Response::HTTP_OK)->setContext(
            (new Context())->setGroups(["short-info"])
        );
        return $this->handleView($view);
    }

    /**
     * @param \App\Entity\User $user
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("/following")
     *
     * @FOS\QueryParam(name="limit", requirements="\d+", default="1000", description="Maximum results")
     * @FOS\QueryParam(name="offset", requirements="\d+", default="0", description="Offset of the overview")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @ParamConverter("user", options={"id" = "id"}, class="App\Entity\User")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="User id resource"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="User following list",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="results", type="array",
     *              @SWG\Items(ref=@Model(type=App\Entity\User::class))
     *          ),
     *          @SWG\Property(property="count", type="integer")
     *      )
     * )
     */
    public function cgetUserFollowingAction(User $user, ParamFetcher $paramFetcher)
    {
        $data = $this->userHandler->getUserFollowing($user, $paramFetcher);

        $view = $this->view($data, Response::HTTP_OK)->setContext(
            (new Context())->setGroups(["short-info"])
        );
        return $this->handleView($view);
    }
}
