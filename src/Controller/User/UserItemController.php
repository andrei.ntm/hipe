<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Handlers\ORM\FeedHandler;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as FOS;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * Class UserItemController
 * @package App\Controller\User
 * @SWG\Tag(name="User items")
 * @FOS\Route("/api/users/{id}/items")
 */
class UserItemController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\FeedHandler
     */
    private $feedHandler;

    /**
     * UserItemController constructor.
     * @param \App\Handlers\ORM\FeedHandler $feedHandler
     */
    public function __construct(FeedHandler $feedHandler)
    {
        $this->feedHandler = $feedHandler;
    }

    /**
     * @param \App\Entity\User $user
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("")
     *
     * @FOS\QueryParam(name="limit", requirements="\d+", default="1000", description="Maximum results")
     * @FOS\QueryParam(name="offset", requirements="\d+", default="0", description="Offset of the overview")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @ParamConverter("user", options={"id" = "id"}, class="App\Entity\User")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="User id resource"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="User items list",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="results", type="array",
     *              @SWG\Items(ref=@Model(type=App\Entity\FeedItem::class))
     *          ),
     *          @SWG\Property(property="count", type="integer")
     *      )
     * )
     */
    public function cgetUserItemsAction(User $user, ParamFetcher $paramFetcher)
    {
        $data = $this->feedHandler->getFeedListByUser($user, $paramFetcher);

        $view = $this->view($data, Response::HTTP_OK)->setContext(
            (new Context())->addGroups(['feed-item', 'guest-profile'])
        );
        return $this->handleView($view);
    }
}
