<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Handlers\ORM\UserPaypalHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as FOS;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserPaypalController
 * @package App\Controller\User
 * @SWG\Tag(name="User Paypal")
 * @FOS\Route("/api/users/paypal")
 */
class UserPaypalController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\UserPaypalHandler
     */
    private $userPaypalHandler;

    /**
     * UserPaypalController constructor.
     * @param \App\Handlers\ORM\UserPaypalHandler $userPaypalHandler
     */
    public function __construct(UserPaypalHandler $userPaypalHandler)
    {
        $this->userPaypalHandler = $userPaypalHandler;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Response(
     *     response=200,
     *     description="User onboard url",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="url", type="string")
     *      )
     * )
     */
    public function getUserPaypalAction()
    {
        $response = $this->userPaypalHandler->getOnboardUrl();

        $view = $this->view($response, Response::HTTP_OK);
        return $this->handleView($view);
    }
}