<?php

namespace App\Controller\User;

use App\Handlers\ORM\ProfileHandler;
use App\Utils\User as UserUtils;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOS;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProfileController.
 *
 * @SWG\Tag(name="Profile")
 * @FOS\Route("/api/profile")
 */
class ProfileController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\ProfileHandler
     */
    private $profileHandler;

    /**
     * ProfileController constructor.
     * @param \App\Handlers\ORM\ProfileHandler $profileHandler
     */
    public function __construct(ProfileHandler $profileHandler)
    {
        $this->profileHandler = $profileHandler;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Get(path="/api/profile")
     *
     * @SWG\Response(
     *     response=200,
     *     description="User resource",
     *     @Model(type=App\Entity\User::class)
     * )
     */
    public function getProfileAction()
    {
        $data = $this->profileHandler->getProfile();

        $view = $this->view($data, Response::HTTP_OK)->setContext(
            (new Context())->setGroups(['profile'])
        );
        return $this->handleView($view);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     *
     * @FOS\Get("/send-validate-email")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Get(path="/api/profile/send-validate-email")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Send email validate",
     * )
     */
    public function getSendValidateEmailAction()
    {
        $this->profileHandler->sendValidateEmail();

        $view = $this->view([], Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \App\Exception\InvalidFormException
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @FOS\Put("", name="update_profile")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Put(path="/api/profile")
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Profile information",
     *     @Model(type=App\Form\ProfileType::class)
     * )
     *
     * @SWG\Response(
     *     response=200,
     *      description="Updated user resource",
     *     @Model(type=App\Entity\User::class)
     * )
     */
    public function putProfileAction(Request $request)
    {
        $data = $this->profileHandler->updateProfile($request->request->all());

        $view = $this->view($data, Response::HTTP_OK);

        return $this->handleView($view);
    }
}
