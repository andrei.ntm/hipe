<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Handlers\ORM\UserHandler;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as FOS;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * Class UserController
 * @package App\Controller\User
 *
 * @SWG\Tag(name="User")
 * @FOS\Route("/api/users")
 */
class UserController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\UserHandler
     */
    private $userHandler;

    /**
     * UserController constructor.
     * @param \App\Handlers\ORM\UserHandler $userHandler
     */
    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("", name="get_users_collection")
     *
     * @FOS\QueryParam(name="limit", requirements="\d+", default="1000", description="Maximum results")
     * @FOS\QueryParam(name="offset", requirements="\d+", default="0", description="Offset of the overview")
     * @FOS\QueryParam(name="search", description="Search user by username or fullname")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the users list",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="results", type="array",
     *              @SWG\Items(ref=@Model(type=App\Entity\User::class))
     *          ),
     *          @SWG\Property(property="count", type="integer")
     *      )
     * )
     */
    public function cgetUserAction(ParamFetcher $paramFetcher)
    {
        $data = $this->userHandler->getUserList($paramFetcher);

        $view = $this->view($data, Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * @param \App\Entity\User $user
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("/{id}", name="get_user", requirements={"id"="\d+"})
     *
     * @ParamConverter("user", options={"id" = "id"}, class="App\Entity\User")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Get(path="/api/users/{id}")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="User id resource"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Follow user status",
     *     @Model(type=App\Entity\User::class)
     * )
     */
    public function getUserAction(User $user)
    {
        $view = $this->view($user, Response::HTTP_OK)->setContext(
            (new Context())->setGroups(['visit-profile'])
        );
        return $this->handleView($view);
    }
}
