<?php

namespace App\Controller;

use App\Handlers\ORM\CountryHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOS;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CountryController.
 * @SWG\Tag(name="Country")
 * @FOS\Route("/api/countries")
 */
class CountryController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\CountryHandler
     */
    protected $countryHandler;

    /**
     * CountryController constructor.
     * @param \App\Handlers\ORM\CountryHandler $countryHandler
     */
    public function __construct(CountryHandler $countryHandler)
    {
        $this->countryHandler = $countryHandler;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("", name="get_countries_all")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Get(path="/api/countries")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Get countries collection",
     *     @Model(type=App\Entity\Country::class)
     * )
     */
    public function cgetCountriesAction()
    {
        $countries = $this->countryHandler->allCountries();

        $view = $this->view($countries, Response::HTTP_OK);

        return $this->handleView($view);
    }
}
