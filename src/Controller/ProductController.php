<?php

namespace App\Controller;

use App\Handlers\ORM\FeedHandler;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as FOS;

/**
 * Class ProductController
 * @package App\Controller
 * @SWG\Tag(name="Product")
 * @FOS\Route("/api/products")
 */
class ProductController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\FeedHandler
     */
    private $feedHandler;

    /**
     * ProductController constructor.
     * @param \App\Handlers\ORM\FeedHandler $feedHandler
     */
    public function __construct(FeedHandler $feedHandler)
    {
        $this->feedHandler = $feedHandler;
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("", name="get_product_collection")
     *
     * @FOS\QueryParam(name="limit", requirements="\d+", default="1000", description="Maximum results")
     * @FOS\QueryParam(name="offset", requirements="\d+", default="0", description="Offset of the overview")
     * @FOS\QueryParam(name="sort", description="Sort parameter with the format ?sort=id,-status")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns product collection.",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="results", type="array",
     *              @SWG\Items(ref=@Model(type=App\Entity\FeedItem::class))
     *          ),
     *          @SWG\Property(property="count", type="integer")
     *     )
     * )
     */
    public function cgetProductsAction(ParamFetcher $paramFetcher)
    {
        $data = $this->feedHandler->getProductsList($paramFetcher);

        $view = $this->view($data, Response::HTTP_OK)->setContext(
            (new Context())->setGroups(['minimal-feed-item'])
        );
        return $this->handleView($view);
    }
}
