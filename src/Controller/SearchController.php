<?php

namespace App\Controller;

use App\Handlers\ORM\SearchHandler;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as FOS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class SearchController
 * @package App\Controller
 * @SWG\Tag(name="Search")
 * @FOS\Route("/api/search")
 */
class SearchController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\SearchHandler
     */
    private $searchHandler;

    /**
     * SearchController constructor.
     * @param \App\Handlers\ORM\SearchHandler $searchHandler
     */
    public function __construct(SearchHandler $searchHandler)
    {
        $this->searchHandler = $searchHandler;
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("", name="get_search_collection")
     *
     * @FOS\QueryParam(name="limit", requirements="\d+", default="1000", description="Maximum results")
     * @FOS\QueryParam(name="offset", requirements="\d+", default="0", description="Offset of the overview")
     * @FOS\QueryParam(name="sort", description="Sort parameter with the format ?sort=id,-status")
     * @FOS\QueryParam(name="type", default="users", requirements="(users|products)", description="Type of search users or products")
     * @FOS\QueryParam(name="search", strict=true, requirements=".{3,}", nullable=false, description="Search for something")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns products collection.",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="results", type="array",
     *              @SWG\Items(ref=@Model(type=App\Entity\FeedItem::class)),
     *              @SWG\Items(ref=@Model(type=App\Entity\User::class)),
     *          ),
     *          @SWG\Property(property="count", type="integer")
     *     )
     * )
     */
    public function cgetSearchAction(ParamFetcher $paramFetcher)
    {
        $data = $this->searchHandler->searchResults($paramFetcher);

        $view = $this->view($data, Response::HTTP_OK)->setContext(
            (new Context())->setGroups(["feed-short-info", "short-info"])
        );
        return $this->handleView($view);
    }
}
