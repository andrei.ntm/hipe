<?php

namespace App\Controller;

use App\Entity\FeedItem;
use App\Handlers\ORM\FeedHandler;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOS;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class FeedController.
 *
 * @SWG\Tag(name="Feed")
 * @FOS\Route("/api/feed")
 */
class FeedController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\FeedHandler
     */
    protected $feedHandler;

    /**
     * FeedController constructor.
     * @param \App\Handlers\ORM\FeedHandler $feedHandler
     */
    public function __construct(FeedHandler $feedHandler)
    {
        $this->feedHandler = $feedHandler;
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("", name="get_feed")
     *
     * @FOS\QueryParam(name="limit", requirements="\d+", default="1000", description="Maximum results")
     * @FOS\QueryParam(name="offset", requirements="\d+", default="0", description="Offset of the overview")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Get(path="/api/feed")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the feed list",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="results", type="array",
     *              @SWG\Items(ref=@Model(type=App\Entity\FeedItem::class))
     *          ),
     *          @SWG\Property(property="may_like_results", type="array",
     *              @SWG\Items(ref=@Model(type=App\Entity\FeedItem::class))
     *          ),
     *          @SWG\Property(property="count", type="integer")
     *      )
     * )
     */
    public function cgetFeedAction(ParamFetcher $paramFetcher)
    {
        $data = $this->feedHandler->getFeedList($paramFetcher);

        $view = $this->view($data, Response::HTTP_OK)->setContext(
            (new Context())->addGroups(['feed-item', 'guest-profile'])
        );

        return $this->handleView($view);
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("/{id}", requirements={"id"="\d+"}, name="get_feed_item")
     *
     * @ParamConverter("feedItem", options={"id" = "id"}, class="App\Entity\FeedItem")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Get(path="/api/feed/{id}")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="FeedItem id resource"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Feed item resource",
     *     @Model(type=App\Entity\FeedItem::class)
     * )
     */
    public function getFeedItemAction(FeedItem $feedItem)
    {
        $view = $this->view($feedItem, Response::HTTP_OK)->setContext(
            (new Context())->addGroups(['feed-item', 'guest-profile'])
        );

        return $this->handleView($view);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \App\Exception\InvalidFormException
     * @FOS\Post("", name="post_feed_item")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Post(path="/api/feed")
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Add feed item",
     *     @Model(type=App\Form\FeedItemType::class)
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Add feed item",
     *     @Model(type=App\Entity\FeedItem::class)
     * )
     */
    public function postFeedAction(Request $request)
    {
        $data = $this->feedHandler->addFeedItem($request->request->all());

        $view = $this->view($data, Response::HTTP_CREATED)->setContext(
            (new Context())->addGroups(['feed-item', 'guest-profile'])
        );

        return $this->handleView($view);
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Delete("/{id}", name="delete_feed_item")
     *
     * @ParamConverter("feedItem", options={"id" = "id"}, class="App\Entity\FeedItem")
     *
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     *
     * @Security("user == feedItem.getCreatedBy()")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Feed item id resource"
     * )
     *
     * @SWG\Response(
     *     response=204,
     *      description="Delete feed item resource"
     * )
     */
    public function deleteFeedAction(FeedItem $feedItem)
    {
        $this->feedHandler->remove($feedItem);

        $view = $this->view([], Response::HTTP_NO_CONTENT);
        return $this->handleView($view);
    }
}
