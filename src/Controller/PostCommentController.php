<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\FeedItem;
use App\Handlers\ORM\CommentHandler;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as FOS;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class PostCommentController
 * @package App\Controller
 * @SWG\Tag(name="Post comments")
 * @FOS\Route("/api/feed/{id}/comments")
 */
class PostCommentController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\CommentHandler
     */
    private $commentHandler;

    /**
     * PostCommentController constructor.
     * @param \App\Handlers\ORM\CommentHandler $commentHandler
     */
    public function __construct(CommentHandler $commentHandler)
    {
        $this->commentHandler = $commentHandler;
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("", name="get_comments_collection")
     *
     * @ParamConverter("feedItem", options={"id" = "id"}, class="App\Entity\FeedItem")
     *
     * @FOS\QueryParam(name="limit", requirements="\d+", default="1000", description="Maximum results")
     * @FOS\QueryParam(name="offset", requirements="\d+", default="0", description="Offset of the overview")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Feed item id resource"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the comments list",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="results", type="array",
     *              @SWG\Items(ref=@Model(type=App\Entity\Comment::class))
     *          ),
     *          @SWG\Property(property="count", type="integer")
     *      )
     * )
     */
    public function cgetCommentsAction(FeedItem $feedItem, ParamFetcher $paramFetcher)
    {
        $data = $this->commentHandler->getCommentsByPost($feedItem, $paramFetcher);

        $view = $this->view($data, Response::HTTP_OK)->setContext(
            (new Context())->setGroups(["short-comment", "short-info"])
        );
        return $this->handleView($view);
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\InvalidFormException
     *
     * @FOS\Post("", name="post_comment")
     *
     * @ParamConverter("feedItem", options={"id" = "id"}, class="App\Entity\FeedItem")
     *
     * @FOS\QueryParam(name="limit", requirements="\d+", default="1000", description="Maximum results")
     * @FOS\QueryParam(name="offset", requirements="\d+", default="0", description="Offset of the overview")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Feed id resource"
     * )
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Add comment item",
     *     @Model(type=App\Form\CommentType::class)
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Comment resource",
     *     @Model(type=App\Entity\Comment::class)
     * )
     */
    public function postCommentsAction(FeedItem $feedItem, Request $request)
    {
        $data = $this->commentHandler->createComment($feedItem, $request->request->all());

        $view = $this->view($data, Response::HTTP_CREATED)->setContext(
            (new Context())->setGroups(["short-comment", "short-info"])
        );
        return $this->handleView($view);
    }

    /**
     * @param \App\Entity\FeedItem $feedItem
     * @param \App\Entity\Comment $comment
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Delete("/{comment_id}", name="delete_comment")
     *
     * @ParamConverter("feedItem", options={"id" = "id"}, class="App\Entity\FeedItem")
     * @ParamConverter("comment", options={"id" = "comment_id"}, class="App\Entity\Comment")
     *
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     *
     * @Security("user == comment.getCreatedBy()")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Feed item id resource"
     * )
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Comment id resource"
     * )
     *
     * @SWG\Response(
     *     response=204,
     *      description="Delete comment resource"
     * )
     */
    public function deleteCommentAction(FeedItem $feedItem, Comment $comment)
    {
        $this->commentHandler->remove($comment);

        $view = $this->view([], Response::HTTP_NO_CONTENT);
        return $this->handleView($view);
    }
}
