<?php

namespace App\Controller;

use App\Entity\User;
use App\Handlers\ORM\BagHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as FOS;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class BagAvailabilityController
 * @package App\Controller
 * @SWG\Tag(name="Bag")
 * @FOS\Route("/api/bag/availability")
 */
class BagAvailabilityController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\BagHandler
     */
    private $bagHandler;

    /**
     * BagController constructor.
     * @param \App\Handlers\ORM\BagHandler $bagHandler
     */
    public function __construct(BagHandler $bagHandler)
    {
        $this->bagHandler = $bagHandler;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @FOS\Get("", name="get_availability_collection")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="multipart/form-data")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns availability.",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="results", type="array",
     *              @SWG\Items(type="object",
     *              	@SWG\Property(type="integer", property="id"),
     *                  @SWG\Property(type="type", property="available")
     *              )
     *          )
     *     )
     * )
     */
    public function cgetBagAvailabilityAction()
    {
        $data = $this->bagHandler->availability();

        $view = $this->view($data, Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * @param \App\Entity\User $user
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @FOS\Get("/{user}", name="get_availability_user")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="multipart/form-data")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @ParamConverter("user", options={"id" = "id"}, class="App\Entity\User")
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="User id resource"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns availability.",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="results", type="array",
     *              @SWG\Items(type="object",
     *              	@SWG\Property(type="integer", property="id"),
     *                  @SWG\Property(type="string", property="status")
     *              )
     *          )
     *     )
     * )
     */
    public function getBagAvailabilityAction(User $user)
    {
        $data = $this->bagHandler->availability($user);

        $view = $this->view($data, Response::HTTP_OK);
        return $this->handleView($view);
    }
}