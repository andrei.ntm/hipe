<?php

namespace App\Controller\Auth;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOS;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * Class LoginController
 * @package App\Controller\Auth
 *
 * @SWG\Tag(name="Authentication")
 * @FOS\Route("/api/login")
 */
class LoginController extends AbstractFOSRestController
{
    /**
     * @FOS\Post("")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Accept-Language", type="string", default="en_US")
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Login user by email",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="username", type="string"),
     *          @SWG\Property(property="password", type="string"),
     *     )
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="JWT token",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="token", type="string"),
     *          @SWG\Property(property="data", type="object",
     *              @SWG\Property(property="id", type="integer"),
     *              @SWG\Property(property="name", type="string"),
     *              @SWG\Property(property="roles", type="array",
     *                  @SWG\Items(type="string")
     *              )
     *          )
     *     )
     * )
     */
    public function index()
    {
    }
}