<?php

namespace App\Controller\Auth;

use App\Handlers\ORM\AppleRegisterHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOS;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AppleController.
 *
 * @SWG\Tag(name="Authentication")
 * @FOS\Route("/api/login-apple")
 */
class AppleController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\AppleRegisterHandler
     */
    private $appleRegisterHandler;

    /**
     * AppleController constructor.
     * @param \App\Handlers\ORM\AppleRegisterHandler $appleRegisterHandler
     */
    public function __construct(AppleRegisterHandler $appleRegisterHandler)
    {
        $this->appleRegisterHandler = $appleRegisterHandler;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Post("")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     *
     * @SWG\Post(path="/api/login-apple")
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Authenticate user by apple",
     *     @SWG\Schema(type="object",
     *          @SWG\Property(property="access_token", type="string")
     *     )
     * )
     *
     * @SWG\Response(
     *     response=201,
     *      description="Create user resource",
     *     @SWG\Schema(type="object",
     *          @SWG\Property(property="user", ref=@Model(type=App\Entity\User::class)),
     *          @SWG\Property(property="token", type="string")
     *     )
     * )
     */
    public function postAuthAction(Request $request)
    {
        $data = $this->appleRegisterHandler->auth($this->getUser());

        $view = $this->view($data, Response::HTTP_OK);

        return $this->handleView($view);
    }
}
