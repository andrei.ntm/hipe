<?php

namespace App\Controller\Auth;

use App\Handlers\ORM\RegisterHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOS;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RegisterController.
 *
 * @SWG\Tag(name="Authentication")
 * @FOS\Route("/api/register")
 */
class RegisterController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\RegisterHandler
     */
    protected $registerHandler;

    /**
     * RegisterController constructor.
     * @param \App\Handlers\ORM\RegisterHandler $registerHandler
     */
    public function __construct(RegisterHandler $registerHandler)
    {
        $this->registerHandler = $registerHandler;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \App\Exception\InvalidFormException
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @FOS\Post("")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     *
     * @SWG\Post(path="/api/register")
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Register user by email",
     *     @Model(type=App\Form\UserType::class)
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Create user resource",
     *     @Model(type=App\Entity\User::class)
     * )
     */
    public function postUserAction(Request $request)
    {
        /** @var \App\Entity\User $data */
        $data = $this->registerHandler->registerByEmail($request->request->all());

        $view = $this->view($data, Response::HTTP_CREATED);

        return $this->handleView($view);
    }
}
