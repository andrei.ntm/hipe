<?php

namespace App\Controller\Auth;

use App\Handlers\ORM\AccountValidationHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ResetPasswordController.
 */
class AccountValidationController extends AbstractController
{
    /**
     * @var \App\Handlers\ORM\AccountValidationHandler
     */
    private $accountValidationHandler;

    /**
     * AccountValidationController constructor.
     * @param \App\Handlers\ORM\AccountValidationHandler $accountValidationHandler
     */
    public function __construct(AccountValidationHandler $accountValidationHandler)
    {
        $this->accountValidationHandler = $accountValidationHandler;
    }

    /**
     * @param string $token
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route(
     *     "/account-validation/{token}",
     *     methods={"GET"},
     *     name="account-validation"
     * )
     */
    public function index(string $token)
    {
        $validated = $this->accountValidationHandler->checkUserByToken($token);

        return $this->render('validate-account/index.html.twig', compact('validated'));
    }
}
