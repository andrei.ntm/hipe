<?php

namespace App\Controller\Auth;

use App\Handlers\ORM\ResetPasswordHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOS;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ResetPasswordController.
 *
 * @SWG\Tag(name="Reset password")
 */
class ResetPasswordController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\ResetPasswordHandler
     */
    private $resetPasswordHandler;

    /**
     * ResetPasswordController constructor.
     * @param \App\Handlers\ORM\ResetPasswordHandler $resetPasswordHandler
     */
    public function __construct(ResetPasswordHandler $resetPasswordHandler)
    {
        $this->resetPasswordHandler = $resetPasswordHandler;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \App\Exception\InvalidFormException
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @FOS\Post("/api/reset-password")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     *
     * @SWG\Post(path="/api/reset-password")
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Reset password",
     *     @Model(type=App\Form\RequestResetPasswordType::class)
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Reset password",
     *     @SWG\Schema(type="object")
     * )
     */
    public function postRequestResetAction(Request $request)
    {
        $data = $this->resetPasswordHandler->requestResetPassword($request->request->all());

        $view = $this->view($data, Response::HTTP_OK);

        return $this->handleView($view);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param string $token
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route(
     *     "/reset-password/{token}",
     *     methods={"GET", "POST"},
     *     name="reset-password"
     * )
     */
    public function resetPasswordAction(Request $request, string $token)
    {
        $response = $this->resetPasswordHandler->getResetPasswordForm($request, $token);

        return $this->render('reset-password/index.html.twig', [
            'form' => $response instanceof FormInterface ? $response->createView() : null,
            'status' => !$response instanceof FormInterface ? $response['status'] : null,
        ]);
    }
}
