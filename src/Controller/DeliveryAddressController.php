<?php

namespace App\Controller;

use App\Handlers\ORM\DeliveryAddressHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as FOS;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class DeliveryAddressController
 * @package App\Controller
 * @SWG\Tag(name="Delivery Addresses")
 * @FOS\Route("/api/delivery-addresses")
 */
class DeliveryAddressController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\DeliveryAddressHandler
     */
    private $deliveryAddressHandler;

    /**
     * DeliveryAddressController constructor.
     * @param \App\Handlers\ORM\DeliveryAddressHandler $deliveryAddressHandler
     */
    public function __construct(DeliveryAddressHandler $deliveryAddressHandler)
    {
        $this->deliveryAddressHandler = $deliveryAddressHandler;
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("", name="get_delivery_addresses")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Get(path="/api/delivery-addresses")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the delivery address",
     *     @Model(type=App\Entity\DeliveryAddress::class)
     * )
     */
    public function getDeliveryAddressAction(ParamFetcher $paramFetcher): Response
    {
        /** @var \App\Entity\DeliveryAddress $address */
        $address = $this->deliveryAddressHandler->getDeliveryAddress();

        $view = $this->view($address, Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\InvalidFormException
     *
     * @FOS\Post("", name="post_delivery_addresses")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Create or update delivery address",
     *     @Model(type=App\Form\DeliveryAddressType::class)
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns delivery address",
     *     @Model(type=App\Entity\DeliveryAddress::class)
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns delivery address",
     *     @Model(type=App\Entity\DeliveryAddress::class)
     * )
     */
    public function postDeliveryAddressAction(Request $request): Response
    {
        $response = $this->deliveryAddressHandler->createOrUpdateDeliveryAddress($request->request->all());

        $view = $this->view($response['address'], $response['code']);
        return $this->handleView($view);
    }
}