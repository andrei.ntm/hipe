<?php

namespace App\Controller;

use App\Handlers\ORM\PaypalWebhookHandler;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as FOS;
use Swagger\Annotations as SWG;

/**
 * Class PaypalWebhookController
 * @package App\Controller
 *
 * @SWG\Tag(name="Paypal webhook")
 * @FOS\Route("/api/paypal-webhook")
 */
class PaypalWebhookController extends AbstractFOSRestController
{
    /**
     * @var \App\Controller\PaypalWebhookController
     */
    protected $paypalWebhookHandler;

    /**
     * PaypalWebhookController constructor.
     * @param \App\Handlers\ORM\PaypalWebhookHandler $paypalWebhookController
     */
    public function __construct(PaypalWebhookHandler $paypalWebhookController)
    {
        $this->paypalWebhookHandler = $paypalWebhookController;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Post("", name="post_paypal_webhook")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     *
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="Intercept paypal response",
     *     @SWG\Schema(type="object")
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Save paypal response"
     * )
     */
    public function postPaypalWebhookAction(Request $request)
    {
        $this->paypalWebhookHandler->handlePaypalResponse($request);

        $view = $this->view([], Response::HTTP_CREATED);
        return $this->handleView($view);
    }
}