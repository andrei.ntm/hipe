<?php

namespace App\Controller;

use App\Handlers\ORM\InfluencerHandler;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as FOS;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * Class InfluencerController
 * @package App\Controller
 * @SWG\Tag(name="Influencer")
 * @FOS\Route("/api/influencers")
 */
class InfluencerController extends AbstractFOSRestController
{
    /**
     * @var \App\Handlers\ORM\InfluencerHandler
     */
    private $influencerHandler;

    /**
     * InfluencerController constructor.
     * @param \App\Handlers\ORM\InfluencerHandler $influencerHandler
     */
    public function __construct(InfluencerHandler $influencerHandler)
    {
        $this->influencerHandler = $influencerHandler;
    }

    /**
     * @param \FOS\RestBundle\Request\ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @FOS\Get("/random", name="get_random/influencers_all")
     *
     * @SWG\Parameter(in="header", name="Content-Type", required=true, type="string", default="application/json")
     * @SWG\Parameter(in="header", name="Authorization", required=true, type="string", description="Bearer [token]")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Get random users collection",
     *     @Model(type=App\Entity\User::class)
     * )
     */
    public function cgetInfluencerAction(ParamFetcher $paramFetcher)
    {
        $data = $this->influencerHandler->getRandomInfluencers($paramFetcher);

        $view = $this->view($data, Response::HTTP_OK)->setContext(
            (new Context())->setGroups(['visit-profile'])
        );
        return $this->handleView($view);
    }
}
