<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UserType extends Constraint
{
    public $message = "User type can not be changed";
}
