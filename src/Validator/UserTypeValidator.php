<?php

namespace App\Validator;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class UserTypeValidator
 * @package App\Validator
 */
class UserTypeValidator extends ConstraintValidator
{
    /**
     * @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * UserTypeValidator constructor.
     * @param \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param mixed $value
     * @param \Symfony\Component\Validator\Constraint|\App\Validator\UserType $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if ($this->tokenStorage->getToken()->getUser()->isAccountCompleted()) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
