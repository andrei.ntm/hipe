<?php

namespace App\Validator;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ImagesExistsValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint \App\Validator\ImagesExists */
        if (!$value instanceof ArrayCollection || 0 == $value->count()) {
            return;
        }

        foreach ($value as $image) {
            if (!file_exists($image)) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }

        return;
    }
}
