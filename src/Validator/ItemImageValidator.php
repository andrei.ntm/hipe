<?php

namespace App\Validator;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ItemImageValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint \App\Validator\ItemImage */

        if ($value instanceof ArrayCollection && $value->count() > 0) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->addViolation();
    }
}
