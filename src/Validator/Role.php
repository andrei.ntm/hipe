<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Role extends Constraint
{
    public $message = 'You are not allowed to do this stuff !';

    public $role;
}
