<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ImageExists extends Constraint
{
    public $message = 'Corrupt image path.';
}
