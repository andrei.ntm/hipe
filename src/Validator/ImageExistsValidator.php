<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class ImageExistsValidator
 * @package App\Validator
 */
class ImageExistsValidator extends ConstraintValidator
{
    /**
     * @param mixed $value
     * @param \Symfony\Component\Validator\Constraint|\App\Validator\ImageExists $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$value) {
            return;
        }

        if (!file_exists($value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }

        return;
    }
}
