<?php

namespace App\EventListener;

use App\Exception\InvalidFormException;
use App\Exception\NotCompletedAccountException;
use App\Exception\StandardException;
use App\Factory\NormalizerFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

/**
 * Class ExceptionListener
 * @package App\EventListener
 */
class ExceptionListener
{
    /**
     * @var \App\Factory\NormalizerFactory
     */
    private $normalizeFactory;

    /**
     * ExceptionListener constructor.
     * @param \App\Factory\NormalizerFactory $normalizerFactory
     */
    public function __construct(NormalizerFactory $normalizerFactory)
    {
        $this->normalizeFactory = $normalizerFactory;
    }

    /**
     * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        switch (true) {
            case $exception instanceof InvalidFormException:
                $exceptionResponse = $this->normalizeResponse($exception);
                $event->setResponse(
                    new JsonResponse($exceptionResponse, Response::HTTP_BAD_REQUEST)
                );
                break;
            case $exception instanceof StandardException:
            case $exception instanceof NotCompletedAccountException:
                $exceptionResponse = $this->normalizeResponse($exception);
                $event->allowCustomResponseCode();
                $event->setResponse(
                    new JsonResponse($exceptionResponse, $exception->getCode())
                );
                break;
        }
    }

    /**
     * @param \Exception $exception
     * @return array|\ArrayObject|bool|float|int|string|null
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalizeResponse(\Exception $exception)
    {
        $normalizer = $this->normalizeFactory->getNormalizer($exception);

        try {
            $response = $normalizer ? $normalizer->normalize($exception) : [];
        } catch (\Exception $e) {
            $response = [];
        }

        return $response;
    }
}
