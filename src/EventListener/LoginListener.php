<?php

namespace App\EventListener;

use App\Exception\NotCompletedAccountException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LoginListener
{
    /**
     * @var \Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage
     */
    private $tokenStorage;

    /**
     * LoginListener constructor.
     * @param \Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage $tokenStorage
     */
    public function __construct(UsageTrackingTokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param \Symfony\Component\Security\Http\Event\InteractiveLoginEvent $interactiveLoginEvent
     * @throws \App\Exception\NotCompletedAccountException
     */
    public function onAuthenticationSuccess(InteractiveLoginEvent $interactiveLoginEvent)
    {
        /** @var \App\Entity\User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        if (!$user->isAccountCompleted() && !in_array($interactiveLoginEvent->getRequest()->get('_route'), ['update_profile'])) {
            throw new NotCompletedAccountException($user, $this->tokenStorage->getToken()->getCredentials());
        }
    }
}
