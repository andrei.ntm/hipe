<?php

namespace App\Serializer;

use App\Exception\InvalidFormException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class FormExceptionNormalizer implements NormalizerInterface
{

    /**
     * @inheritDoc
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = [];
        /** @var \Symfony\Component\Form\Form $form */
        $form = $object->getForm();

        /** @var \Symfony\Component\Form\FormError $error */
        foreach ($form->getErrors() as $error) {
            $data[$error->getOrigin()->getName()][] = $error->getMessage();
        }

        /** @var \Symfony\Component\Form\Form $child */
        foreach ($form as $child) {
            /** @var \Symfony\Component\Form\FormErrorIterator $embedForm */
            foreach ($child as $embedForm) {
                foreach ($embedForm->getErrors() as $error) {
                    $data[$child->getName()][$embedForm->getName()][] = $error->getMessage();
                }

                foreach ($embedForm as $subEmbedForm) {
                    foreach ($subEmbedForm->getErrors() as $error) {
                        $data[$child->getName()][$embedForm->getName()][$subEmbedForm->getName()][] = $error->getMessage();
                    }
                }
            }

            foreach ($child->getErrors() as $error) {
                $data[$child->getName()][] = $error->getMessage();
            }
        }

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof InvalidFormException;
    }
}
