<?php

namespace App\Serializer;

use App\Exception\StandardException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class StandardExceptionNormalizer implements NormalizerInterface
{
    /**
     * @param StandardException $object
     * @param null $format
     * @param array $context
     * @return array|\ArrayObject|bool|float|int|string|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'error' => $object->getMessage()
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof StandardException;
    }
}
