<?php

namespace App\Serializer;

use App\Exception\NotCompletedAccountException;
use App\Utils\ErrorType;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class NotCompletedAccountNormalizer implements NormalizerInterface
{
    /**
     * @param NotCompletedAccountException $object
     * @param null $format
     * @param array $context
     * @return array|\ArrayObject|bool|float|int|string|void|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        /** @var \App\Entity\User $user */
        $user = $object->getUser();

        $data = [];

        if (!$user->getEmail()) {
            $data[] = ErrorType::EMPTY_EMAIL;
        }

        if (!$user->getUsername()) {
            $data[] = ErrorType::EMPTY_USERNAME;
        }

        if (count($user->getRoles()) == 0) {
            $data[] = ErrorType::EMPTY_ROLE;
        }

        return [
            'type' => ErrorType::NOT_COMPLETED_ACCOUNT,
            'data' => $data,
            'token' => $object->getToken(),
        ];
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed $data Data to normalize
     * @param string $format The format being (de-)serialized from or into
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof NotCompletedAccountException;
    }
}
