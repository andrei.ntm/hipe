<?php

namespace App\Exception;

use Symfony\Component\Form\FormInterface;
use Throwable;

/**
 * Class InvalidFormException
 * @package App\Exception
 */
class InvalidFormException extends \Exception
{

    /**
     * @var \Symfony\Component\Form\FormInterface
     */
    private $form;

    /**
     * InvalidFormException constructor.
     * @param \Symfony\Component\Form\FormInterface $form
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(FormInterface $form, $message = "Validation Failed", $code = 0, Throwable $previous = null)
    {
        $this->form = $form;

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return \Symfony\Component\Form\FormInterface
     */
    public function getForm(): FormInterface
    {
        return $this->form;
    }
}
