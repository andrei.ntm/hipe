<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Throwable;

class NotCompletedAccountException extends \Exception
{
    /**
     * @var \Symfony\Component\Security\Core\User\UserInterface
     */
    private $user;

    /**
     * @var string
     */
    private $token;

    public function __construct(UserInterface $user, $token, $message = "Not completed account", $code = Response::HTTP_PARTIAL_CONTENT, Throwable $previous = null)
    {
        $this->user = $user;
        $this->token = $token;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return \Symfony\Component\Security\Core\User\UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}
