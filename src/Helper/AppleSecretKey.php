<?php

namespace App\Helper;

class AppleSecretKey
{
    /**
     * @return string
     */
    public static function generate()
    {
        $header = [
            'alg' => 'ES256',
            'kid' => getenv('SECRET_KEY_ID')
        ];
        $body = [
            'iss' => getenv('TEAM_ID'),
            'iat' => time(),
            'exp' => time() + 3600,
            'aud' => 'https://appleid.apple.com',
            'sub' => getenv('APP_CLIENT_ID')
        ];

        $rootPath = dirname(__FILE__, 3);

        $key = openssl_pkey_get_private('file://' . $rootPath . '/config/apple/key.pem');

        $payload = self::encode(json_encode($header)) . '.' . self::encode(json_encode($body));

        openssl_sign($payload, $signature, $key, OPENSSL_ALGO_SHA256);

        $raw_signature = self::fromDER($signature, 64);

        return sprintf("%s.%s", $payload, self::encode($raw_signature));
    }

    /**
     * @param $data
     * @return string
     */
    public static function encode($data)
    {
        $encoded = strtr(base64_encode($data), '+/', '-_');

        return rtrim($encoded, '=');
    }

    /**
     * @param string $der
     * @param int $partLength
     * @return false|string
     */
    public static function fromDER(string $der, int $partLength)
    {
        $hex = unpack('H*', $der)[1];
        if ('30' !== mb_substr($hex, 0, 2, '8bit')) {
            throw new \RuntimeException();
        }

        if ('81' === mb_substr($hex, 2, 2, '8bit')) {
            $hex = mb_substr($hex, 6, null, '8bit');
        } else {
            $hex = mb_substr($hex, 4, null, '8bit');
        }

        if ('02' !== mb_substr($hex, 0, 2, '8bit')) {
            throw new \RuntimeException();
        }

        $Rl = hexdec(mb_substr($hex, 2, 2, '8bit'));
        $R = self::retrievePositiveInteger(mb_substr($hex, 4, $Rl * 2, '8bit'));
        $R = str_pad($R, $partLength, '0', STR_PAD_LEFT);
        $hex = mb_substr($hex, 4 + $Rl * 2, null, '8bit');

        if ('02' !== mb_substr($hex, 0, 2, '8bit')) {
            throw new \RuntimeException();
        }

        $Sl = hexdec(mb_substr($hex, 2, 2, '8bit'));
        $S = self::retrievePositiveInteger(mb_substr($hex, 4, $Sl * 2, '8bit'));
        $S = str_pad($S, $partLength, '0', STR_PAD_LEFT);

        return pack('H*', $R.$S);
    }

    /**
     * @param string $data
     * @return string
     */
    private static function retrievePositiveInteger(string $data)
    {
        while ('00' === mb_substr($data, 0, 2, '8bit') && mb_substr($data, 2, 2, '8bit') > '7f') {
            $data = mb_substr($data, 2, null, '8bit');
        }

        return $data;
    }
}
