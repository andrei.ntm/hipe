<?php

namespace App\Helper\QueryBuilder;

use Doctrine\ORM\QueryBuilder;

class PaginateHelper
{
    const DEFAULT_LIMIT = 100;
    const MAX_LIMIT = 500;


    /**
     * @param \Doctrine\ORM\QueryBuilder $qb
     * @param $limit
     * @param $offset
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public static function apply(QueryBuilder $qb, $limit, $offset)
    {
        if ($limit > self::MAX_LIMIT) {
            $limit = self::MAX_LIMIT;
        }

        $qb->setFirstResult($offset);
        $qb->setMaxResults($limit);

        return $qb;
    }
}
