<?php

namespace App\Helper\QueryBuilder;

use App\Helper\Camelizer;
use Doctrine\ORM\QueryBuilder;

class SortHelper
{
    /**
     * @param \Doctrine\ORM\QueryBuilder $qb
     * @param array $orderBy
     * @return \Doctrine\ORM\QueryBuilder
     */
    public static function applyORM(QueryBuilder $qb, array $orderBy)
    {
        $rootAlias = current($qb->getRootAliases());

        foreach ($orderBy as $field => $sortOrder) {
            $qb->orderBy(sprintf("%s.%s", $rootAlias, Camelizer::camelize($field)), $sortOrder);
        }

        return $qb;
    }

    /**
     * @param $sorParam
     *
     * @return array
     */
    public static function buildSortCriteriaFromQueryParam($sorParam)
    {
        $sortCriteria = [];

        if (empty($sorParam)) {
            return $sortCriteria;
        }

        $fieldsToSort = explode(',', $sorParam);

        foreach ($fieldsToSort as $field) {
            if ('-' == $field[0]) {
                $sortedField = Camelizer::camelize(substr($field, 1));
                if (true === ctype_alnum($sortedField)) {
                    $sortCriteria[$sortedField] = 'DESC';
                }
            } else {
                $sortedField = Camelizer::camelize($field);
                if (true === ctype_alnum($sortedField)) {
                    $sortCriteria[$sortedField] = 'ASC';
                }
            }
        }

        return $sortCriteria;
    }
}
