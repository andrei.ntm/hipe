<?php

namespace App\Helper;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TruncateTableHandler
{
    public static function handle(EntityManagerInterface $em, OutputInterface $output, $entity)
    {
        $output->writeln([
            '<info>Truncate table</info>',
            '<info>=======================================</info>',
        ]);

        $cmd = $em->getClassMetadata($entity);
        $connection = $em->getConnection();
        $em->beginTransaction();

        try {
            $connection->query(sprintf('DELETE FROM %s', $cmd->getTableName()));
            $connection->query(sprintf('ALTER SEQUENCE %s_id_seq RESTART WITH 1', strtolower($cmd->getTableName())));
            $connection->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            $output->writeln(sprintf('<info>Transaction failed: %s</info>', $e->getMessage()));

            return 0;
        }

        $output->writeln([
            '<info>Starting populate into database</info>',
            '<info>=======================================</info>',
        ]);
    }
}
