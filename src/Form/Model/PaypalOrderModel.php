<?php

namespace App\Form\Model;

use App\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PaypalOrderModel
 * @package App\Form\Model
 */
class PaypalOrderModel
{
    /**
     * @Assert\NotBlank
     */
    private $seller;

    /**
     * @return mixed
     */
    public function getSeller(): ?User
    {
        return $this->seller;
    }

    /**
     * @param $seller
     * @return $this
     */
    public function setSeller($seller): self
    {
        $this->seller = $seller;

        return $this;
    }
}