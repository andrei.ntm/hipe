<?php

namespace App\Form\Model\UploadMedia;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

class Image
{
    /**
     * @Assert\File(
     *     maxSize="5120k",
     *     mimeTypes = {"image/png", "image/jpg", "image/jpeg"}
     * )
     */
    protected $image;

    /**
     * @return mixed
     */
    public function getImage(): ?UploadedFile
    {
        return $this->image;
    }

    /**=
     * @param $image
     * @return $this
     */
    public function setImage($image): self
    {
        $this->image = $image;

        return $this;
    }
}
