<?php

namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ResetPasswordForm
 * @package App\Form\Model
 */
class ResetPasswordForm
{
    /**
     * @var string
     * @Assert\Length(min=6, max=100)
     * @Assert\NotBlank
     */
    private $password;

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return $this
     */
    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
