<?php

namespace App\Form\Model\FeedItem;

use App\Entity\Category;
use App\Entity\Country;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Swagger\Annotations as SWG;
use App\Validator as AppAssert;

class FeedItem
{
    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @Assert\Length(min=1, max=50)
     */
    protected $title;

    /**
     * @Assert\Type("string")
     * @Assert\Length(max=2000)
     */
    protected $description;

    /**
     * @Assert\Type("bool")
     */
    protected $worldwideShipping = false;

    /**
     * @Assert\NotBlank
     * @Assert\Valid
     */
    protected $location;

    /**
     * @Assert\NotBlank
     * @Assert\Valid
     */
    protected $category;

    /**
     * @Assert\Valid
     */
    protected $quantities;

    /**
     * @Assert\NotBlank
     * @Assert\Valid
     */
    protected $price;

    /**
     * @AppAssert\ItemImage
     * @AppAssert\ImagesExists
     */
    protected $images;

    /**
     * FeedItem constructor.
     */
    public function __construct()
    {
        $this->quantities = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return $this
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLocation(): ?Country
    {
        return $this->location;
    }

    /**
     * @param \App\Entity\Country $location
     * @return $this
     */
    public function setLocation(Country $location): self
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return \App\Entity\Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param \App\Entity\Category $category
     * @return $this
     */
    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getQuantities(): Collection
    {
        return $this->quantities;
    }

    /**
     * @param \App\Form\Model\FeedItem\Quantity $quantity
     *
     * @return $this
     */
    public function addQuantity(Quantity $quantity): self
    {
        if (!$this->quantities->contains($quantity)) {
            $this->quantities->add($quantity);
        }

        return $this;
    }

    /**
     * @param \App\Form\Model\FeedItem\Quantity $quantity
     *
     * @return $this
     */
    public function removeQuantity(Quantity $quantity): self
    {
        if ($this->quantities->contains($quantity)) {
            $this->quantities->removeElement($quantity);
        }

        return $this;
    }

    /**
     * @return \App\Form\Model\FeedItem\Price|null
     */
    public function getPrice(): ?Price
    {
        return $this->price;
    }

    /**
     * @param \App\Form\Model\FeedItem\Price $price
     *
     * @return $this
     */
    public function setPrice(Price $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return bool
     */
    public function isWorldwideShipping(): ?bool
    {
        return $this->worldwideShipping;
    }

    /**
     * @param bool|null $worldwideShipping
     * @return $this
     */
    public function setWorldwideShipping(?bool $worldwideShipping): self
    {
        $this->worldwideShipping = $worldwideShipping;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * @param string $image
     * @return $this
     */
    public function addImage(string $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images->add($image);
        }

        return $this;
    }

    /**
     * @param string $image
     * @return $this
     */
    public function removeImage(string $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
        }

        return $this;
    }
}
