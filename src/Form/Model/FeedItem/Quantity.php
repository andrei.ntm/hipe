<?php

namespace App\Form\Model\FeedItem;

use Symfony\Component\Validator\Constraints as Assert;

class Quantity
{
    /**
     * @Assert\NotBlank
     * @Assert\Type("integer")
     * @Assert\GreaterThanOrEqual(0)
     */
    protected $amount;

    /**
     * @Assert\Type("string")
     */
    protected $size;

    /**
     * @Assert\Type("string")
     * @Assert\Regex("/^\#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/")
     */
    protected $color;

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(?int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(?string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }
}
