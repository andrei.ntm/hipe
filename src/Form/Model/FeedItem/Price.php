<?php

namespace App\Form\Model\FeedItem;

use Symfony\Component\Validator\Constraints as Assert;

class Price
{
    /**
     * @Assert\NotBlank
     * @Assert\Type("float")
     * @Assert\GreaterThanOrEqual(0)
     */
    protected $amount;

    /**
     * @Assert\NotBlank
     * @Assert\Type("float")
     * @Assert\GreaterThanOrEqual(0)
     */
    protected $domesticShippingAmount;

    /**
     * @Assert\Type("float")
     * @Assert\GreaterThanOrEqual(0)
     */
    protected $worldwideShippingAmount;

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getDomesticShippingAmount(): ?float
    {
        return $this->domesticShippingAmount;
    }

    /**
     * @param float $domesticShippingAmount
     * @return $this
     */
    public function setDomesticShippingAmount(float $domesticShippingAmount): self
    {
        $this->domesticShippingAmount = $domesticShippingAmount;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getWorldwideShippingAmount(): ?float
    {
        return $this->worldwideShippingAmount;
    }

    /**
     * @param float|null $worldwideShippingAmount
     * @return $this
     */
    public function setWorldwideShippingAmount(?float $worldwideShippingAmount): self
    {
        $this->worldwideShippingAmount = $worldwideShippingAmount;

        return $this;
    }
}
