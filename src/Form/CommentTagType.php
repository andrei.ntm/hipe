<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CommentTagType
 * @package App\Form
 */
class CommentTagType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user_id', EntityType::class, [
                'class' => User::class
            ])
            ->add('name', TextType::class);

        $builder->get('user_id')->addModelTransformer(new CallbackTransformer(
            function ($value) {
            },
            function (User $user) {
                return $user->getId();
            }
        ));
    }
}
