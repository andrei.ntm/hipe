<?php

namespace App\Form;

use App\Entity\BagItem;
use App\Entity\Color;
use App\Entity\FeedItem;
use App\Entity\Size;
use App\Repository\FeedItemRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BagItemType
 * @package App\Form
 */
class BagItemType extends AbstractType
{
    /**
     * @var \App\Repository\FeedItemRepository
     */
    private $feedItemRepository;

    /**
     * BagItemType constructor.
     * @param \App\Repository\FeedItemRepository $feedItemRepository
     */
    public function __construct(FeedItemRepository $feedItemRepository)
    {
        $this->feedItemRepository = $feedItemRepository;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantity', IntegerType::class)
            ->add('feed_item', EntityType::class, [
                'property_path' => 'feedItem',
                'class' => FeedItem::class
            ])
            ->add('color', EntityType::class, [
                'class' => Color::class
            ])
            ->add('size', EntityType::class, [
                'class' => Size::class
            ])
        ;

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var BagItem $bagItem */
            $bagItem = $event->getData();
            $form = $event->getForm();

            /** @var FeedItem $feedItem */
            $feedItem = $this->feedItemRepository->retrieveFeedItem($bagItem);

            if (!$feedItem instanceof FeedItem) {
                $form->get('feed_item')->addError(new FormError('Selected item does not exists.'));
            }
        });
    }

    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BagItem::class,
        ]);
    }
}
