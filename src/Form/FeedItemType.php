<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Country;
use App\Form\DataTransformer\FeedItemImageTransformer;
use App\Form\Model\FeedItem\FeedItem;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FeedItemType.
 */
class FeedItemType extends AbstractType
{
    /**
     * @var \App\Form\DataTransformer\FeedItemImageTransformer
     */
    protected $feedIdemImageTransformer;

    /**
     * FeedItemType constructor.
     * @param \App\Form\DataTransformer\FeedItemImageTransformer $feedIdemImageTransformer
     */
    public function __construct(FeedItemImageTransformer $feedIdemImageTransformer)
    {
        $this->feedIdemImageTransformer = $feedIdemImageTransformer;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
            ])
            ->add('description', TextType::class, [
                'required' => false,
            ])
            ->add('location', EntityType::class, [
                'class' => Country::class,
                'choice_value' => 'code',
                'required' => true,
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'required' => true,
            ])
            ->add('price', PriceType::class, [
                'required' => true,
            ])
            ->add('worldwide_shipping', CheckboxType::class, [
                'property_path' => 'worldwideShipping',
                'false_values' => ['', 0, false, null],
                'required' => false,
            ])
            ->add('images', CollectionType::class, [
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'required' => true,
            ])
            ->add('quantities', CollectionType::class, [
                'entry_type' => QuantityType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'required' => true,
            ]);

        $builder->get('images')->addModelTransformer($this->feedIdemImageTransformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FeedItem::class,
        ]);
    }
}
