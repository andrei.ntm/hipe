<?php

namespace App\Form;

use App\Entity\Currency;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class)
            ->add('fullname', TextType::class)
            ->add('currency', EntityType::class, [
                'property_path' => 'currency',
                'class' => Currency::class,
            ])
            ->add('role', ChoiceType::class, [
                'choices' => [
                    'ROLE_USER',
                    'ROLE_INFLUENCER'
                ],
                'required' => true,
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'groups' => ['registration']
                    ])
                ],
            ])
            ->add('email', EmailType::class)
            ->add('password', PasswordType::class)
        ;
    }

    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
