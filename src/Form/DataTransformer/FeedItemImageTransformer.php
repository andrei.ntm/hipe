<?php

namespace App\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class FeedItemImageTransformer implements DataTransformerInterface
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * FeedItemImageTransformer constructor.
     * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        return $value;
    }

    /**
     * @param mixed $value
     *
     * @return mixed
     */
    public function reverseTransform($value)
    {
        if ($value instanceof ArrayCollection && $value->count() > 0) {
            foreach ($value as $key => $image) {
                $relativePath = str_replace(sprintf('%s/', $this->request->getSchemeAndHttpHost()), '', $image);
                $value->set($key, $relativePath);
            }

            return $value;
        }

        return $value;
    }
}
