<?php

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class UserPhotoTransformer
 * @package App\Form\DataTransformer
 */
class UserPhotoTransformer implements DataTransformerInterface
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request|null
     */
    private $request;

    /**
     * UserPhotoTransformer constructor.
     * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @param mixed $value
     * @return mixed|void
     */
    public function transform($value)
    {
        // TODO: Implement transform() method.
    }

    /**
     * @param mixed $value
     * @return mixed|string|string[]
     */
    public function reverseTransform($value)
    {
        if ($value) {
            return str_replace(sprintf('%s/', $this->request->getSchemeAndHttpHost()), '', $value);
        }

        return $value;
    }
}
