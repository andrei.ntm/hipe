<?php

namespace App\Form;

use App\Entity\Currency;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VkUserType extends AbstractType
{
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullname', TextType::class)
            ->add('email', TextType::class)
            ->add('currency_id', EntityType::class, [
                'property_path' => 'currency',
                'class' => Currency::class,
            ])
            ->add('vk_id', TextType::class, [
                'property_path' => 'vkId',
            ])
            ->add('vk_access_token', TextType::class, [
                'property_path' => 'vkAccessToken',
            ])
            ->add('email_verified', CheckboxType::class, [
                'property_path' => 'emailVerified',
                'false_values' => ['', 0, false, null]
            ])
            ->add('email_verification_token', TextType::class, [
                'property_path' => 'emailVerificationToken'
            ])
        ;
    }

    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
