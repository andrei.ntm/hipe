<?php

namespace App\Form;

use App\Form\Model\FeedItem\Price;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PriceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amount', NumberType::class, [
                'required' => true,
                'scale' => 2,
            ])
            ->add('domestic_shipping_amount', NumberType::class, [
                'required' => true,
                'scale' => 2,
                'property_path' => 'domesticShippingAmount',
            ])
            ->add('worldwide_shipping_amount', NumberType::class, [
                'required' => false,
                'scale' => 2,
                'property_path' => 'worldwideShippingAmount',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Price::class,
        ]);
    }
}
