<?php

namespace App\Form;

use App\Entity\Country;
use App\Entity\Currency;
use App\Entity\User;
use App\Form\DataTransformer\UserPhotoTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProfileType
 * @package App\Form
 */
class ProfileType extends AbstractType
{
    /**
     * @var \App\Form\DataTransformer\UserPhotoTransformer
     */
    protected $userPhotoTransformer;

    /**
     * ProfileType constructor.
     * @param \App\Form\DataTransformer\UserPhotoTransformer $userPhotoTransformer
     */
    public function __construct(UserPhotoTransformer $userPhotoTransformer)
    {
        $this->userPhotoTransformer = $userPhotoTransformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullname', TextType::class, [
                'required' => true,
            ])
            ->add('username', TextType::class, [
                'required' => true,
            ])
            ->add('email', EmailType::class, [
                'required' => true,
            ])
            ->add('photo', TextType::class, [
                'required' => false,
            ])
            ->add('description', TextType::class, [
                'required' => false,
            ])
            ->add('old_password', TextType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('currency', EntityType::class, [
                'property_path' => 'currency',
                'class' => Currency::class,
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => ['label' => 'user.password.first'],
                'second_options' => ['label' => 'user.password.second'],
                'required' => false,
            ])
            ->add('instagram_account', TextType::class, [
                'property_path' => 'instagramAccount',
                'required' => false,
            ])
            ->add('youtube_account', TextType::class, [
                'property_path' => 'youtubeAccount',
                'required' => false,
            ])
            ->add('role', ChoiceType::class, [
                'choices' => [
                    'ROLE_USER',
                    'ROLE_INFLUENCER'
                ],
                'required' => false,
                'mapped' => false
            ])
            ->add('tiktok_account', TextType::class, [
                'property_path' => 'tiktokAccount',
                'required' => false,
            ]);

        $builder->get('photo')->addModelTransformer($this->userPhotoTransformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
